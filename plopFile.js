module.exports = plop => {
  plop.setGenerator('component', {
    description: 'Create a reusable component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your component name?',
      },
      {
        type: 'input',
        name: 'path',
        message: 'What is component root path?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '{{ path }}/components/{{pascalCase name}}/{{pascalCase name}}.js',
        templateFile: 'plop-templates/Component/Component.js.hbs',
      },
      {
        type: 'add',
        path: '{{ path }}/components/{{pascalCase name}}/{{pascalCase name}}.test.js',
        templateFile: 'plop-templates/Component/Component.test.js.hbs',
      },
      {
        type: 'add',
        path: '{{ path }}/components/{{pascalCase name}}/{{dashCase name}}.module.scss',
        templateFile: 'plop-templates/Component/Component.module.scss.hbs',
      },
      {
        type: 'add',
        path: '{{ path }}/components/{{pascalCase name}}/index.js',
        templateFile: 'plop-templates/Component/index.js.hbs',
      },
      {
        type: 'add',
        path: '{{ path }}/components/index.js',
        templateFile: 'plop-templates/injectable-index.js.hbs',
        skipIfExists: true,
      },
      {
        type: 'append',
        path: '{{ path }}/components/index.js',
        pattern: `/* PLOP_INJECT_IMPORT */`,
        template: `import {{pascalCase name}} from './{{pascalCase name}}';`,
      },
      {
        type: 'append',
        path: '{{ path }}/components/index.js',
        pattern: `/* PLOP_INJECT_EXPORT */`,
        template: `{{pascalCase name}},`,
      },
    ],
  });

  plop.setGenerator('page', {
    description: 'Create a page',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your page name?',
      },
      {
        type: 'input',
        name: 'path',
        message: 'What is your page root folder?',
      },
      {
        type: 'input',
        name: 'route',
        message: 'What is your page route path?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '{{ path }}/pages/{{pascalCase name}}/{{pascalCase name}}.js',
        templateFile: 'plop-templates/Component/Component.js.hbs',
      },
      {
        type: 'add',
        path: '{{ path }}/pages/{{pascalCase name}}/{{pascalCase name}}.test.js',
        templateFile: 'plop-templates/Component/Component.test.js.hbs',
      },
      {
        type: 'add',
        path: '{{ path }}/pages/{{pascalCase name}}/{{dashCase name}}.module.scss',
        templateFile: 'plop-templates/Component/Component.module.scss.hbs',
      },
      {
        type: 'add',
        path: '{{ path }}/pages/{{pascalCase name}}/index.js',
        templateFile: 'plop-templates/Component/index.js.hbs',
      },
      {
        type: 'add',
        path: 'src/router/index.js',
        templateFile: 'plop-templates/injectable-router.js.hbs',
        skipIfExists: true,
      },
      {
        type: 'append',
        path: 'src/router/Router.js',
        pattern: `/* PLOP_INJECT_COMPONENT_IMPORT */`,
        template: `import {{pascalCase name}} from '{{ path }}/pages/{{pascalCase name}}';`,
      },
      {
        type: 'append',
        path: 'src/router/Router.js',
        pattern: `/* PLOP_INJECT_COMPONENT_ROUTE */`,
        template: `<Route path='{{ route }}' component={ {{ pascalCase name }} } />`,
      },
    ],
  });
};

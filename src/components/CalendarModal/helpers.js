import { isEqual } from 'date-fns';

export function getScrollbarSize() {
  const scrollDiv = document.createElement('div');
  scrollDiv.style.width = '99px';
  scrollDiv.style.height = '99px';
  scrollDiv.style.position = 'absolute';
  scrollDiv.style.top = '-9999px';
  scrollDiv.style.overflow = 'scroll';

  document.body.appendChild(scrollDiv);
  const scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
  document.body.removeChild(scrollDiv);

  return scrollbarSize;
}

export function isOverflowing(container) {
  // works only if container is body =))
  return window.innerWidth > container.clientWidth;
}
export const isToday = day => {
  const today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
  return isEqual(day, today);
};

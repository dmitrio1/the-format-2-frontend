import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import DefaultBackground from 'components/Modal/DefaultBackground';
import { createPortal } from 'react-dom';
import classes from './eventDescrPopUp.module.css';

import EventList from '../EventList';

const EventDescrPopUp = props => {
  const { onClose, date, calendarEvent, isOpen } = props;

  useEffect(() => {
    const root = document.body;

    const handleKeyDown = e => {
      if (e.key !== 'Escape') {
        return;
      }

      if (onClose) {
        onClose();
      }
    };
    /* eslint-disable no-underscore-dangle */
    const _handleKeyDown = e => handleKeyDown(e);

    root.addEventListener('keydown', _handleKeyDown);

    return () => {
      root.removeEventListener('keydown', _handleKeyDown);
    };
  }, [isOpen, onClose]);

  const handleBackgroundClick = e => {
    if (e.target !== e.currentTarget) {
      return;
    }

    if (onClose) {
      onClose(e, 'backgroundClick');
    }
  };
  return createPortal(
    <>
      <DefaultBackground isOpen={isOpen} onKeyDown={() => {}} onClick={handleBackgroundClick} />
      <div className={classes.modalContent}>
        <div className={classes.popUp}>
          <div className={classes.top}>
            <span className={classes.date}>{date}</span>
            <button className={classes.closeBtn} onClick={onClose} type="button" />
            <span className={classes.line} />
          </div>
          <div className={classes.main}>
            <span className={classes.rectangleBig} />
            <span className={classes.rectangleSmall} />
            <EventList date={date} calendarEvent={calendarEvent} />
          </div>
        </div>
      </div>
    </>,
    document.getElementById('root'),
  );
};

EventDescrPopUp.propTypes = {
  calendarEvent: PropTypes.arrayOf(PropTypes.object),
  date: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};
EventDescrPopUp.defaultProps = {
  date: '',
  calendarEvent: null,
};
export default EventDescrPopUp;

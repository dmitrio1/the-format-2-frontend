import React from 'react';
import PropTypes from 'prop-types';
import classes from './eventList.module.css';

const randomID = () => {
  return Math.random();
};
const EventList = props => {
  const { date, calendarEvent } = props;

  const renderEventItem = () => {
    if (!calendarEvent) {
      return false;
    }

    const curEv = calendarEvent.filter(el => el.date === date);

    return curEv.map(eventObj => {
      return (
        <li className={classes.eventItem} key={randomID()}>
          <div className={classes.time}>{eventObj.time}</div>
          <div className={classes.text}>
            <p className={classes.title}>{eventObj.name}</p>
            <p className={classes.descr}>{eventObj.description}</p>
          </div>
        </li>
      );
    });
  };
  return <ul className={classes.eventList}>{renderEventItem()}</ul>;
};

EventList.propTypes = {
  calendarEvent: PropTypes.arrayOf(PropTypes.object).isRequired,
  date: PropTypes.string.isRequired,
};

export default EventList;

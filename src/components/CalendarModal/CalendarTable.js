/* eslint-disable */
import React, { useContext } from 'react';
import cn from 'classnames';
import DataContext from 'Contexts';
import * as dateFns from 'date-fns';
import format from 'date-fns/format';
import { isBefore, isAfter, parse, isEqual, isValid } from 'date-fns';
import { isToday } from './helpers';
import PropTypes from 'prop-types';
import classes from './calendar.module.css';

const dayN = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];

const renderTHead = days => {
  const tHead = days.map((day, index) => {
    return (
      <div className={classes.tHead} key={`${index + day}`}>
        {day}
      </div>
    );
  });
  return <div className={classes.tRow}>{tHead}</div>;
};

const CalendarTable = props => {
  const { date, setIsModalOpen, setEventD } = props;
  const appData = useContext(DataContext);
  const calendar = appData ? appData.calendar : [];

  const renderTBody = () => {
    const { months, years } = date;

    const currentMonth = new Date(` ${years} ${months}`);

    const monthStart = dateFns.startOfMonth(currentMonth);
    const monthEnd = dateFns.endOfMonth(monthStart);
    const startDate = dateFns.startOfWeek(monthStart, { weekStartsOn: 1 });
    /* return the date of 1-st day of week of current
      mounth e.x  2 September 2014 => Mon Sep 01 2014 */
    const endDate = dateFns.endOfWeek(monthEnd);
    // return the date of last day of week of current mounth
    const dateFormat = 'dd'; // 'dd' => 01,02,03
    const rows = [];

    let days = [];
    let day = startDate;
    let formattedDate = '';

    while (day <= endDate) {
      /* code below format date from calendar and date from db to the same
format and compare them, return obj with amount events on correspondent date */

      for (let i = 0; i < 7; i++) {
        if (!calendar.length) {
          return;
        }
        let evCount = 0;
        let eventDate = '';
        const dateF = 'yyyy MM dd'; // format date due to this pattern

        for (let j = 0; j < calendar.length; j++) {
          const formattedDay = format(day, dateF);
          let formatDate = parse(calendar[j].date, 'dd.MM.yyyy', new Date());

          if (!isValid(formatDate)) {
            continue;
          }
          formatDate = format(formatDate, dateF);

          if (
            isEqual(parse(formattedDay, dateF, new Date()), parse(formatDate, dateF, new Date()))
          ) {
            evCount = calendar[j].countEvent;
            eventDate = calendar[j].date;
          }
        }

        let eventText;
        if (evCount == 1) {
          eventText = 'Подія';
        } else if (evCount >= 5 && evCount <= 10) {
          eventText = 'Подій';
        } else {
          eventText = 'Події';
        }

        const bgEventCounter = (
          <span className={classes.bgEventCount}>{`${evCount} ${eventText}`}</span>
        );

        formattedDate = dateFns.format(day, dateFormat);
        const hoverBackdrop = (
          <div
            role="presentation"
            className={classes.backdrop}
            date={eventDate}
            onKeyDown={() => {
              setIsModalOpen(true);
              setEventD(eventDate);
            }}
            onClick={() => {
              setIsModalOpen(true);
              setEventD(eventDate);
            }}
          >
            <span className={classes.backdropDate}>{formattedDate}</span>
            <span className={classes.eventCount}>{`${evCount} ${eventText}`}</span>
            <span className={classes.details}>детальніше</span>
          </div>
        );

        /** classes @TODO  split cn in separate file/ function   */
        const disabledDays = cn({ disabled: isAfter(day, monthEnd) || isBefore(day, monthStart) });
        const clsEventday = cn({
          eventDay: evCount > 0,
        });
        const curDayCls = cn({ cell: !isToday(day), curentDay: isToday(day) });
        const curDateIndication = cn({
          curDate: isToday(day),
          date: !isToday(day),
        });

        days.push(
          <div
            className={`${classes[curDayCls]} ${classes[disabledDays] || ' '}
            ${classes[clsEventday] || ' '}`}
            key={day}
          >
            <span className={classes[curDateIndication]}>{formattedDate}</span>
            {evCount > 0 && bgEventCounter}
            {evCount > 0 && hoverBackdrop}
          </div>,
        );
        day = dateFns.addDays(day, 1);
      }
      /* create a rows for calendar  */
      rows.push(
        <div className={classes.tRow} key={day}>
          {days}
        </div>,
      );
      days = [];
    }
    return <div className={classes.tBody}>{rows}</div>;
  };

  return (
    <div className={classes.table}>
      {renderTHead(dayN)}
      {renderTBody()}
    </div>
  );
};
CalendarTable.propTypes = {
  date: PropTypes.shape({
    months: PropTypes.any,
    years: PropTypes.any,
  }).isRequired,
  setIsModalOpen: PropTypes.func.isRequired,
  setEventD: PropTypes.func.isRequired,
};
export default CalendarTable;

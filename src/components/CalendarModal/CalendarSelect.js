/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import classes from './calendar.module.css';

const addYears = () => {
  const years = [];
  const curYear = new Date().getFullYear() - 1;

  for (let i = 0; i <= 9; i++) {
    years.push(curYear + i);
  }
  return years;
};
const formControll = {
  years: addYears(),
  months: [
    'Січень',
    'Лютий',
    'Березень',
    'Квітень',
    'Травень',
    'Червень',
    'Липень',
    'Серпень',
    'Вересень',
    'Жовтень',
    'Листопад',
    'Грудень',
  ],
};

const CalendarSelect = props => {
  const { onChange, date } = props;
  /* date = {months ,years} */
  const generateSelect = () => {
    return Object.keys(formControll).map((sel, index) => {
      const options = formControll[sel];
      return (
        <select
          className={classes.calendarSelect}
          key={`${index + sel}`}
          onChange={onChange}
          value={date[sel]}
          name={sel}
        >
          {options.map((option, indx) => {
            const val = sel === 'months' ? indx + 1 : option;
            return (
              <option key={`${indx + option}`} value={val} className={classes.calendarOption}>
                {option}
              </option>
            );
          })}
        </select>
      );
    });
  };
  return <>{generateSelect()}</>;
};
CalendarSelect.propTypes = {
  date: PropTypes.shape({
    months: PropTypes.any,
    years: PropTypes.any,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
};
export default CalendarSelect;

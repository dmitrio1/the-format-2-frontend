import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import PropTypes from 'prop-types';
import { createPortal } from 'react-dom';
import classes from './calendar.module.css';
import EventDescrPopUp from './components/EventDescrPopUp';
import CalendarSelect from './CalendarSelect';
import CalendarTable from './CalendarTable';
import { getScrollbarSize } from './helpers';

const CalendarModal = props => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [eventD, setEventD] = useState(null);
  const { onClose } = props;
  const [calendarEvent, setCalendarEvent] = useState(null);
  const container = document.body;

  /* eslint-disable no-underscore-dangle */
  useEffect(() => {
    const handleEsc = e => {
      if (e.key !== 'Escape') {
        return;
      }
      onClose();
    };

    const _handleEsc = e => handleEsc(e);

    container.addEventListener('keydown', _handleEsc);

    if (isModalOpen) {
      container.removeEventListener('keydown', _handleEsc);
    }
    container.style.overflow = 'hidden';
    container.style.paddingRight = `${getScrollbarSize(container)}px`;

    return () => {
      container.removeEventListener('keydown', _handleEsc);

      container.style.overflow = 'visible';
      container.style.paddingRight = '0';
    };
  }, [isModalOpen, onClose, container]);

  useEffect(() => {
    const loadEvents = async () => {
      const result = await Axios.get('https://68.183.77.250/headerData');
      setCalendarEvent(result.data.calendarEvent);
    };
    loadEvents();
  }, []);

  const [date, setDate] = useState({
    months: new Date().getMonth() + 1,
    years: new Date().getFullYear(),
  });

  const handleChange = event => {
    setDate({ ...date, [event.currentTarget.name]: event.currentTarget.value });
  };

  return createPortal(
    <>
      <div className={classes.calendar}>
        <div className={classes.calendarInner}>
          <div className={classes.top}>
            <div className={classes.selects}>
              <CalendarSelect onChange={handleChange} date={date} />
            </div>
            <button type="button" className={classes.closeBtn} onClick={onClose} />
          </div>
          <CalendarTable date={date} setIsModalOpen={setIsModalOpen} setEventD={setEventD} />
        </div>
        {isModalOpen && (
          <EventDescrPopUp
            isOpen={isModalOpen}
            onClose={() => setIsModalOpen(false)}
            date={eventD}
            calendarEvent={calendarEvent}
          />
        )}
      </div>
    </>,
    document.getElementById('root'),
  );
};

CalendarModal.propTypes = {
  onClose: PropTypes.func.isRequired,
};

export default CalendarModal;

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styles from './BreadCrumbs.module.scss';

const BreadCrumbs = ({ breadCrumbsItems }) => {
  return (
    <ul className={styles.list}>
      {breadCrumbsItems.map((item, idx) => {
        const isLastItem = idx === breadCrumbsItems.length - 1;
        return (
          <li key={item.to} className={styles.list__item}>
            <Link
              to={item.to}
              className={!isLastItem ? styles.list__link : styles.list__link_accent}
            >
              {item.pageName}
              {!isLastItem && '/'}
            </Link>
          </li>
        );
      })}
    </ul>
  );
};

BreadCrumbs.propTypes = {
  breadCrumbsItems: PropTypes.arrayOf(
    PropTypes.shape({
      pageName: PropTypes.string.isRequired,
      to: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default BreadCrumbs;

import React from 'react';
import success from 'images/successIcon/success.svg';
import PropTypes from 'prop-types';

import styles from './SuccessMessage.module.scss';

const SuccessMessage = ({ children }) => {
  return (
    <div className={styles.success}>
      <div className={styles.success__imgBox}>
        <img src={success} alt="success" className={styles.success__img} />
      </div>
      {children}
    </div>
  );
};

SuccessMessage.propTypes = {
  children: PropTypes.node.isRequired,
};

export default SuccessMessage;

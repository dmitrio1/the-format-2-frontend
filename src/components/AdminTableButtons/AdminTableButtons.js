import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import styles from './AdminTableButtons.module.css';

const AdminTableButtons = ({ id, path, onDelete }) => {
  return (
    <div className={styles.btnBox}>
      <Link to={{ pathname: path }} className={styles.btnEdit}>
        Edit
      </Link>
      <button type="button" className={styles.btnDelete} onClick={() => onDelete(id)}>
        Delete
      </button>
    </div>
  );
};

AdminTableButtons.propTypes = {
  id: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  path: PropTypes.string.isRequired,
};

export default AdminTableButtons;

/* eslint-disable */
import React, { useContext, createContext, useState, useEffect } from 'react';
import defaultStyles from './form.module.scss';
/**
 *  in progress. form, based on context
 *  
 *  Example usage
 * 
 *    <Form initialFormData={{ name: 'her', surname: 's gori' }}>
        <Input name='name' />
        <Input name='surname' />
      </Form>
 *  With such approach splitting form to parts become much easier 
 *  @TODO rename Input to FormField. FormField consumes context and renders form element (not only input, but any element such as select, range, checkbox, etc)
 *
 */

const FormDataContext = createContext({});
const ErrorsContext = createContext({});
const HandleChangeContext = createContext(() => {});
const DefaultFormStylesContext = createContext({});
const ValidationContext = createContext({});

export const FormConsumer = ({ children }) => {
  const formData = useContext(FormDataContext);
  const errors = useContext(ErrorsContext);
  const handleChange = useContext(HandleChangeContext);
  const styles = useContext(DefaultFormStylesContext);
  const validator = useContext(ValidationContext);
  //  const childrenArr = React.Children.toArray(children);
  return <>{children({ formData, errors, handleChange, styles, validator })}</>;
};

const Form = props => {
  const { initialFormData, onSubmit, children, validator, placeholder, styles, ...other } = props;
  const s = styles ? { ...defaultStyles, ...styles } : defaultStyles;
  const [formData, setFormData] = useState(initialFormData);
  const [errors, setErrors] = useState({});

  const _handleChange = (e, validator) => {
    const { name, value } = e.target;    

    if (validator) {
      const error = validator(value);
      setErrors(prevErrors => ({ ...prevErrors, [name]: error }));
    }
    setFormData({ ...formData, [name]: value });
  };

  useEffect(() => {
    setFormData(initialFormData);
    setErrors({});
  }, [initialFormData]);

  const _handleSubmit = e => {
    e.preventDefault();
    let errors = {};

    for (const name in validator) {
      const error = validator[name](formData[name]);
      if (error) errors[name] = error;
    }

    if (!Object.keys(errors).length) onSubmit(formData);
    else setErrors(errors);
  };

  return (
    <FormDataContext.Provider value={formData}>
      <ErrorsContext.Provider value={errors}>
        <HandleChangeContext.Provider value={_handleChange}>
          <DefaultFormStylesContext.Provider value={s}>
            <ValidationContext.Provider value={validator}>
              <form onSubmit={e => _handleSubmit(e)} className={s.form}>
                {children}
              </form>
            </ValidationContext.Provider>
          </DefaultFormStylesContext.Provider>
        </HandleChangeContext.Provider>
      </ErrorsContext.Provider>
    </FormDataContext.Provider>
  );
};

Form.propTypes = {};

export default Form;

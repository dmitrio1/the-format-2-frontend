import validator from 'validator';

export const nameValidator = value => {
  if (validator.isEmpty(value)) {
    return " Введіть ім'я";
  }

  if (!validator.isLength(value, { min: 2, max: 40 })) {
    return " Ім'я має складатися не менш ніж з 2 та не більше ніж з 40 символів";
  }

  return null;
};

export const emailValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Будь ласка, введіть email';
  }
  if (!validator.isEmail(value)) {
    return ' Будь ласка, введіть коректний email';
  }
  return null;
};

export const phoneValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть номер';
  }

  if (!validator.isMobilePhone(value, 'uk-UA', { strictMode: true })) {
    return ' Номер телефону не коректний';
  }
  return null;
};

export const notValidate = () => null;

export const required = value => {
  return !validator.isEmpty(value) ? "обов'язкове поле" : null;
};

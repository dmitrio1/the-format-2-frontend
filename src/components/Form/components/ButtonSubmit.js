/* eslint-disable no-underscore-dangle */
import React from 'react';
import { string } from 'prop-types';
import { FormConsumer } from '../Form';

const ButtonSubmit = ({ text }) => {
  return (
    <FormConsumer>
      {({ styles }) => {
        return (
          <button type="submit" className={styles.button}>
            {text}
          </button>
        );
      }}
    </FormConsumer>
  );
};

ButtonSubmit.propTypes = {
  text: string.isRequired,
};

export default ButtonSubmit;

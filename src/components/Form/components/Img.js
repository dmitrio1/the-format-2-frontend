import React from 'react';
import { string, bool, objectOf } from 'prop-types';
import { FormConsumer } from '../Form';

const Img = ({ name, isRequired, labelText, alt, ...rest }) => {
  return (
    <FormConsumer>
      {({ formData, errors, styles }) => {
        // eslint-disable-next-line no-underscore-dangle
        return (
          <div className={`${styles.wrapper} ${styles['wrapper-image']}`}>
            <label className={styles.label} htmlFor={name}>
              <span className={styles.labelText}>{labelText}</span>

              {isRequired && <span className={styles.star}> * </span>}

              {errors[name] && <span className={styles.error}>{errors[name]}</span>}
            </label>
            <img
              className={`${styles.field} ${styles.image}`}
              id={name}
              name={name}
              src={formData[name] || ''}
              alt={alt}
              {...rest}
            />
          </div>
        );
      }}
    </FormConsumer>
  );
};
Img.defaultProps = {
  alt: '',
  isRequired: true,
  labelText: '',
  styles: null,
};

Img.propTypes = {
  alt: string,
  isRequired: bool,
  labelText: string,
  name: string.isRequired,
  styles: objectOf(string),
};

export default Img;

/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/no-onchange */
import React from 'react';
import PropTypes, { element, string, bool, arrayOf, objectOf } from 'prop-types';
import { FormConsumer } from '../Form';

const Select = ({ children, name, isRequired, labelText, placeholder, optionsList, ...rest }) => (
  <FormConsumer>
    {({ formData, errors, handleChange, validator, styles }) => {
      // to allow styles customizing

      return (
        <div className={styles.wrapper}>
          <label className={styles.label} htmlFor={name}>
            <span className={styles.labelText}>{labelText}</span>

            {isRequired && <span className={styles.star}> * </span>}

            {errors[name] && <span className={styles.error}>{errors[name]}</span>}
          </label>

          <div className={styles.selectWrapper}>
            <select
              name={name}
              id={name}
              value={formData[name] || ''}
              className={styles.field}
              onChange={e => {
                handleChange(e, validator[name]);
              }}
              {...rest}
            >
              {optionsList}
            </select>
          </div>
        </div>
      );
    }}
  </FormConsumer>
);

Select.defaultProps = {
  children: null,
  isRequired: true,
  styles: null,
  placeholder: '',
};

Select.propTypes = {
  children: element,
  isRequired: bool,
  labelText: string.isRequired,
  name: PropTypes.string.isRequired,
  optionsList: arrayOf(element).isRequired,
  placeholder: string,
  styles: objectOf(string),
};

export default Select;

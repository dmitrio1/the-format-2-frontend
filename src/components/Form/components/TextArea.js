/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { string, bool, objectOf, func } from 'prop-types';
import { FormConsumer } from '../Form';

const TextArea = ({ name, isRequired, labelText, placeholder, ...rest }) => {
  return (
    <FormConsumer>
      {({ formData, handleChange, errors, styles, validator }) => {
        // eslint-disable-next-line no-underscore-dangle
        return (
          <div className={styles.wrapper}>
            <label className={styles.label} htmlFor={name}>
              <span className={styles.labelText}>{labelText}</span>

              {isRequired && <span className={styles.star}> * </span>}

              {errors[name] && <span className={styles.error}>{errors[name]}</span>}
            </label>

            <textarea
              placeholder={placeholder}
              className={`${styles.field} ${styles.textarea}`}
              id={name}
              name={name}
              value={formData[name] || ''}
              type={rest.type ? rest.type : 'text'}
              onChange={e => {
                handleChange(e, validator[name]);
              }}
              {...rest}
            />
          </div>
        );
      }}
    </FormConsumer>
  );
};
TextArea.defaultProps = {
  isRequired: true,
  styles: null,
  validator: null,
  placeholder: '',
  labelText: '',
};

TextArea.propTypes = {
  isRequired: bool,
  labelText: string,
  name: string.isRequired,
  placeholder: string,
  styles: objectOf(string),
  validator: func,
};

export default TextArea;

/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { string, bool, objectOf } from 'prop-types';
import { FormConsumer } from '../Form';

const Input = ({ name, isRequired, labelText, placeholder, isDisabled, ...rest }) => {
  return (
    <FormConsumer>
      {({ formData, handleChange, errors, styles, validator }) => {
        // eslint-disable-next-line no-underscore-dangle
        return (
          <div className={styles.wrapper}>
            <label className={styles.label} htmlFor={name}>
              <span className={styles.labelText}>{labelText}</span>

              {isRequired && <span className={styles.star}> * </span>}

              {errors[name] && <span className={styles.error}>{errors[name]}</span>}
            </label>

            <input
              className={styles.field}
              id={name}
              name={name}
              value={formData[name] || ''}
              type={rest.type ? rest.type : 'text'}
              placeholder={placeholder}
              onChange={e => {
                handleChange(e, validator[name]);
              }}
              disabled={isDisabled}
              {...rest}
            />
          </div>
        );
      }}
    </FormConsumer>
  );
};
Input.defaultProps = {
  isDisabled: false,
  isRequired: true,
  styles: null,
  placeholder: '',
  labelText: '',
};

Input.propTypes = {
  isDisabled: bool,
  isRequired: bool,
  labelText: string,
  name: string.isRequired,
  placeholder: string,
  styles: objectOf(string),
};

export default Input;

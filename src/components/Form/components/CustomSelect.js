/* eslint-disable no-underscore-dangle, jsx-a11y/no-onchange */
import React, { useState } from 'react';
import PropTypes, { element, string, bool, arrayOf, objectOf, number } from 'prop-types';
import { FormConsumer } from '../Form';

const CustomSelect = ({
  children,
  name,
  isRequired,
  labelText,
  placeholder,
  options,
  defOptInd,
  ...rest
}) => {
  const [isOptionsShown, setIsOptionsShown] = useState(false);
  const [isOverDropList, setIsOverDropList] = useState(false);
  const [TemporalPlaceholder, setTemporalPlaceholder] = useState('');

  return (
    <FormConsumer>
      {({ errors, formData, handleChange, validator, styles }) => {
        const handleOptionSelect = index => {
          const e = { target: { value: options[index], name } };
          setIsOptionsShown(false);
          setIsOverDropList(false);
          handleChange(e, validator[name]);
        };

        const handleBlur = () => {
          if (!isOverDropList) {
            const event = { target: { value: TemporalPlaceholder, name } };
            handleChange(event, validator[name]);
            setIsOptionsShown(false);
          }
        };
        // // eslint-disable-next-line react/prop-types
        const optionsList = options.map((option, ind) => {
          const active = ind === 0 ? styles.active : '';
          return (
            <div
              aria-hidden
              className={`${styles.option} ${active}`}
              key={option}
              onClick={() => handleOptionSelect(ind)}
            >
              {option}
            </div>
          );
        });

        return (
          <div className={styles.wrapper}>
            <label className={styles.label} htmlFor={name}>
              <span className={styles.labelText}>{labelText}</span>

              {isRequired && <span className={styles.star}> * </span>}

              {errors[name] && <span className={styles.error}>{errors[name]}</span>}
            </label>

            <div className={styles.selectWrapper}>
              <input
                name={name}
                aria-hidden
                id={name}
                value={formData[name]}
                placeholder={TemporalPlaceholder}
                className={styles.field}
                onFocus={e => {
                  setTemporalPlaceholder(e.target.value);
                  const event = { target: { value: '', name } };
                  handleChange(event, validator[name]);
                  setIsOptionsShown(true);
                }}
                onBlur={e => handleBlur(e)}
                onChange={() => {
                  /* DO NOTHING. NEEDED TO REMOVE ERROR */
                  /* THAT VALUE IS HardCoded, WITHOUT ONCHANGE HANDLER */
                }}
                {...rest}
              />
              <div className={styles['drop-down-icon']} />
              {isOptionsShown && (
                <div
                  className={styles['option-wrapper']}
                  onMouseEnter={() => setIsOverDropList(true)}
                  onMouseLeave={() => setIsOverDropList(false)}
                >
                  {optionsList}
                </div>
              )}
            </div>
          </div>
        );
      }}
    </FormConsumer>
  );
};

CustomSelect.defaultProps = {
  children: null,
  isRequired: true,
  styles: null,
  placeholder: '',
  labelText: '',
};

CustomSelect.propTypes = {
  children: element,
  defOptInd: number.isRequired,
  isRequired: bool,
  labelText: string,
  name: PropTypes.string.isRequired,
  options: arrayOf(string).isRequired,
  placeholder: string,
  styles: objectOf(string),
};

export default CustomSelect;

import React from 'react';
import PropTypes from 'prop-types';
import calendar from './assets/calendar.svg';
import classes from './calendarToggle.module.css';

const CalendarToggle = props => {
  const { onClick } = props;
  return (
    <div
      className={classes.calendar}
      onClick={onClick}
      onKeyDown={() => {}}
      role="button"
      tabIndex={0}
    >
      <p className={classes.calendarText}>календар подій</p>
      <img className={classes.calendarIcon} src={calendar} alt="calendar" />
    </div>
  );
};

CalendarToggle.propTypes = {
  date: PropTypes.shape({
    months: PropTypes.string,
    years: PropTypes.string,
  }),
  onClick: PropTypes.func.isRequired,
};
CalendarToggle.defaultProps = {
  date: {},
};
export default CalendarToggle;

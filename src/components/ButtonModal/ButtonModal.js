import React, { useState } from 'react';
import { string, objectOf, func } from 'prop-types';
import Modal from 'components/Modal/Modal';
import defaultStyles from './button-modal.module.scss';

const ButtonModal = ({ buttonText, ModalItem, styles }) => {
  const [isModalDisplayed, setIsModalDisplayed] = useState(false);
  const s = styles ? { ...defaultStyles, ...styles } : defaultStyles;

  return (
    <div className={s.wrapper}>
      <button
        type="button"
        className={`${s['registration-button']} ${s['modal-button']}`}
        onClick={() => {
          setIsModalDisplayed(true);
        }}
      >
        {buttonText}
      </button>
      <Modal
        isOpen={isModalDisplayed}
        onClose={() => {
          setIsModalDisplayed(false);
        }}
      >
        <ModalItem closeModal={() => setIsModalDisplayed(false)} />
      </Modal>
    </div>
  );
};

ButtonModal.defaultProps = {
  styles: null,
};

ButtonModal.propTypes = {
  buttonText: string.isRequired,
  ModalItem: func.isRequired,
  styles: objectOf(string),
};

export default ButtonModal;

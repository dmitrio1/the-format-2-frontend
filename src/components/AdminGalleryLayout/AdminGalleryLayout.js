import React from 'react';
import { element, string } from 'prop-types';
import styles from './admin-gallery-layout.module.scss';

const AdminGalleryLayout = ({ children, title }) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.title}>{title}</div>
      <div className={styles['content-wrapper']}>
        {children}
        {/* <div className={styles.photos}>{photos}</div> */}
        {/* <NavLink to="/adminpanel/gallery/add" className={styles['add-photo-button']}>
        Додати фото
      </NavLink> */}
      </div>
    </div>
  );
};

// AdminGalleryLayout.defaultProps = {}

AdminGalleryLayout.propTypes = {
  children: element.isRequired,
  title: string.isRequired,
};

export default AdminGalleryLayout;

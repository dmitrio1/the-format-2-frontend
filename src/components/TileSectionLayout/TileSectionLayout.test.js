import React from 'react';
import { shallow } from 'enzyme';
import TileSection from './TileSectionLayout';

describe('TileSectionLayout', () => {
  const cards = <div>Cards</div>;
  it('rendered without crashes', () => {
    const component = shallow(
      <TileSection title="title" linkText="someLink" linkPath="/">
        {cards}
      </TileSection>,
    );
    expect(component.exists()).toBe(true);
  });

  it('grid columns must be calculated based on count of children elements', () => {
    const component = shallow(
      <TileSection title="title" linkText="someLink" linkPath="/">
        {cards}
      </TileSection>,
    );
    expect(1).toBe(1);
  });
});

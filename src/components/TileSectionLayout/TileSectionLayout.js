import React from 'react';
import { string, arrayOf, element, func } from 'prop-types';
import ButtonLink from 'components/ButtonLink/index';
import ButtonModal from 'components/ButtonModal/index';
import styles from './tile-section-layout.module.scss';

const TileSection = ({ title, buttonText, linkPath, children, buttonType, modalItem }) => {
  const columnsCountStyle = {
    gridTemplateColumns: `repeat(${children.length < 4 ? children.length : 4},1fr)`,
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.root}>
        <h2 className={styles.title}>{title}</h2>

        <div className={styles.cards} style={columnsCountStyle}>
          {children}
        </div>

        {buttonType === 'link' && <ButtonLink url={linkPath} text={buttonText} />}

        {buttonType === 'modal' && <ButtonModal buttonText={buttonText} ModalItem={modalItem} />}
      </div>
    </div>
  );
};

TileSection.defaultProps = {
  modalItem() {
    return <div>NOTHING HERE</div>;
  },
};

TileSection.propTypes = {
  buttonText: string.isRequired,
  buttonType: string.isRequired,
  children: arrayOf(element).isRequired,
  linkPath: string.isRequired,
  modalItem: func,
  title: string.isRequired,
};

export default TileSection;

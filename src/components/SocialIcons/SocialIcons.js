import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import instagram from './assets/instagram.svg';
import instagramFotter from './assets/instagramFooter.svg';
import facebook from './assets/facebook.svg';
import facebookFooter from './assets/facebookFooter.svg';
import telegram from './assets/telegram.svg';
import telegramFooter from './assets/telegramFooter.svg';
import classes from './socialIcons.module.css';

const icons = [
  {
    link: 'https://www.facebook.com/TheFormatVn/',
    sideBar: facebook,
    footer: facebookFooter,
    id: 1,
  },
  {
    link: 'https://www.instagram.com/theformatvn/?utm_source=ig_profile_share&igshid=1fj8gtv0dtwlv',
    sideBar: instagram,
    footer: instagramFotter,
    id: 2,
  },
  {
    link: 'https://t.me/the_format',
    sideBar: telegram,
    footer: telegramFooter,
    id: 3,
  },
];

const SocialIcons = props => {
  const { cls } = props;
  const clsIcon = cn({
    footerIcon: cls === 'footer',
    sidebarIcon: cls === 'sidebar',
    burgerIcon: cls === 'burgerIcon',
  });
  const clsHover = cn({
    hoverIcon: cls === 'footer',
  });
  return (
    <>
      {icons.map(icon => {
        return (
          <a target="blank" href={icon.link} key={icon.id}>
            <div className={classes[clsHover]}>
              <img
                className={classes[clsIcon] || ' '}
                src={cls === 'footer' ? icon.footer : icon.sideBar}
                alt="посилання на соц. мережу"
              />
            </div>
          </a>
        );
      })}
    </>
  );
};

SocialIcons.propTypes = {
  cls: PropTypes.string,
};
SocialIcons.defaultProps = {
  cls: 'sideBar',
};

export default SocialIcons;

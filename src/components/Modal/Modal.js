/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useLayoutEffect } from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';
import styles from './modal.module.scss';
import DefaultBackground from './DefaultBackground';
import { getScrollbarSize, isOverflowing } from './helpers';

const Modal = props => {
  const { children, onClose, isOpen, ...other } = props;

  /**
   * @TODO implement container resolver
   */

  const container = document.body;

  const handleKeyDown = e => {
    if (e.key !== 'Escape') {
      return;
    }

    e.stopPropagation();

    if (onClose) {
      onClose(e, 'escapeKeyDown');
    }
  };

  useLayoutEffect(() => {
    /* eslint-disable no-underscore-dangle */
    const _handleKeyDown = e => handleKeyDown(e);
    const scrollbarSize = getScrollbarSize();
    /**
     * @TODO attach handler to modal wrapper, not to document. use focus lock to trap focus
     */
    document.addEventListener('keydown', _handleKeyDown);

    if (isOpen) {
      /**
       * @TODO determine scroll width and original padding
       */
      if (isOverflowing(container)) {
        container.style.overflow = 'hidden';
        container.style.paddingRight = `${scrollbarSize}px`;
      }
    } else {
      container.style.overflow = 'visible';
      container.style.paddingRight = '0';
    }

    return () => {
      /**
       * @TODO store previous style value
       */
      container.style.overflow = 'visible';
      container.style.paddingRight = '0';

      document.removeEventListener('keydown', _handleKeyDown);
    };
  }, [isOpen]);

  const handleBackgroundClick = e => {
    if (e.target !== e.currentTarget) {
      return;
    }

    if (onClose) {
      onClose(e, 'backgroundClick');
    }
  };

  return createPortal(
    <>
      {isOpen && (
        <div role="presentation" onKeyDown={handleKeyDown} {...other}>
          <DefaultBackground isOpen={isOpen} onClick={handleBackgroundClick} />
          <div className={styles.modalContent}>{children}</div>
        </div>
      )}
    </>,
    document.getElementById('root'),
  );
};

Modal.propTypes = {
  children: PropTypes.element.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default Modal;

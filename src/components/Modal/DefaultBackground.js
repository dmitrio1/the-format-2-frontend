/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import styles from './modal.module.scss';

const DefaultBackground = props => {
  const { isOpen, ...other } = props;

  return isOpen && <div id="dfBg" aria-hidden className={styles.modalBackground} {...other} />;
};

DefaultBackground.propTypes = {
  isOpen: PropTypes.bool.isRequired,
};

export default DefaultBackground;

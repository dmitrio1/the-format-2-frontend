import React from 'react';
import PropTypes from 'prop-types';
import defaultClasses from './adminButtonSaveEdit.module.css';

const AdminButtonSaveEdit = ({ reset, saveBtnTxt, editBtnTxt, classes }) => {
  const s = classes ? { ...defaultClasses, ...classes } : defaultClasses;
  return (
    <div className={s.adminBtns}>
      <button type="submit" className={s.buttonSave}>
        {saveBtnTxt}
      </button>
      <button className={`${s.buttonEdit}`} type="button" onClick={reset}>
        {editBtnTxt}
      </button>
    </div>
  );
};
AdminButtonSaveEdit.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string),
  editBtnTxt: PropTypes.string.isRequired,
  reset: PropTypes.func.isRequired,
  saveBtnTxt: PropTypes.string.isRequired,
};
AdminButtonSaveEdit.defaultProps = {
  classes: {},
};

export default AdminButtonSaveEdit;

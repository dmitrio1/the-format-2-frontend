import React from 'react';
import { Link } from 'react-router-dom';
import HeaderInner from './components/HeaderInner';
import logo from './assets/header-logo.svg';
import classes from './header.module.css';

const Header = () => {
  return (
    <>
      <header className={classes.header}>
        <div className={classes.headerLogo}>
          <Link to="/">
            <img src={logo} alt="logo" className={classes.logoImg} />
          </Link>
        </div>
        <div className={classes.headerContent}>
          <HeaderInner />
        </div>
      </header>
    </>
  );
};

export default Header;

import React, { useEffect } from 'react';
import { HashLink as Link } from 'react-router-hash-link';
import PropTypes from 'prop-types';
import cn from 'classnames';
import SocialIcons from 'components/SocialIcons';
import classes from './menu.module.css';
import Backdrop from '../Backdrop';

const lists = {
  top: [
    { to: '/#CoursesForAdult', label: 'Живопис для дорослих', exact: false, id: 1 },
    { to: '/#coursesForChildren', label: 'Живопис для дітей та підлітків', exact: false, id: 2 },
    { to: '/#orders', label: 'В нас можна замовити', exact: false, id: 3 },
    { to: '/#courses', label: 'Курси живопису', exact: false, id: 4 },
  ],
  middle: [
    { to: '/', label: 'Головна', exact: true, id: 5 },
    { to: '/#shop', label: 'Магазин', exact: true, id: 6 },
    { to: '/#galery', label: 'Галерея', exact: true, id: 8 },
    { to: '/#comments', label: 'Відгуки', exact: true, id: 9 },
    { to: '/#container-map-id', label: 'Контакти', exact: true, id: 10 },
  ],
};
export const idGenerator = () => {
  return Math.random();
};

const getScreenWidth = () => {
  return document.documentElement.clientWidth < 1000;
};
const renderLinks = (listsArr, func) => {
  return Object.keys(listsArr).map(listName => {
    const list = listsArr[listName];
    const licls = cn({
      topLi: listName === 'top',
      middleLi: listName === 'middle',
    });

    const linkCls = cn({
      boldLink: listName === 'top',
      lightLink: listName === 'middle',
    });

    return (
      <ul className={classes[listName]} key={idGenerator()}>
        {list.map(link => {
          return (
            <li key={link.id} className={classes[licls]}>
              <Link
                to={link.to}
                exact={link.exact.toString()}
                className={classes[linkCls]}
                onClick={func}
              >
                {link.label}
              </Link>
            </li>
          );
        })}
      </ul>
    );
  });
};

/* eslint-disable no-underscore-dangle */

const Menu = props => {
  const { isOpen, onClick } = props;
  const container = document.body;

  useEffect(() => {
    const handleEsc = e => {
      if (e.key !== 'Escape') {
        return;
      }
      onClick();
    };
    const _handleEsc = e => handleEsc(e);
    container.addEventListener('keydown', _handleEsc);

    container.style.overflow = 'hidden';

    return () => {
      container.removeEventListener('keydown', _handleEsc);

      container.style.overflow = 'visible';
      container.style.paddingRight = '0';
    };
  }, [isOpen, container, onClick]);

  return (
    <>
      <Backdrop onClick={onClick}>
        <nav className={classes.menu}>
          <button className={classes.closeBtn} onClick={() => onClick} type="button" />
          <ul className={classes.mainMenu}>
            {renderLinks(lists, onClick)}
            <ul className={classes.bottom}>
              <li className={classes.bottomLi}>
                <a href="tel:+380989929258" className={classes.bottomLink}>
                  +38 (098) 992 92 58
                </a>
              </li>
              <li className={classes.bottomLi}>
                <a href="mailto:the.format.vn@gmail.com" className={classes.bottomLink}>
                  the.format.vn@gmail.com
                </a>
              </li>
              {getScreenWidth() && (
                <li className={classes.burgerIcons}>
                  <SocialIcons cls="burgerIcon" />
                </li>
              )}
            </ul>
          </ul>
        </nav>
      </Backdrop>
    </>
  );
};

Menu.propTypes = {
  isOpen: PropTypes.bool,
  onClick: PropTypes.func,
};
Menu.defaultProps = {
  isOpen: false,
  onClick() {
    return null;
  },
};

export default Menu;

import React, { useState } from 'react';
import CalendarToggle from 'components/CalendarToggle';
import CalendarModal from 'components/CalendarModal';
import { CSSTransition } from 'react-transition-group';
import classes from './headerInner.module.css';
import calendarAnimation from './calendarAnimation.module.scss';

import Menu from '../Menu';

const HeaderInner = () => {
  const [isCalendarOpen, setOpen] = useState(false);
  const [isOpenMenu, setMenu] = useState(false);

  const toggleOffMenuHandler = () => {
    setMenu(false);
  };
  const toggleOnMenuHandler = () => {
    setMenu(true);
  };

  const handleCalendarOpen = () => {
    setOpen(true);
  };
  const handleCalendarClose = () => {
    setOpen(s => {
      return !s;
    });
  };

  return (
    <>
      <CSSTransition
        in={isOpenMenu}
        classNames={{ ...classes }}
        mountOnEnter
        unmountOnExit
        timeout={700}
      >
        <Menu onClick={toggleOffMenuHandler} isOpen={isOpenMenu} />
      </CSSTransition>
      <header className={classes.header}>
        <div className={classes.schedule}>
          <a href="tel:+380989929258" className={classes.phone}>
            +38 (098) 992 92 58
          </a>
          <CalendarToggle onClick={handleCalendarOpen} />
        </div>

        <button className={classes.burger} onClick={toggleOnMenuHandler} type="button">
          <span className={classes.top} />
          <span className={classes.middle} />
          <span className={classes.bottom} />
        </button>
      </header>
      <CSSTransition
        classNames={{ ...calendarAnimation }}
        timeout={700}
        mountOnEnter
        unmountOnExit
        in={isCalendarOpen}
      >
        <CalendarModal onClose={handleCalendarClose} isOpen={isCalendarOpen} />
      </CSSTransition>
    </>
  );
};

export default HeaderInner;

import React from 'react';
import PropTypes from 'prop-types';
import classes from './Backdrop.module.css';

const Backdrop = ({ children, onClick }) => {
  return (
    <div
      role="button"
      type="button"
      className={classes.Backdrop}
      onClick={onClick}
      tabIndex={0}
      onKeyDown={() => {}}
    >
      {children}
    </div>
  );
};

Backdrop.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
};
Backdrop.defaultProps = {
  onClick() {
    return null;
  },
};
export default Backdrop;

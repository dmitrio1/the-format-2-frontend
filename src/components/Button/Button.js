import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classes from './button.module.css';

const Button = props => {
  const { url, children } = props;
  return (
    <Link className={classes.button} to={url}>
      {children}
    </Link>
  );
};

Button.propTypes = {
  children: PropTypes.string,
  url: PropTypes.string,
};
Button.defaultProps = {
  url: '/',
  children: '',
};
export default Button;

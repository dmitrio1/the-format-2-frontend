/* eslint-disable max-len */
import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import styles from './AdminSideBar.module.scss';
import SalesList from './components/SalesList/SalesList';
import FeedBackList from './components/FeedBackList/FeedBackList';
import ClientsList from './components/ClientsList/ClientsList';
import ServiceList from './components/ServiceList/ServiceList';
import CalendarList from './components/CalendarList/CalendarList';
import GalleryList from './components/GalleryList/GalleryList';

const AdminSideBar = ({ newClients, newComments }) => {
  return (
    <nav className={styles.nav}>
      <ul className={styles.navigation}>
        <li className={styles.navigation__item}>
          <NavLink
            className={styles.navigation__link}
            to="/adminpanel/page/mainpage"
            activeStyle={{ color: '#ffdd15' }}
          >
            <svg
              aria-hidden="true"
              focusable="false"
              data-prefix="fas"
              data-icon="home"
              className={styles.navigation__icon}
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 -70 576 512"
            >
              <path
                fill="currentColor"
                d="M280.37 148.26L96 300.11V464a16 16 0 0 0 16 16l112.06-.29a16 16 0 0 0 15.92-16V368a16 16 0 0 1 16-16h64a16 16 0 0 1 16 16v95.64a16 16 0 0 0 16 16.05L464 480a16 16 0 0 0 16-16V300L295.67 148.26a12.19 12.19 0 0 0-15.3 0zM571.6 251.47L488 182.56V44.05a12 12 0 0 0-12-12h-56a12 12 0 0 0-12 12v72.61L318.47 43a48 48 0 0 0-61 0L4.34 251.47a12 12 0 0 0-1.6 16.9l25.5 31A12 12 0 0 0 45.15 301l235.22-193.74a12.19 12.19 0 0 1 15.3 0L530.9 301a12 12 0 0 0 16.9-1.6l25.5-31a12 12 0 0 0-1.7-16.93z"
              />
            </svg>
            <span className={styles.navigation__category}>Головна сторінка</span>
          </NavLink>
        </li>
        {/* <li className={styles.navigation__item}>
            <svg
              aria-hidden="true"
              focusable="false"
              data-prefix="fas"
              data-icon="home"
              className={styles.navigation__icon}
              role="img"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 -70 576 512"
            >
              <path
                fill="currentColor"
                d="M280.37 148.26L96 300.11V464a16 16 0 0 0 16 16l112.06-.29a16 16 0 0 0 15.92-16V368a16 16 0 0 1 16-16h64a16 16 0 0 1 16 16v95.64a16 16 0 0 0 16 16.05L464 480a16 16 0 0 0 16-16V300L295.67 148.26a12.19 12.19 0 0 0-15.3 0zM571.6 251.47L488 182.56V44.05a12 12 0 0 0-12-12h-56a12 12 0 0 0-12 12v72.61L318.47 43a48 48 0 0 0-61 0L4.34 251.47a12 12 0 0 0-1.6 16.9l25.5 31A12 12 0 0 0 45.15 301l235.22-193.74a12.19 12.19 0 0 1 15.3 0L530.9 301a12 12 0 0 0 16.9-1.6l25.5-31a12 12 0 0 0-1.7-16.93z"
              />
            </svg>
            <span className={styles.navigation__category}>Головна сторінка</span>
          </Link>
        </li> */}
        <li className={styles.navigation__item}>
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="folder"
            className={styles.navigation__icon}
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 -70 512 512"
          >
            <path
              fill="currentColor"
              d="M464 128H272l-64-64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V176c0-26.51-21.49-48-48-48z"
            />
          </svg>
          <span className={styles.navigation__category}>Продаж</span>
          <SalesList />
        </li>
        <li className={styles.navigation__item}>
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="folder"
            className={styles.navigation__icon}
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 -70 512 512"
          >
            <path
              fill="currentColor"
              d="M464 128H272l-64-64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V176c0-26.51-21.49-48-48-48z"
            />
          </svg>
          <span className={styles.navigation__category}>Відгуки</span>
          <FeedBackList newComments={newComments} />
        </li>
        <li className={styles.navigation__item}>
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="folder"
            className={styles.navigation__icon}
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 -70 512 512"
          >
            <path
              fill="currentColor"
              d="M464 128H272l-64-64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V176c0-26.51-21.49-48-48-48z"
            />
          </svg>
          <span className={styles.navigation__category}>Клієнти</span>
          <ClientsList newClients={newClients} />
        </li>
        <li className={styles.navigation__item}>
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="folder"
            className={styles.navigation__icon}
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 -70 512 512"
          >
            <path
              fill="currentColor"
              d="M464 128H272l-64-64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V176c0-26.51-21.49-48-48-48z"
            />
          </svg>
          <span className={styles.navigation__category}>Послуги</span>
          <ServiceList />
        </li>
        <li className={styles.navigation__item}>
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="folder"
            className={styles.navigation__icon}
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 -70 512 512"
          >
            <path
              fill="currentColor"
              d="M464 128H272l-64-64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V176c0-26.51-21.49-48-48-48z"
            />
          </svg>
          <span className={styles.navigation__category}>Календар подій</span>
          <CalendarList />
        </li>
        <li className={styles.navigation__item}>
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="folder"
            className={styles.navigation__icon}
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 -70 512 512"
          >
            <path
              fill="currentColor"
              d="M464 128H272l-64-64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V176c0-26.51-21.49-48-48-48z"
            />
          </svg>
          <span className={styles.navigation__category}>Галерея</span>
          <GalleryList />
        </li>
      </ul>
    </nav>
  );
};

AdminSideBar.propTypes = {
  newClients: PropTypes.number.isRequired,
  newComments: PropTypes.number.isRequired,
};

export default AdminSideBar;

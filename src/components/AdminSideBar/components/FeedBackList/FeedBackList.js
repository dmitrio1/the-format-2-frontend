import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import styles from '../../AdminSideBar.module.scss';
/* eslint-disable max-len */
const FeedBackList = ({ newComments }) => {
  const hasNewComments = newComments > 0;
  return (
    <ul className={styles.innerList}>
      <li>
        <NavLink
          activeStyle={{ color: '#ffdd15' }}
          to="/adminpanel/comments"
          className={styles.navigation__link}
        >
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="comments"
            className={styles.navigation__icon}
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 576 512"
          >
            <path
              fill="currentColor"
              d="M416 192c0-88.4-93.1-160-208-160S0 103.6 0 192c0 34.3 14.1 65.9 38 92-13.4 30.2-35.5 54.2-35.8 54.5-2.2 2.3-2.8 5.7-1.5 8.7S4.8 352 8 352c36.6 0 66.9-12.3 88.7-25 32.2 15.7 70.3 25 111.3 25 114.9 0 208-71.6 208-160zm122 220c23.9-26 38-57.7 38-92 0-66.9-53.5-124.2-129.3-148.1.9 6.6 1.3 13.3 1.3 20.1 0 105.9-107.7 192-240 192-10.8 0-21.3-.8-31.7-1.9C207.8 439.6 281.8 480 368 480c41 0 79.1-9.2 111.3-25 21.8 12.7 52.1 25 88.7 25 3.2 0 6.1-1.9 7.3-4.8 1.3-2.9.7-6.3-1.5-8.7-.3-.3-22.4-24.2-35.8-54.5z"
            />
          </svg>
          <span className={styles.navigation__category}>Всі відгуки</span>
          {hasNewComments && (
            <span className={styles.navigation__category_accent}>
              Нових:
              {newComments}
            </span>
          )}
        </NavLink>
      </li>
      <li>
        <NavLink
          activeStyle={{ color: '#ffdd15' }}
          to="/adminpanel/comment/add"
          className={styles.navigation__link}
        >
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="plus"
            className={styles.navigation__icon}
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 448 512"
          >
            <path
              fill="currentColor"
              d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"
            />
          </svg>
          <span className={styles.navigation__category}>Додати відгук</span>
        </NavLink>
      </li>
    </ul>
  );
};

FeedBackList.propTypes = {
  newComments: PropTypes.number.isRequired,
};

export default FeedBackList;

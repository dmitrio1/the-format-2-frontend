/* eslint-disable max-len */
import React from 'react';
import { NavLink } from 'react-router-dom';

import styles from '../../AdminSideBar.module.scss';

const GalleryList = () => {
  return (
    <ul className={styles.innerList}>
      <li>
        <NavLink
          activeStyle={{ color: '#ffdd15' }}
          to="/adminpanel/galleries"
          className={styles.navigation__link}
        >
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="camera"
            className={styles.navigation__icon}
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 512 512"
          >
            <path
              fill="currentColor"
              d="M512 144v288c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V144c0-26.5 21.5-48 48-48h88l12.3-32.9c7-18.7 24.9-31.1 44.9-31.1h125.5c20 0 37.9 12.4 44.9 31.1L376 96h88c26.5 0 48 21.5 48 48zM376 288c0-66.2-53.8-120-120-120s-120 53.8-120 120 53.8 120 120 120 120-53.8 120-120zm-32 0c0 48.5-39.5 88-88 88s-88-39.5-88-88 39.5-88 88-88 88 39.5 88 88z"
            />
          </svg>
          <span className={styles.navigation__category}>Всі фото</span>
        </NavLink>
      </li>
      <li>
        <NavLink
          activeStyle={{ color: '#ffdd15' }}
          to="/adminpanel/gallery/add"
          className={styles.navigation__link}
        >
          <svg
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="plus"
            className={styles.navigation__icon}
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 448 512"
          >
            <path
              fill="currentColor"
              d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"
            />
          </svg>
          <span className={styles.navigation__category}>Додати фото</span>
        </NavLink>
      </li>
    </ul>
  );
};

export default GalleryList;

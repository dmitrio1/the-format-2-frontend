import React from 'react';
import { NavLink } from 'react-router-dom';
import { string, objectOf } from 'prop-types';
import defaultStyles from './button-link.module.scss';

const ButtonLink = ({ url, text, styles }) => {
  const s = styles ? { ...defaultStyles, ...styles } : defaultStyles;
  return (
    <NavLink to={url} className={s['link-button']}>
      {text}
    </NavLink>
  );
};

ButtonLink.defaultProps = {
  styles: null,
};

ButtonLink.propTypes = {
  styles: objectOf(string),
  text: string.isRequired,
  url: string.isRequired,
};

export default ButtonLink;

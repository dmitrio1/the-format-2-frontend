import React from 'react';
import SocialIcons from '../SocialIcons';
import classes from './footer.module.css';

const Footer = () => {
  return (
    <footer className={classes.footer}>
      <div className={classes.content}>
        <p className={classes.copyright}>&copy;&nbsp;2020 Magisoft LLC. Всі права захищені.</p>
        <div className={classes.socialIcons}>
          <SocialIcons cls="footer" />
        </div>
      </div>
    </footer>
  );
};

export default Footer;

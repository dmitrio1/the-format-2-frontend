export default function getNewClientsQuantity(allClients) {
  const newClientsQuantity = allClients.reduce((acc, client) => {
    if (client.isCheck === false) {
      return acc + 1;
    }

    return acc;
  }, 0);

  return newClientsQuantity;
}

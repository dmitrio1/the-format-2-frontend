/* eslint-disable no-underscore-dangle */
import axios from 'axios';

async function getOrdersPictures() {
  const orders = await axios.get('https://68.183.77.250/admin/order/');
  return orders.data;
}

async function getOrder(id) {
  const order = await axios.get(`https://68.183.77.250/admin/order/${id}?id=${id}`);
  return order.data;
}

async function getClientServices() {
  const clients = await axios.get('https://68.183.77.250/admin/client?service=m');
  const services = clients.data.map(client => {
    return {
      services: client.service,
      phone: client.phone,
      name: client.name,
      clientId: client._id,
    };
  });
  return services;
}

async function removeClientService(id, serviceInd) {
  const { status } = await axios.delete(`https://68.183.77.250/admin/client/${id}`, {
    params: { [serviceInd]: id },
  });
  return status;
}

async function removePicture(id) {
  const { status } = await axios.delete(`https://68.183.77.250/admin/shop/delete/${id}`);
  return status;
}

async function addProduct(product) {
  const { name, size, material, price } = product;
  const query = `name=${name}&size=${size}&material=${material}&price=${price}`;
  const image = new FormData();
  image.append('image', product.image);
  const { status } = await axios.post(`https://68.183.77.250/admin/shop/add?${query}`, image, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  return status;
}

// async function buyArt(contacts) {
//   const response = await axios.post('https://68.183.77.250/shop', contacts);
//   return response;
// }

async function updateOrder(id, order) {
  const { address, name, status, date, item, price, phone, email } = order;
  // eslint-disable-next-line max-len
  const query = `id=${id}&date=${date}&status=${status}&address=${address}&name=${name}&phone=${phone}&email=${email}&item=${item}&price=${price}`;
  const response = await axios.put(`https://68.183.77.250/admin/order/${id}?${query}`, order);
  return response.status;
}

async function updateClientService(id, data) {
  const _data = { ...data, id };
  const response = await axios.put(`https://68.183.77.250/admin/client/${id}`, _data);
  return response.status;
}

async function getPictureToSale(name) {
  const order = await axios.get('https://68.183.77.250/admin/shop/all');
  return order.data.filter(picture => picture.name === name);
}

async function getPictures() {
  const order = await axios.get('https://68.183.77.250/admin/shop/all');
  return order.data;
}

async function getProduct(id) {
  const response = await axios.get(`https://68.183.77.250/admin/shop/get/${id}`);
  return response.data;
}

async function updateProduct(id, product) {
  const response = await axios.put(`https://68.183.77.250/admin/shop/edit/${id}`, product);
  return response.status;
}

async function removeOrderPictures(id) {
  const { status } = await axios.delete(`https://68.183.77.250/admin/order/${id}`, {
    params: { id },
  });
  return status;
}
// GET ===============================================================================

async function getAllClients() {
  const response = await axios.get('https://68.183.77.250/admin/client?');
  return response.data;
}

async function getClientsAdult() {
  const response = await axios.get('https://68.183.77.250/admin/client?courseType=MasterAdult');
  return response.data;
}

async function getClientsChild() {
  const response = await axios.get('https://68.183.77.250/admin/client?courseType=MasterKid');
  return response.data;
}

async function getClientsCourse() {
  const response = await axios.get('https://68.183.77.250/admin/client?courseType=Course');
  return response.data;
}

async function getAllComments() {
  const response = await axios.get('https://68.183.77.250/admin/comments/');
  return response.data;
}

async function getAllGallery() {
  const gallery = await axios.get('https://68.183.77.250/admin/gallery');
  return gallery.data;
}

async function getImageUrl(imgId) {
  const response = await axios.get(`https://68.183.77.250/admin/masterAdult/single?id=${imgId}`);

  return response.data.icon;
}

async function getCourses() {
  const response = await axios.get('https://68.183.77.250/admin/coursesMain/');
  return response.data;
}

async function getMasterAdultCourses() {
  const response = await axios.get('https://68.183.77.250/admin/masterAdult');
  return response.data;
}
async function getAdultCourseById(id) {
  const response = await axios.get(`https://68.183.77.250/admin/masterAdult/single?id=${id}`);
  return response.data;
}

async function getMasterKidsCourses() {
  const response = await axios.get('https://68.183.77.250/admin/masterKid');
  return response.data;
}

async function getServices() {
  const response = await axios.get('https://68.183.77.250/admin/service/');
  return response.data;
}

async function getCourseById(id) {
  const response = await axios.get(`https://68.183.77.250/admin/coursesMain/single/?id=${id}`);
  return response.data;
}

async function getKidsCourseById(id) {
  const response = await axios.get(`https://68.183.77.250/admin/masterKid/single?id=${id}`);
  return response.data;
}

async function getServiceById(id) {
  const response = await axios.get(`https://68.183.77.250/admin/service/single?id=${id}`);
  return response.data;
}

// POST =================================================================================

async function addGalleryPhoto(photo) {
  const formData = new FormData();
  formData.append('image', photo);
  const response = await axios.post('https://68.183.77.250/admin/gallery/add', formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  return response;
}

async function getClient(id) {
  const response = await axios.get(`https://68.183.77.250/admin/client/${id}`);
  return response.data;
}

async function addNewClientAndCourse(newClient) {
  const response = await axios.post('https://68.183.77.250/admin/client', newClient);
  return response.status;
}

async function addNewMasterKidsClient(newClient) {
  const response = await axios.post('https://68.183.77.250/admin/client/', newClient);
  return response.status;
}

async function addCourse(formValues, photo) {
  const { name, price, description } = formValues;

  const time = Date.now();
  const formData = new FormData();
  const config = {
    headers: { 'content-type': 'multipart/form-data' },
  };

  formData.append('name', name);
  formData.append('price', price);
  formData.append('description', description);
  formData.append('time', time);
  formData.append('order', time);
  formData.append('icon', photo);

  const response = await axios.post(
    'https://68.183.77.250/admin/coursesMain/single',
    formData,
    config,
  );
  return response.status;
}
async function addAdultCourse(formValues, photos) {
  const { name, imgDesc1 } = formValues;
  const { icon, img1 } = photos;

  const time = Date.now();
  const formData = new FormData();
  const config = {
    headers: { 'content-type': 'multipart/form-data' },
  };

  formData.append('name', name);
  formData.append('imgDesc1', imgDesc1);
  formData.append('order', time);
  formData.append('icon', icon);
  formData.append('img1', img1);
  formData.append('img2', img1);

  const response = await axios.post(
    'https://68.183.77.250/admin/masterAdult/single',
    formData,
    config,
  );
  return response.status;
}

async function addKidsCourse(formValues, photo) {
  const { name, price, description, duration } = formValues;

  const time = Date.now();
  const formData = new FormData();
  const config = {
    headers: { 'content-type': 'multipart/form-data' },
  };

  formData.append('name', name);
  formData.append('price', price);
  formData.append('description', description);
  formData.append('duration', duration);
  formData.append('icon', photo);
  formData.append('order', time);

  const response = await axios.post(
    'https://68.183.77.250/admin/masterKid/single',
    formData,
    config,
  );
  return response.status;
}

async function addService(formValues, photo) {
  const { name, price, description } = formValues;

  const time = Date.now();
  const formData = new FormData();
  const config = {
    headers: { 'content-type': 'multipart/form-data' },
  };

  formData.append('name', name);
  formData.append('price', price);
  formData.append('description', description);
  formData.append('icon', photo);
  formData.append('order', time);

  const response = await axios.post('https://68.183.77.250/admin/service/single', formData, config);
  return response.status;
}

// PUT ====================================================================================

async function unmarkNewUsers() {
  const options = { isCheck: { isCheck: 'false' } };
  const response = await axios.put('https://68.183.77.250/admin/client/id', options);
  return response.status;
}

async function updateClient(client) {
  const response = await axios.put(`https://68.183.77.250/admin/client/${client.id}`, client);
  return response.status;
}

async function updateCourse(courseId, formValues, photo, order) {
  const { name, price, description } = formValues;

  const formData = new FormData();
  const config = {
    headers: { 'content-type': 'multipart/form-data' },
  };

  formData.append('name', name);
  formData.append('price', price);
  formData.append('description', description);
  formData.append('time', '');
  formData.append('order', order);
  formData.append('icon', photo);

  const response = await axios.put(
    `https://68.183.77.250/admin/coursesMain/single?id=${courseId}`,
    formData,
    config,
  );
  return response.status;
}

async function updateKidsCourse(courseId, formValues, photo, order) {
  const { name, price, description, duration } = formValues;

  const formData = new FormData();
  const config = {
    headers: { 'content-type': 'multipart/form-data' },
  };

  formData.append('name', name);
  formData.append('price', price);
  formData.append('description', description);
  formData.append('duration', duration);
  formData.append('icon', photo);
  formData.append('order', order);

  const response = await axios.put(
    `https://68.183.77.250/admin/masterKid/single?id=${courseId}`,
    formData,
    config,
  );

  return response.status;
}
async function updateAdultCourse(courseId, formValues, photo) {
  const { name, info, order } = formValues;
  const { icon, img1 } = photo;

  const formData = new FormData();
  const config = {
    headers: { 'content-type': 'multipart/form-data' },
  };

  formData.append('name', name);
  formData.append('info', info);
  formData.append('icon', icon);
  formData.append('img1', img1);
  formData.append('order', order);

  const response = await axios.put(
    `https://68.183.77.250/admin/masterAdult/single?id=${courseId}`,
    formData,
    config,
  );

  return response.status;
}

async function updateService(serviceId, formValues, photo, order) {
  const { name, price, description } = formValues;

  const formData = new FormData();
  const config = {
    headers: { 'content-type': 'multipart/form-data' },
  };

  formData.append('name', name);
  formData.append('price', price);
  formData.append('description', description);
  formData.append('icon', photo);
  formData.append('order', order);

  const response = await axios.put(
    `https://68.183.77.250/admin/service/single/?id=${serviceId}`,
    formData,
    config,
  );

  return response.status;
}

// DELETE ==========================================================================================

async function removeUser(id) {
  const { status } = await axios.delete(`https://68.183.77.250/admin/client/${id}`);
  return status;
}

async function removeCourse(id) {
  const response = await axios.delete(`https://68.183.77.250/admin/coursesMain/single?id=${id}`);
  return response.status;
}

async function removeMasterKidCourse(id) {
  const response = await axios.delete(`https://68.183.77.250/admin/masterKid/single/?id=${id}`);
  return response.status;
}

async function removeService(id) {
  const response = await axios.delete(`https://68.183.77.250/admin/service/single?id=${id}`);
  return response.status;
}

export default {
  getAllClients,
  removeUser,
  getClientsAdult,
  getClientsChild,
  getClientsCourse,
  getClient,
  getImageUrl,
  updateClient,
  getCourses,
  addNewClientAndCourse,
  getMasterAdultCourses,
  getMasterKidsCourses,
  addNewMasterKidsClient,
  getAllComments,
  removeCourse,
  removeMasterKidCourse,
  getServices,
  removeService,
  addCourse,
  updateCourse,
  updateKidsCourse,
  addKidsCourse,
  getCourseById,
  addAdultCourse,
  getKidsCourseById,
  addService,
  getServiceById,
  updateService,
  getAdultCourseById,
  updateAdultCourse,
  unmarkNewUsers,
};

export {
  getOrdersPictures,
  getOrder,
  updateOrder,
  removeOrderPictures,
  getPictureToSale,
  getAllGallery,
  addGalleryPhoto,
  getClientServices,
  removeClientService,
  updateClientService,
  getPictures,
  removePicture,
  addProduct,
  getProduct,
  updateProduct,
};

export default function getNewCommentsQuantity(allComments) {
  const newCommentsQuantity = allComments.reduce((acc, comment) => {
    if (comment.isVisible === false) {
      return acc + 1;
    }

    return acc;
  }, 0);

  return newCommentsQuantity;
}

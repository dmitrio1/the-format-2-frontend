/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import AdminSideBar from 'components/AdminSideBar';
import AdminPanelHeader from 'components/AdminPanelHeader';
import {
  AdminClientsAll,
  AdminClientsCourse,
  AdminClientsAdult,
  AdminClientsChild,
  ClientUpdateForm,
  AddCourseForm,
  AddAdultCourseForm,
  AddKidsCourseForm,
} from 'pages/AdminMainPage/pages/AdminClientsPage';
import {
  CalendarEvents,
  NewEventForm,
  EditEventForm,
  NewRegularEventForm,
} from './pages/CalendarEvents';
import AllCommentsContainer from './pages/Comments/components/AllComments';
import AddComments from './pages/Comments/components/AddComments';
import EditComment from './pages/Comments/components/EditComment';
import {
  Courses,
  CoursesForAdult,
  CoursesForKids,
  ServicesBlock,
  AddCoursForm,
  EditCoursForm,
  AddCoursesAdult,
  CourseForKidsAddForm,
  CourseForKidsEditForm,
  AddServiceForm,
  EditServiceForm,
  EditCourseAdult,
} from './pages/AdminServices';
import MainPage from './pages/MainPage';
import fetchHelper from './helpers/fetchHelper';
import getNewClientsQuantity from './helpers/getNewClientsQuantity';
import getNewCommentsQuantity from './helpers/getNewCommentsQuantity';
import styles from './AdminMainPage.module.scss';
import AllPhotosContainer from './pages/Gallery/components/AllPhotos/AllPhotosContainer';
import AddPhotoContainer from './pages/Gallery/components/AddPhoto/AddPhotoContainer';
import PicturesContainer from './pages/Sales/Pictures/PicturesContainer';
// eslint-disable-next-line max-len
import EditOrderedPictureContainer from './pages/Sales/Pictures/components/EditOrderedPictureForm/EditOrderPictureContainer';
import ServicesContainer from './pages/Sales/Services/ServicesContainer';
// eslint-disable-next-line max-len
import EditOrderedServiceFormContainer from './pages/Sales/Services/compopnents/EditOrderedServiceForm/EditOrderedServiceFormContainer';
import ShopContainer from './pages/Sales/Shop/ShopContainer';
import AddProduct from './pages/Sales/Shop/compomemts/AddProduct/AddProduct';
import EditProductContainer from './pages/Sales/Shop/compomemts/EditProduct/EditProductContainer';

const AdminMainPage = () => {
  const [clients, setClients] = useState([]);
  const [comments, setComments] = useState([]);

  useEffect(() => {
    const getAllClients = async () => setClients(await fetchHelper.getAllClients());
    getAllClients();

    const getAllComments = async () => setComments(await fetchHelper.getAllComments());
    getAllComments();
  }, []);

  const deleteUser = id => {
    const removeUser = async () => {
      const response = await fetchHelper.removeUser(id);

      if (response === 200) {
        setClients(prevClients => prevClients.filter(client => client._id !== id));
      }
    };

    removeUser();
  };

  const onUpdate = () => {
    const getAllClients = async () => setClients(await fetchHelper.getAllClients());
    getAllClients();
  };

  async function unmarNewUsers() {
    const status = await fetchHelper.unmarkNewUsers();
    if (status === 200) {
      const getAllClients = async () => setClients(await fetchHelper.getAllClients());
      getAllClients();
    }
  }

  const WrappedClientsCourse = props => <AdminClientsCourse {...props} onDelete={deleteUser} />;
  const WrappedClientsAdult = props => <AdminClientsAdult {...props} onDelete={deleteUser} />;
  const WrappedClientsChild = props => <AdminClientsChild {...props} onDelete={deleteUser} />;
  const WrappedClientUpdateForm = props => <ClientUpdateForm {...props} onUpdate={onUpdate} />;
  const WrappedAddCourseForm = props => <AddCourseForm {...props} onUpdate={onUpdate} />;
  const WrappedAddAdultCourseForm = props => <AddAdultCourseForm {...props} onUpdate={onUpdate} />;
  const WrappedAddKidsCourseForm = props => <AddKidsCourseForm {...props} onUpdate={onUpdate} />;
  const WrappedClientsAll = props => (
    <AdminClientsAll
      {...props}
      clients={clients}
      onDelete={deleteUser}
      onUnmark={unmarNewUsers}
      clientsQuantity={getNewClientsQuantity(clients)}
    />
  );

  return (
    <>
      <AdminPanelHeader />
      <div className={styles.container}>
        <aside className={styles.aside}>
          <AdminSideBar
            newClients={getNewClientsQuantity(clients)}
            newComments={getNewCommentsQuantity(comments)}
          />
        </aside>
        <main className={styles.contentWrapper}>
          <Switch>
            <Route path="/adminpanel/order/:id/update" component={EditOrderedPictureContainer} />
            <Route path="/adminpanel/product/:id/update" component={EditProductContainer} />
            <Route path="/adminpanel/orders" component={PicturesContainer} />
            <Route path="/adminpanel/orders-service" component={ServicesContainer} />
            <Route
              path="/adminpanel/orderService/:id/update"
              component={EditOrderedServiceFormContainer}
            />
            <Route path="/adminpanel/products" component={ShopContainer} />
            <Route path="/adminpanel/product/add" component={AddProduct} />
            <Route path="/adminpanel/page/mainpage" component={MainPage} />
            <Route path="/adminpanel/comments" component={AllCommentsContainer} />
            <Route path="/adminpanel/comment/add" component={AddComments} />
            <Route path="/adminpanel/comment/:id/update" component={EditComment} />
            <Route path="/adminpanel/clients" component={WrappedClientsAll} />
            <Route path="/adminpanel/course-clients" component={WrappedClientsCourse} />
            <Route path="/adminpanel/adult-clients" component={WrappedClientsAdult} />
            <Route path="/adminpanel/child-clients" component={WrappedClientsChild} />
            <Route path="/adminpanel/client/:clientId/update" component={WrappedClientUpdateForm} />
            <Route path="/adminpanel/course-client/add" component={WrappedAddCourseForm} />
            <Route path="/adminpanel/adult-client/add" component={WrappedAddAdultCourseForm} />
            <Route path="/adminpanel/child-client/add" component={WrappedAddKidsCourseForm} />
            <Route path="/adminpanel/events" component={CalendarEvents} />
            <Route path="/adminpanel/event/:id/update" component={EditEventForm} />
            <Route path="/adminpanel/event/add" component={NewEventForm} />
            <Route path="/adminpanel/regular-event/add" component={NewRegularEventForm} />
            <Route path="/adminpanel/lessons" component={Courses} />
            <Route path="/adminpanel/galleries" component={AllPhotosContainer} />
            <Route path="/adminpanel/gallery/add" component={AddPhotoContainer} />
            <Route path="/adminpanel/highlight/adult" component={CoursesForAdult} />
            <Route path="/adminpanel/children" component={CoursesForKids} />
            <Route path="/adminpanel/services" component={ServicesBlock} />
            <Route path="/adminpanel/adult/add" component={AddCoursesAdult} />
            <Route path="/adminpanel/adult/:id/update" component={EditCourseAdult} />

            <Route path="/adminpanel/lesson/:courseId/update" component={EditCoursForm} />
            <Route path="/adminpanel/lesson/add" component={AddCoursForm} />
            <Route path="/adminpanel/child/add" component={CourseForKidsAddForm} />
            <Route path="/adminpanel/child/:courseId/update" component={CourseForKidsEditForm} />

            <Route path="/adminpanel/service/add" component={AddServiceForm} />
            <Route path="/adminpanel/service/:serviceId/update" component={EditServiceForm} />
          </Switch>
        </main>
      </div>
    </>
  );
};

export default AdminMainPage;

import React, { useEffect, useState } from 'react';
import AllComments from './AllComments';
import { getAllComments, deleteComment, setVisible } from '../../helpers/helpers';

const AllCommentsContainer = () => {
  const [allComments, setAllComments] = useState(null);

  useEffect(() => {
    const res = async () => {
      setAllComments(await getAllComments());
    };
    res();
  }, []);
  const handleVisible = async comment => {
    await setVisible(comment);
    setAllComments(await getAllComments());
  };
  const handlerDeleteComment = async id => {
    const resp = await deleteComment(id);
    if (resp === 200) {
      setAllComments(await getAllComments());
    }
  };
  if (!allComments) return false;
  return (
    <>
      <AllComments
        commentsList={allComments}
        handlerDeleteComment={handlerDeleteComment}
        setVisible={handleVisible}
      />
    </>
  );
};

export default AllCommentsContainer;

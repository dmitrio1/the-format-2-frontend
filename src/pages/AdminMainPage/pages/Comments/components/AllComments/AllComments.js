/* eslint-disable no-underscore-dangle */
import React from 'react';
import AdminTableButtons from 'components/AdminTableButtons';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import ConfirmButton from './ConfirmButton';
import classes from './allComments.module.scss';

const AllComments = props => {
  const { commentsList, handlerDeleteComment, setVisible } = props;

  const tHeadData = ['Id', 'Name', 'Email', 'Text', 'Is visible'];

  return (
    <>
      <h1 className={classes.title}>Список Коментарів</h1>
      <table className={classes.table}>
        <tbody>
          <tr className={classes.row}>
            {tHeadData.map((title, index) => (
              <td className={classes.tHead} key={`${Math.random() * index}`}>
                {title}
              </td>
            ))}
          </tr>
        </tbody>
        <tbody>
          {commentsList.map((comment, index) => {
            const show = comment.isVisible ? 'показується' : 'не показується';
            const toConfirm = { id: comment._id };
            return (
              <tr className={classes.row} key={comment._id}>
                <td className={classes.cell}>{index + 1}</td>
                <td className={classes.cell}>{comment.name}</td>
                <td className={classes.cell}>{comment.email}</td>
                <td className={classes.cell}>{comment.text}</td>
                <td className={classes.cell}>{show}</td>
                <td className={classes.cell}>
                  <ConfirmButton
                    onClick={() => setVisible(toConfirm)}
                    isActive={!comment.isVisible}
                  />
                </td>
                <td className={classes.cell}>
                  <AdminTableButtons
                    id={comment._id}
                    path={`comment/${comment._id}/update`}
                    onDelete={handlerDeleteComment}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="">
        <Link className={classes.addComment} to="/adminpanel/comment/add">
          Додати Коментар
        </Link>
      </div>
    </>
  );
};
AllComments.propTypes = {
  commentsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  handlerDeleteComment: PropTypes.func.isRequired,
  setVisible: PropTypes.func.isRequired,
};
export default AllComments;

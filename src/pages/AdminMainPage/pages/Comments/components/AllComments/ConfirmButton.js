import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import classes from './allComments.module.scss';

const ConfirmButton = props => {
  const { onClick, isActive } = props;

  const isDisable = cn({
    confirmBtn: isActive,
    disable: !isActive,
  });
  return (
    <button type="submit" className={classes[isDisable]} onClick={onClick} disabled={!isActive}>
      Confirm
    </button>
  );
};
ConfirmButton.propTypes = {
  isActive: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};
export default ConfirmButton;

/* eslint-disable react/forbid-prop-types */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CommentsForm from '../CommentsForm';
import { addComment } from '../../helpers/helpers';
import { fields } from '../../helpers/constants';

const AddComments = ({ history }) => {
  const [initialData, setInitialData] = useState({
    [fields.name]: '',
    [fields.email]: '',
    [fields.text]: '',
  });

  const handleSubmit = async comment => {
    await addComment(comment);
    history.push('/adminpanel/comments');
  };
  const onReset = () => {
    setInitialData({ ...initialData });
  };
  return (
    <CommentsForm
      title="Додати відгук"
      initialData={initialData}
      onSubmit={handleSubmit}
      resetFields={onReset}
    />
  );
};
AddComments.propTypes = {
  history: PropTypes.object.isRequired,
};
export default AddComments;

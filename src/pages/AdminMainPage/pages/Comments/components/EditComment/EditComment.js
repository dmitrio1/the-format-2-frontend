/* eslint-disable react/forbid-prop-types */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CommentsForm from '../CommentsForm';
import { addComment, editComment } from '../../helpers/helpers';

const EditComment = ({ match, history }) => {
  const { id } = match.params;

  const [initialData, setInitialData] = useState(null);

  useEffect(() => {
    const fetching = async () => {
      setInitialData(await editComment(id));
    };
    fetching();
  }, [id]);

  const handleSubmit = async comment => {
    await addComment(comment);
    history.push('/adminpanel/comments');
  };
  const onReset = () => {
    setInitialData({ ...initialData });
  };
  if (!initialData) {
    return false;
  }
  return (
    <CommentsForm
      title="Редагувати відгук"
      initialData={initialData}
      onSubmit={handleSubmit}
      resetFields={onReset}
    />
  );
};
EditComment.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};
export default EditComment;

import React from 'react';
import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import TextArea from 'components/Form/components/TextArea';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit';
import PropTypes from 'prop-types';
import classes from './commentsForm.module.scss';
import { nameValidator, emailValidator, textValidator } from '../../helpers/formValidator';
import { fields } from '../../helpers/constants';

const CommentsForm = props => {
  const { title, initialData, onSubmit, resetFields } = props;
  const validator = {
    [fields.name]: nameValidator,
    [fields.email]: emailValidator,
    [fields.text]: textValidator,
  };

  return (
    <>
      <h1 className={classes.title}>{title}</h1>
      <Form
        initialFormData={initialData}
        onSubmit={onSubmit}
        validator={validator}
        styles={classes}
      >
        <Input name={fields.name} labelText="Введіть автора відгуку" />
        <Input name={fields.email} labelText="Введіть email автора відгуку" />
        <TextArea name={fields.text} labelText="Введіть свій коментар" />
        <AdminButtonSaveEdit
          classes={classes}
          editBtnTxt="Очистити"
          saveBtnTxt="Зберегти"
          reset={resetFields}
        />
      </Form>
    </>
  );
};
CommentsForm.propTypes = {
  initialData: PropTypes.objectOf(PropTypes.any).isRequired,
  onSubmit: PropTypes.func.isRequired,
  resetFields: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export default CommentsForm;

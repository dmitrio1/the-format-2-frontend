import axios from 'axios';

async function getAllComments() {
  const response = await axios.get('https://68.183.77.250/admin/comments/');
  return response.data;
}
async function deleteComment(id) {
  const response = await axios.delete(`https://68.183.77.250/admin/comments/delete/?id=${id}`);
  return response.status;
}
async function setVisible(comment) {
  const response = await axios.post('https://68.183.77.250/admin/comments/confirm', comment);
  return response.status;
}
async function addComment(comment) {
  const response = await axios.post('https://68.183.77.250/admin/comments/add', comment);
  return response.status;
}
async function editComment(id) {
  const response = await axios.get(`https://68.183.77.250/admin/comments/${id}`);
  return response.data;
}
export default {
  getAllComments,
  deleteComment,
  setVisible,
  addComment,
};
export { getAllComments, deleteComment, setVisible, addComment, editComment };

import validator from 'validator';

export const nameValidator = value => {
  if (validator.isEmpty(value)) {
    return " Введіть  ім'я";
  }
  if (!validator.isLength(value, { min: 3, max: 15 })) {
    return "ім'я від 3 до 15 символів";
  }

  return null;
};

export const textValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть  Коментар';
  }
  if (!validator.isLength(value, { min: 5, max: 800 })) {
    return ' не менше 5 і не більше ніж 800 символів';
  }
  return null;
};

export const emailValidator = email => {
  if (!validator.isEmail(email)) {
    return 'Введіть коректний email';
  }

  return null;
};

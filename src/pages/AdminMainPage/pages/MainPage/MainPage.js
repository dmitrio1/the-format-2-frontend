/* eslint-disable  no-alert */
import React, { useState, useEffect } from 'react';
import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import TextArea from 'components/Form/components/TextArea';
import { getHeaderInfo, updateHeaderInfo } from './helpers/helpers';
import { fields } from './helpers/constants';
import {
  emailValidator,
  phoneValidator,
  info1Validator,
  info2Validator,
  adressValidator,
} from './helpers/formValidator';
import classes from './mainPage.module.scss';

const validator = {
  [fields.email]: emailValidator,
  [fields.phone]: phoneValidator,
  [fields.info1]: info1Validator,
  [fields.info2]: info2Validator,
  [fields.address]: adressValidator,
};

const MainPage = () => {
  const [initFormData, setInitFormData] = useState(null);

  useEffect(() => {
    const fetching = async () => {
      setInitFormData(await getHeaderInfo());
    };
    fetching();
  }, []);

  const handlerSubmit = async data => {
    const fetchPut = await updateHeaderInfo(data);
    if (fetchPut === 200) {
      (async () => {
        setInitFormData(await getHeaderInfo());
      })();

      alert("Зміни внесено і вони вже з'явилися на сайті");
    }
  };
  if (!initFormData) {
    return false;
  }
  return (
    <>
      <h1 className={classes.title}>Головна сторінка</h1>
      <Form
        initialFormData={initFormData}
        onSubmit={handlerSubmit}
        validator={validator}
        styles={classes}
      >
        <h2 className={classes.legend}>Header</h2>
        <fieldset className={classes.fieldset}>
          <Input labelText="Заголовок" name={fields.info1} />
          <TextArea labelText="Короткий опис" name={fields.info2} />
        </fieldset>
        <h2 className={classes.legend}>Контакти</h2>
        <fieldset className={classes.fieldset}>
          <Input labelText="Адреса" name={fields.address} />
          <Input labelText="Телефон" name={fields.phone} />
          <Input labelText="email" name={fields.email} />
        </fieldset>
        <h2 className={classes.legend}>SEO(головна сторінка)</h2>
        <fieldset className={classes.fieldset}>
          <Input labelText="Title" name="" isRequired={false} />
          <Input labelText="Keywords" name="" isRequired={false} />
          <TextArea labelText="Description" name="" isRequired={false} />
        </fieldset>
        <h2 className={classes.legend}>SEO(Магазин)</h2>
        <fieldset className={classes.fieldset}>
          <Input labelText="Title" name="" isRequired={false} />
          <Input labelText="Keywords" name="" isRequired={false} />
          <TextArea labelText="Description" name="" isRequired={false} />
        </fieldset>
        <h2 className={classes.legend}>SEO(Реєстрація)</h2>
        <fieldset className={classes.fieldset}>
          <Input labelText="Title" name="" isRequired={false} />
          <Input labelText="Keywords" name="" isRequired={false} />
          <TextArea labelText="Description" name="" isRequired={false} />
        </fieldset>
        <button className={classes.buttonSave} type="submit">
          Зберегти
        </button>
      </Form>
    </>
  );
};

export default MainPage;

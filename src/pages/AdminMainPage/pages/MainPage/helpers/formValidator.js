import validator from 'validator';

export const emailValidator = email => {
  if (!validator.isEmail(email)) {
    return 'Введіть коректний email';
  }

  return null;
};
export const phoneValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть номер';
  }

  return null;
};

export const info1Validator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть заголовок';
  }
  if (!validator.isLength(value, { min: 5, max: 100 })) {
    return 'від 5 до 100 символів';
  }
  return null;
};
export const info2Validator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть опис';
  }
  if (!validator.isLength(value, { min: 0, max: 700 })) {
    return 'до 700 символів';
  }
  return null;
};

export const adressValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть адресу';
  }
  if (!validator.isLength(value, { min: 5, max: 150 })) {
    return 'від 5 до 150 символів';
  }
  return null;
};

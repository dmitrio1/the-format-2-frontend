/* eslint-disable import/prefer-default-export */
const fields = {
  name: 'name',
  phone: 'phone',
  email: 'email',
  address: 'address',
  info1: 'info1',
  info2: 'info2',
  info3: 'info3',
  info4: 'info4',
  info5: 'info5',
};
export { fields };

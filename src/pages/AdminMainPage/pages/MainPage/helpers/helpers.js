import axios from 'axios';

async function getHeaderInfo() {
  const resp = await axios.get('https://68.183.77.250/headerData');
  return resp.data.site[0];
}
export async function updateHeaderInfo(data) {
  const resp = await axios.put('https://68.183.77.250/admin/info/changeinfo', data);
  return resp.status;
}
export default { getHeaderInfo };
export { getHeaderInfo };

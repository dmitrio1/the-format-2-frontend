/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { getAllGallery } from '../../../../helpers/fetchHelper';
import AllPhotos from './AllPhotos';

const AllPhotosContainer = () => {
  const [photos, SetPhotos] = useState([]);

  useEffect(() => {
    (async () => SetPhotos(await getAllGallery()))();
  }, []);

  const handlePhotoDelete = async id => {
    const response = await axios.delete(`https://68.183.77.250/admin/gallery/delete?id=${id}`);
    if (response.status === 200) SetPhotos(await getAllGallery());
  };

  return <AllPhotos photos={photos} deletePhoto={handlePhotoDelete} />;
};

export default AllPhotosContainer;

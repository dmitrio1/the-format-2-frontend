/* eslint-disable no-underscore-dangle */
import React from 'react';
import { func, arrayOf, number, string, shape } from 'prop-types';
import { NavLink } from 'react-router-dom';
import styles from './allPhotos.module.scss';
import bucket from '../../assets/bucket.svg';
import AdminGalleryLayout from '../../../../../../components/AdminGalleryLayout/AdminGalleryLayout';

const AllPhotos = ({ photos, deletePhoto }) => {
  if (photos) {
    photos = photos.map(photo => {
      return (
        <div className={styles.photo} key={photo._id}>
          <img
            className={styles.image}
            src={`https://68.183.77.250${photo.images}`}
            alt="click on bucket to delete"
          />
          <img
            aria-hidden="true"
            className={styles.bucket}
            src={bucket}
            alt="bucket"
            onClick={() => deletePhoto(photo._id)}
          />
        </div>
      );
    });
  }

  return (
    <AdminGalleryLayout title="Всі фото">
      <div className={styles.wrapper}>
        <div className={styles.photos}>{photos}</div>
        <NavLink to="/adminpanel/gallery/add" className={styles['add-photo-button']}>
          Додати фото
        </NavLink>
      </div>
    </AdminGalleryLayout>
  );
};

AllPhotos.propTypes = {
  deletePhoto: func.isRequired,
  photos: arrayOf(
    shape({
      __v: number.isRequired,
      _id: string.isRequired,
    }),
  ).isRequired,
};
export default AllPhotos;

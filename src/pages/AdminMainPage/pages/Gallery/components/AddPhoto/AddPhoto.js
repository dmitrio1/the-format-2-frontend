import React, { useState } from 'react';
import AdminGalleryLayout from 'components/AdminGalleryLayout/index';
import { func } from 'prop-types';
import styles from './add-photo.module.scss';
import uploadImg from '../../assets/load.svg';

const AddPhoto = ({ handleAddPhoto }) => {
  const [icon, setIcon] = useState(uploadImg);
  const [photo, setPhoto] = useState(uploadImg);

  const handleSubmit = e => {
    e.preventDefault();
    handleAddPhoto(photo);
  };

  const handleUpload = e => {
    setIcon(URL.createObjectURL(e.target.files[0]));
    setPhoto(e.target.files[0]);
  };

  return (
    <AdminGalleryLayout title="Додати фото">
      <div className={styles.wrapper}>
        <img className={styles.uploadImg} src={icon} alt="upload" />
        <p>Перетягніть зображення сюди</p>
        <form onSubmit={handleSubmit} id="uploadForm" encType="multipart/form-data">
          <input
            className={styles.fileUploader}
            accept=".jpg, .png, .JPG"
            placeholder="Виберіть файл"
            type="file"
            onChange={e => handleUpload(e)}
          />
        </form>

        <div className={styles.info}>
          <p>Recomended size 370*370 for default theme</p>
          <p className={styles.recommendation}>JPG or PNG format</p>
        </div>

        <button className={styles['add-photo-button']} form="uploadForm" type="submit">
          Додати Фото
        </button>
      </div>
    </AdminGalleryLayout>
  );
};

AddPhoto.defaultProps = {};

AddPhoto.propTypes = {
  handleAddPhoto: func.isRequired,
};

export default AddPhoto;

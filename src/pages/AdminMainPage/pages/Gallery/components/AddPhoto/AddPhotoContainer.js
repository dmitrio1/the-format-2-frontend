import React from 'react';
import { addGalleryPhoto } from '../../../../helpers/fetchHelper';
import AddPhoto from './AddPhoto';

const AddPhotoContainer = props => {
  const handleAddPhoto = async photo => {
    const res = await addGalleryPhoto(photo);
    if (res.status === 200) {
      // eslint-disable-next-line react/prop-types
      props.history.push('/adminpanel/galleries');
    }
    return new Error('error add photo');
  };
  return <AddPhoto handleAddPhoto={handleAddPhoto} />;
};

AddPhotoContainer.defaultProps = {};

AddPhotoContainer.propTypes = {};

export default AddPhotoContainer;

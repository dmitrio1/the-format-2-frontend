import validator from 'validator';

export const nameValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть  назву';
  }
  if (!validator.isLength(value, { min: 3, max: 30 })) {
    return 'Назва від 3 до 30 символів';
  }

  return null;
};

export const priceValidator = value => {
  if (value.includes(',')) {
    return ' Вартість має бути одна';
  }
  if (validator.isEmpty(value)) {
    return ' Введіть вартість';
  }
  return null;
};

export const textValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть  Опис';
  }
  if (!validator.isLength(value, { min: 5, max: 800 })) {
    return ' не менше 5 і не більше ніж 800 символів';
  }
  return null;
};

export const durationValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть  тривалість';
  }
  return null;
};

import axios from 'axios';

async function getCourses() {
  const response = axios.get('https://68.183.77.250/admin/coursesMain/');
  return response.data;
}

export default { getCourses };

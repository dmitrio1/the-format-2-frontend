import Courses from './components/Courses';
import CoursesForAdult from './components/CoursesForAdult';
import CoursesForKids from './components/CoursesForKids';
import ServicesBlock from './components/ServicesBlock';
import AddCoursForm from './components/CourseForms/AddCoursForm';
import EditCoursForm from './components/CourseForms/EditCoursForm';
import AddCoursesAdult from './components/AddCoursesAdult';
import CourseForKidsAddForm from './components/CourseForKidsForms/CourseForKidsAddForm';
import CourseForKidsEditForm from './components/CourseForKidsForms/CourseForKidsEditForm';
import AddServiceForm from './components/ServiceForms/AddServiceForm';
import EditServiceForm from './components/ServiceForms/EditServiceForm';
import EditCourseAdult from './components/EditCourseAdult';

export {
  Courses,
  CoursesForAdult,
  CoursesForKids,
  ServicesBlock,
  AddCoursForm,
  EditCoursForm,
  AddCoursesAdult,
  CourseForKidsAddForm,
  CourseForKidsEditForm,
  AddServiceForm,
  EditServiceForm,
  EditCourseAdult,
};

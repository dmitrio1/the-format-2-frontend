/* eslint-disable react/forbid-prop-types */
import React, { useState } from 'react';
import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import TextArea from 'components/Form/components/TextArea';
import PropTypes from 'prop-types';
import Upload from './uploadBox/Upload';
import classes from '../AdminServicesForm.module.scss';
import fetchHelper from '../../../helpers/fetchHelper';

import { nameValidator, textValidator } from '../helpers/formValidator';

const initialData = {
  name: '',
  imgDesc1: '',
  text: '',
};
const validator = {
  name: nameValidator,
  imgDesc1: textValidator,
  text: textValidator,
};

const AddCoursesAdult = ({ history }) => {
  const [photos, setPhoto] = useState(null);

  const handlerSubmit = async formData => {
    const resp = await fetchHelper.addAdultCourse(formData, photos);
    if (resp === 200) {
      history.push('/adminpanel/highlight/adult');
    }
  };

  function handleAddPhoto(e) {
    e.persist();
    setPhoto(p => {
      return {
        ...p,
        [e.target.name]: e.target.files[0],
      };
    });
  }

  return (
    <div className={classes.formWrapper}>
      <Form
        initialFormData={initialData}
        validator={validator}
        onSubmit={handlerSubmit}
        styles={classes}
      >
        <h1 className={classes.title}>Додати майстерклас</h1>
        <Input placeholder="Введіть автора відгуку" isRequired={false} name="name" labelText="" />
        <div className={classes.formTop}>
          <div className={classes.upload}>
            <p className={classes.formTitle}>Додайте іконку</p>
            <Upload acept=".svg" handleAddPhoto={handleAddPhoto} name="icon">
              SVG
            </Upload>
          </div>
          <div className={classes.uploadDescription}>
            <TextArea labelText="Додаткова інформація" name="text" isRequired={false} />
            <button className={classes.buttonSave} type="submit">
              Зберегти
            </button>
          </div>
        </div>

        <div className={classes.addPic}>
          {/* <button type="button">Зображення</button> */}
          <div className={classes.content}>
            <p className={classes.formTitle}>Додайте картину</p>
            <Upload acept=".jpg, .gif, .png" handleAddPhoto={handleAddPhoto} name="img1">
              Recomended size 570*570 for default theme JPG, GIF or PNG format
            </Upload>
            <TextArea name="imgDesc1" labelText="Опис" />
          </div>
        </div>
      </Form>
    </div>
  );
};
AddCoursesAdult.propTypes = {
  history: PropTypes.object.isRequired,
};

export default AddCoursesAdult;

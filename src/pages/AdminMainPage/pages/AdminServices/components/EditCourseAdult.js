/* eslint-disable react/forbid-prop-types */

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import fetchHelper from '../../../helpers/fetchHelper';
import CourseAdultForm from './CourseAdultForm';

const EditCourseAdult = ({ history, match }) => {
  const { id } = match.params;
  const [formData, setFormData] = useState(null);
  const [icons, setIcons] = useState(null);

  const handlerSubmit = async (data, photo) => {
    if (!photo) {
      photo = icons;
    }

    const put = await fetchHelper.updateAdultCourse(id, data, photo);
    if (put === 200) {
      history.push('/adminpanel/highlight/adult');
    }
  };
  useEffect(() => {
    const response = async () => {
      const data = await fetchHelper.getAdultCourseById(id);
      setFormData({
        name: data.name,
        text: '',
        info: data.desc[0].imgDesc,
        order: data.order,
      });

      setIcons({
        icon: data.icon,
        img1: data.desc[0].image,
      });
    };
    response();
  }, [id]);
  if (!formData || !icons) {
    return false;
  }

  return (
    <>
      <CourseAdultForm
        history={history}
        onSubmit={handlerSubmit}
        initialData={formData}
        icons={icons}
        title="Редагувати майстер клас"
      />
    </>
  );
};
EditCourseAdult.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};
export default EditCourseAdult;

import React from 'react';
import PropTypes, { bool } from 'prop-types';

import uploadImg from '../assets/load.svg';
import styles from './UploadBox.module.scss';

const UploadBox = ({ handleAddPhoto, icon, isDynamicIcon }) => {
  return (
    <div className={styles.uploadBox}>
      <div className={styles.uploadImgBox}>
        {/* @HavryliukAndrii I bit change a code to show loaded image instead
            uploadImg when photo is uploaded into input.
            If you cath some errors or bags after my changes, tell me.
            -----------------------------------------------------------------
            PS: It will be great if you do reuseble component from this
            component
        */}
        {isDynamicIcon ? (
          <img className={styles.uploadImg} src={icon || uploadImg} alt="upload" />
        ) : (
          <img
            className={styles.uploadImg}
            src={icon ? `https://68.183.77.250/${icon}` : uploadImg}
            alt="upload"
          />
        )}
      </div>
      <p className={styles.uploadText}>Перетягніть зображення сюди</p>
      <input
        className={styles.fileUploader}
        accept=".svg, .png, .jpeg"
        placeholder="Виберіть файл"
        type="file"
        onChange={e => handleAddPhoto(e)}
      />

      <div className={styles.info}>
        <p className={styles.recommendation}>SVGformat</p>
      </div>
    </div>
  );
};

UploadBox.defaultProps = {
  icon: null,
  isDynamicIcon: false,
};

UploadBox.propTypes = {
  handleAddPhoto: PropTypes.func.isRequired,
  icon: PropTypes.string,
  isDynamicIcon: bool,
};

export default UploadBox;

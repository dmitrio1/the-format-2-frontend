import React from 'react';
import PropTypes from 'prop-types';

import uploadImg from '../assets/load.svg';
import styles from './UploadBox.module.scss';

const Upload = ({ handleAddPhoto, name, children, acept, icon }) => {
  return (
    <div className={styles.uploadBox}>
      <div className={styles.uploadImgBox}>
        <img
          className={styles.uploadImg}
          src={icon ? `https://68.183.77.250/${icon}` : uploadImg}
          alt="upload"
        />
      </div>
      <p className={styles.uploadText}>Перетягніть зображення сюди</p>
      <input
        className={styles.fileUploader}
        accept={acept}
        placeholder="Виберіть файл"
        type="file"
        onChange={e => handleAddPhoto(e)}
        name={name}
      />

      <div className={styles.info}>
        <p className={styles.recommendation}>{children}</p>
      </div>
    </div>
  );
};
Upload.defaultProps = {
  icon: '',
};
Upload.propTypes = {
  acept: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  handleAddPhoto: PropTypes.func.isRequired,
  icon: PropTypes.string,

  name: PropTypes.string.isRequired,
};

export default Upload;

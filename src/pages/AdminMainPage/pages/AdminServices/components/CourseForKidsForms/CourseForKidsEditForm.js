/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import TextArea from 'components/Form/components/TextArea';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit';

import fetchHelper from '../../../../helpers/fetchHelper';
import {
  nameValidator,
  priceValidator,
  textValidator,
  durationValidator,
} from '../../helpers/formValidator';
import UploadBox from '../uploadBox/UploadBox';
import styles from '../../AdminServicesForm.module.scss';

const initialData = {
  name: '',
  duration: '',
  price: '',
  description: '',
};

const CourseForKidsEditForm = ({ history, match }) => {
  const { courseId } = match.params;

  const [course, setCourse] = useState({});
  const [photo, setPhoto] = useState(null);
  const [initialFormData, setInitialFormData] = useState(initialData);
  // eslint-disable-next-line no-unused-vars
  const [error, setError] = useState(null);

  useEffect(() => {
    const getCourse = async () => {
      const _course = await fetchHelper.getKidsCourseById(courseId);
      setCourse(_course);
      setInitialFormData({
        name: _course.name,
        duration: _course.duration,
        price: _course.price,
        description: _course.description,
      });
      setPhoto(_course.icon);
    };
    getCourse();
  }, [courseId]);

  const validator = {
    name: nameValidator,
    duration: durationValidator,
    price: priceValidator,
    description: textValidator,
  };

  async function handleSubmit(formValues) {
    if (!photo) {
      return;
    }

    const status = await fetchHelper.updateKidsCourse(courseId, formValues, photo, course.order);

    if (status === 200) {
      history.replace('/adminpanel/children');
      return;
    }

    setError('Something went wrong');
  }

  function handleAddPhoto(e) {
    setPhoto(e.target.files[0]);
  }

  function resetFields() {
    setInitialFormData({ ...initialFormData });
  }

  return (
    <section className={styles.wrapper}>
      <h2>Додати курс</h2>
      <div className={styles.contentWrapper}>
        <UploadBox handleAddPhoto={handleAddPhoto} icon={course.icon} />
        <Form initialFormData={initialFormData} validator={validator} onSubmit={handleSubmit}>
          <Input placeholder="Введіть назву майстеркласу" name="name" labelText="Назва" />
          <Input
            placeholder="Введіть тривалість майстеркласу"
            name="duration"
            labelText="Tривалість"
          />
          <Input placeholder="Введіть вартість майстеркласу" name="price" labelText="Вартість" />

          <TextArea placeholder="Введіть пояснення" name="description" labelText="Опис" />

          <AdminButtonSaveEdit
            classes={styles}
            editBtnTxt="Очистити"
            saveBtnTxt="Зберегти"
            reset={resetFields}
          />
        </Form>
      </div>
    </section>
  );
};

CourseForKidsEditForm.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      courseId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default CourseForKidsEditForm;

import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import TextArea from 'components/Form/components/TextArea';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit';

import fetchHelper from '../../../../helpers/fetchHelper';
import {
  nameValidator,
  priceValidator,
  textValidator,
  durationValidator,
} from '../../helpers/formValidator';
import UploadBox from '../uploadBox/UploadBox';
import styles from '../../AdminServicesForm.module.scss';

const initialData = {
  name: '',
  duration: '',
  price: '',
  description: '',
};

const CourseForKidsAddForm = ({ history }) => {
  const [photo, setPhoto] = useState(null);
  const [initialFormData, setInitialFormData] = useState(initialData);
  // eslint-disable-next-line no-unused-vars
  const [error, setError] = useState(null);

  const validator = {
    name: nameValidator,
    duration: durationValidator,
    price: priceValidator,
    description: textValidator,
  };

  function handleAddPhoto(e) {
    setPhoto(e.target.files[0]);
  }

  async function handleSubmit(formValues) {
    if (!photo) {
      return;
    }

    const status = await fetchHelper.addKidsCourse(formValues, photo);

    if (status === 200) {
      history.replace('/adminpanel/children');
      return;
    }

    setError('Something went wrong');
  }

  function resetFields() {
    setInitialFormData({ ...initialData });
  }

  return (
    <section className={styles.wrapper}>
      <h2>Додати курс</h2>
      <div className={styles.contentWrapper}>
        <UploadBox handleAddPhoto={handleAddPhoto} />
        <Form initialFormData={initialFormData} validator={validator} onSubmit={handleSubmit}>
          <Input placeholder="Введіть назву майстеркласу" name="name" labelText="Назва" />
          <Input
            placeholder="Введіть тривалість майстеркласу"
            name="duration"
            labelText="Tривалість"
          />
          <Input placeholder="Введіть вартість майстеркласу" name="price" labelText="Вартість" />

          <TextArea placeholder="Введіть пояснення" name="description" labelText="Опис" />

          <AdminButtonSaveEdit
            classes={styles}
            editBtnTxt="Очистити"
            saveBtnTxt="Зберегти"
            reset={resetFields}
          />
        </Form>
      </div>
    </section>
  );
};

CourseForKidsAddForm.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired,
  }).isRequired,
};

export default CourseForKidsAddForm;

import React from 'react';
import { NavLink } from 'react-router-dom';

import styles from './CoursesFilterButtons.module.scss';

const CoursesFilterButtons = () => {
  return (
    <>
      <NavLink
        activeStyle={{ color: '#282929', backgroundColor: '#cacbcd' }}
        to="/adminpanel/highlight/adult"
        className={styles.filterBtn}
      >
        Всі
      </NavLink>
      <NavLink
        activeStyle={{ color: '#282929', backgroundColor: '#cacbcd' }}
        to="/adminpanel/mk"
        className={styles.filterBtn}
      >
        МК
      </NavLink>
      <NavLink
        activeStyle={{ color: '#282929', backgroundColor: '#cacbcd' }}
        to="/adminpanel/art"
        className={styles.filterBtn}
      >
        Живопис
      </NavLink>
    </>
  );
};

export default CoursesFilterButtons;

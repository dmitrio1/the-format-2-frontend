import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import TextArea from 'components/Form/components/TextArea';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit';

import fetchHelper from '../../../../helpers/fetchHelper';
import { nameValidator, priceValidator, textValidator } from '../../helpers/formValidator';
import UploadBox from '../uploadBox/UploadBox';
import styles from '../../AdminServicesForm.module.scss';

const initialData = {
  name: '',
  price: '',
  description: '',
};

const AddServiceForm = ({ history }) => {
  const [photo, setPhoto] = useState(null);
  const [initialFormData, setInitialFormData] = useState(initialData);
  // eslint-disable-next-line no-unused-vars
  const [error, setError] = useState(null);

  const validator = {
    name: nameValidator,
    price: priceValidator,
    description: textValidator,
  };

  function handleAddPhoto(e) {
    setPhoto(e.target.files[0]);
  }

  async function handleSubmit(formValues) {
    if (!photo) {
      return;
    }

    const status = await fetchHelper.addService(formValues, photo);

    if (status === 200) {
      history.replace('/adminpanel/services');
      return;
    }

    setError('Something went wrong');
  }

  function resetFields() {
    setInitialFormData({ ...initialData });
  }

  return (
    <section className={styles.wrapper}>
      <h2>Додати послугу</h2>
      <div className={styles.contentWrapper}>
        <UploadBox handleAddPhoto={handleAddPhoto} />
        <Form initialFormData={initialFormData} validator={validator} onSubmit={handleSubmit}>
          <Input placeholder="Введіть назву" name="name" labelText="Назва Послуги" />
          <Input placeholder="Введіть вартість" name="price" labelText="Вартість" />

          <TextArea placeholder="Введіть пояснення" name="description" labelText="Опис" />

          <AdminButtonSaveEdit
            classes={styles}
            editBtnTxt="Очистити"
            saveBtnTxt="Зберегти"
            reset={resetFields}
          />
        </Form>
      </div>
    </section>
  );
};

AddServiceForm.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired,
  }).isRequired,
};

export default AddServiceForm;

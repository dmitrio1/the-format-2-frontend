/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import TextArea from 'components/Form/components/TextArea';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit';

import fetchHelper from '../../../../helpers/fetchHelper';
import { nameValidator, priceValidator, textValidator } from '../../helpers/formValidator';
import UploadBox from '../uploadBox/UploadBox';
import styles from '../../AdminServicesForm.module.scss';

const initialData = {
  name: '',
  price: '',
  description: '',
};

const EditServiceForm = ({ history, match }) => {
  const { serviceId } = match.params;

  const [service, setService] = useState({});
  const [photo, setPhoto] = useState(null);
  const [initialFormData, setInitialFormData] = useState(initialData);
  // eslint-disable-next-line no-unused-vars
  const [error, setError] = useState(null);

  useEffect(() => {
    const getService = async () => {
      const _service = await fetchHelper.getServiceById(serviceId);
      setService(_service);
      setInitialFormData({
        name: _service.name,
        price: _service.price[0],
        description: _service.description,
      });
      setPhoto(_service.icon);
    };
    getService();
  }, [serviceId]);

  const validator = {
    name: nameValidator,
    price: priceValidator,
    description: textValidator,
  };

  async function handleSubmit(formValues) {
    if (!photo) {
      return;
    }

    const status = await fetchHelper.updateService(serviceId, formValues, photo, service.order);

    if (status === 200) {
      history.replace('/adminpanel/services');
      return;
    }

    setError('Something went wrong');
  }

  function handleAddPhoto(e) {
    setPhoto(e.target.files[0]);
  }

  function resetFields() {
    setInitialFormData({ ...initialFormData });
  }

  return (
    <section className={styles.wrapper}>
      <h2>Додати послугу</h2>
      <div className={styles.contentWrapper}>
        <UploadBox handleAddPhoto={handleAddPhoto} icon={service.icon} />
        <Form initialFormData={initialFormData} validator={validator} onSubmit={handleSubmit}>
          <Input placeholder="Введіть назву" name="name" labelText="Назва Послуги" />
          <Input placeholder="Введіть вартість" name="price" labelText="Вартість" />

          <TextArea placeholder="Введіть пояснення" name="description" labelText="Опис" />

          <AdminButtonSaveEdit
            classes={styles}
            editBtnTxt="Очистити"
            saveBtnTxt="Зберегти"
            reset={resetFields}
          />
        </Form>
      </div>
    </section>
  );
};

EditServiceForm.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      serviceId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default EditServiceForm;

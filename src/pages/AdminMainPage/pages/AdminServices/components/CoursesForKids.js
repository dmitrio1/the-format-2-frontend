/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';

import AdminTableButtons from 'components/AdminTableButtons';

import fetchHelper from '../../../helpers/fetchHelper';
import AddNewButton from './AddNewButton';
import styles from '../AdminServices.module.scss';

const CoursesForKids = () => {
  const [courses, setCourses] = useState([]);
  // eslint-disable-next-line no-unused-vars
  const [error, setError] = useState(null);

  useEffect(() => {
    const getCourses = async () => {
      const _courses = await fetchHelper.getMasterKidsCourses();
      setCourses(_courses);
    };
    getCourses();
  }, []);

  async function handleDelete(id) {
    const status = await fetchHelper.removeMasterKidCourse(id);
    if (status === 200) {
      const getCourses = async () => {
        const _courses = await fetchHelper.getMasterKidsCourses();
        setCourses(_courses);
      };
      getCourses();
      return;
    }
    setError('Something went wrong');
  }

  return (
    <section>
      <h2 className={styles.serviceTitle}>Список майстер-класів для дітей</h2>
      {courses.length !== 0 && (
        <ul className={styles.serviceList}>
          <li className={styles.serviceList__itemKids_head}>
            <p>Назва</p>
            <p>Тривалість</p>
            <p>Вартість</p>
            <p>Опис</p>
          </li>
          {courses.map(course => {
            return (
              <li key={course._id} className={styles.serviceList__itemKids}>
                <p>{course.name}</p>
                <p>{course.duration}</p>
                <p>{course.price}</p>
                <p>{course.description}</p>

                <AdminTableButtons
                  path={`/adminpanel/child/${course._id}/update`}
                  id={course._id}
                  onDelete={handleDelete}
                />
              </li>
            );
          })}
        </ul>
      )}
      <div className={styles.btnBox}>
        <AddNewButton buttonText="Додати майстер-клас" path="/adminpanel/child/add" />
      </div>
    </section>
  );
};

export default CoursesForKids;

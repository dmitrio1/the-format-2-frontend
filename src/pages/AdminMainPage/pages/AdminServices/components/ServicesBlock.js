/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';

import AdminTableButtons from 'components/AdminTableButtons';

import fetchHelper from '../../../helpers/fetchHelper';
import AddNewButton from './AddNewButton';
import styles from '../AdminServices.module.scss';

const ServicesBlock = () => {
  const [services, setServices] = useState([]);
  // eslint-disable-next-line no-unused-vars
  const [error, setError] = useState(null);

  useEffect(() => {
    const getServices = async () => {
      const _services = await fetchHelper.getServices();
      setServices(_services);
    };
    getServices();
  }, []);

  async function handleDelete(id) {
    const status = await fetchHelper.removeService(id);
    if (status === 200) {
      const getServices = async () => {
        const _services = await fetchHelper.getServices();
        setServices(_services);
      };
      getServices();
      return;
    }
    setError('Something went wrong');
  }

  return (
    <section>
      <h2 className={styles.serviceTitle}>Список послуг</h2>
      {services.length !== 0 && (
        <ul className={styles.serviceList}>
          <li className={styles.serviceList__itemServices_head}>
            <p>№</p>
            <p>Назва</p>
            <p>Сума</p>
            <p>Опис</p>
          </li>
          {services.map((service, idx) => {
            return (
              <li key={service._id} className={styles.serviceList__itemServices}>
                <p>{idx + 1}.</p>
                <p>{service.name}</p>
                <p>
                  {service.price.map(price => {
                    return `${price} `;
                  })}
                </p>
                <p>{service.description}</p>

                <AdminTableButtons
                  path={`/adminpanel/service/${service._id}/update`}
                  id={service._id}
                  onDelete={handleDelete}
                />
              </li>
            );
          })}
        </ul>
      )}
      <div className={styles.btnBox}>
        <AddNewButton buttonText="Додати послугу" path="/adminpanel/service/add" />
      </div>
    </section>
  );
};

export default ServicesBlock;

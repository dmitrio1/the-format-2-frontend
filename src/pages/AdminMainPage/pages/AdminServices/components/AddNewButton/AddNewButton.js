import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import styles from './AddNewButton.module.scss';

const AddNewButton = ({ buttonText, path }) => {
  return (
    <NavLink to={path} className={styles.btn}>
      {buttonText}
    </NavLink>
  );
};

AddNewButton.propTypes = {
  buttonText: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
};

export default AddNewButton;

/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';

import AdminTableButtons from 'components/AdminTableButtons';
import CoursesFilterButtons from './CoursesFilterButtons';

import fetchHelper from '../../../helpers/fetchHelper';
import AddNewButton from './AddNewButton';
import styles from '../AdminServices.module.scss';

const CoursesForAdult = () => {
  const [courses, setCourses] = useState([]);
  // eslint-disable-next-line no-unused-vars
  const [error, setError] = useState(null);

  useEffect(() => {
    const getCourses = async () => {
      const _courses = await fetchHelper.getMasterAdultCourses();
      setCourses(_courses);
    };
    getCourses();
  }, []);

  async function handleDelete(id) {
    const status = await fetchHelper.removeCourse(id);
    if (status === 200) {
      const getCourses = async () => {
        const _courses = await fetchHelper.getMasterAdultCourses();
        setCourses(_courses);
      };
      getCourses();
      return;
    }
    setError('Something went wrong');
  }

  return (
    <section>
      <h2 className={styles.serviceTitle}>Список майстеркласів для дорослих</h2>
      <div className={styles.filterBtnBox}>
        <CoursesFilterButtons />
      </div>
      {courses.length !== 0 && (
        <ul className={styles.serviceList}>
          <li className={styles.serviceList__item_head}>
            <p>Назва</p>
            <p>Інфо</p>
          </li>
          {courses.map(course => {
            return (
              <li key={course._id} className={styles.serviceList__item}>
                <p>{course.name}</p>
                <p>{course.desc.length !== 0 ? course.desc[0].imgDesc : ''}</p>

                <AdminTableButtons
                  path={`/adminpanel/adult/${course._id}/update`}
                  id={course._id}
                  onDelete={handleDelete}
                />
              </li>
            );
          })}
        </ul>
      )}
      <div className={styles.btnBox}>
        <AddNewButton buttonText="Додати" path="/adminpanel/adult/add" />
      </div>
    </section>
  );
};

export default CoursesForAdult;

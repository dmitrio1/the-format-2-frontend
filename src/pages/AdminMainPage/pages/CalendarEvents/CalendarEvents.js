/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import CalendarEventsTable from './helpers/CalendarEventsTable';
import helpers from './helpers/helpers';
import classes from './calendarEvents.module.scss';

const CalendarEvents = () => {
  const [events, setEvents] = useState([]);

  const handlerDeleteEvent = async id => {
    const res = await helpers.delEvent(id);

    if (res === 200) {
      const fetching = async () => {
        setEvents(await helpers.getEvents());
      };
      fetching();
    }
  };

  useEffect(() => {
    const fetching = async () => {
      setEvents(await helpers.getEvents());
    };
    fetching();
  }, []);
  return (
    <>
      <CalendarEventsTable eventsList={events} handlerDeleteEvent={handlerDeleteEvent} />
      <Link to="/adminpanel/event/add">
        <button className={classes.addEventBtn} type="button">
          Додати подію
        </button>
      </Link>
    </>
  );
};

export default CalendarEvents;

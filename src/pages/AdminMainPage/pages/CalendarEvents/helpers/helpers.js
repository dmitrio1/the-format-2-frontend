import axios from 'axios';

const getEvents = async () => {
  const response = await axios.get('https://68.183.77.250/admin/event/');
  const data = response.data;
  return data;
};
const delEvent = async id => {
  const response = await axios.delete(`https://68.183.77.250/admin/event/delete/${id}`);

  return response.status;
};
const addEvent = eventData => {
  return axios.post('https://68.183.77.250/admin/event/add', eventData);
};
const getEventForEdit = async id => {
  const response = await axios.get(`https://68.183.77.250/admin/event/${id}`);

  return response.data;
};
const putEvent = (id, eventData) => {
  return axios.put(`https://68.183.77.250/admin/event/update/${id}`, eventData);
};
export default { getEvents, delEvent, addEvent, getEventForEdit, putEvent };

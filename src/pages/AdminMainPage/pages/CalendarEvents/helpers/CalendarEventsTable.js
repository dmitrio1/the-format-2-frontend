/* eslint-disable no-underscore-dangle */
import React from 'react';
import AdminTableButtons from 'components/AdminTableButtons';
import PropTypes from 'prop-types';
import classes from './calendarEventsTable.module.scss';

const CalendarEventsTable = props => {
  const { eventsList, handlerDeleteEvent } = props;
  return (
    <>
      <h1 className={classes.title}>Список Подій</h1>
      <table className={classes.table}>
        <tbody>
          <tr className={classes.row}>
            <td className={classes.tHead}>Id</td>
            <td className={classes.tHead}>Name</td>
            <td className={classes.tHead}>Date</td>
            <td className={classes.tHead}>Time</td>
          </tr>
        </tbody>
        <tbody>
          {eventsList.map((event, index) => (
            <tr className={classes.row} key={event._id}>
              <td className={classes.cell}>{index + 1}</td>
              <td className={classes.cell}>{event.name}</td>
              <td className={classes.cell}>{event.date}</td>
              <td className={classes.cell}>{event.time}</td>
              <td className={classes.cell}>
                <AdminTableButtons
                  id={event._id}
                  path={`event/${event._id}/update`}
                  onDelete={handlerDeleteEvent}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};
CalendarEventsTable.propTypes = {
  eventsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  handlerDeleteEvent: PropTypes.func.isRequired,
};
export default CalendarEventsTable;

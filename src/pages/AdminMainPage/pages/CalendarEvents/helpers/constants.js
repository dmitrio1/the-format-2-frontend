/* eslint-disable import/prefer-default-export */
const fields = {
  name: 'name',
  date: 'date',
  time: 'time',
  description: 'description',
  isRegular: 'isRegular',
};

export { fields };

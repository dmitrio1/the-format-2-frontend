import validator from 'validator';
// dd.mm.yyyy

const patternDate = /^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$/;
// hh:mm-hh:mm
const patternTime = /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]-(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
export const nameValidator = name => {
  if (!validator.isLength(name, { min: 2, max: 40 })) {
    return "Ім'я має складатися не менш ніж з 2 та не більше ніж з 40 символів";
  }

  if (validator.isEmpty(name)) {
    return "Введіть ім'я";
  }
  return null;
};
export const dateValidator = date => {
  if (!validator.matches(date, patternDate)) {
    return 'Введіть коректну дату';
  }
  if (validator.isEmpty(date)) {
    return 'Введіть дату';
  }
  return null;
};
export const timeValidator = time => {
  if (!validator.matches(time, patternTime)) {
    return 'Введіть коректний час';
  }
  if (validator.isEmpty(time)) {
    return 'Введіть час проведення події';
  }
  return null;
};
export const descriptionValidator = description => {
  /* eslint-disable operator-linebreak */
  if (!validator.isLength(description, { min: 10, max: 10000 })) {
    return 'Опис має складатися не менш ніж  з 10 та не більше ніж з 1000 символів';
  }
  if (validator.isEmpty(description)) {
    return 'Введіть опис';
  }
  return null;
};
export default {};

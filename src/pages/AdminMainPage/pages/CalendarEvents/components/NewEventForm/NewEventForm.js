import React, { useState } from 'react';
import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import TextArea from 'components/Form/components/TextArea';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit';
import PropTypes from 'prop-types';
import { fields } from '../../helpers/constants';
import helpers from '../../helpers/helpers';
import classes from './newEvent.module.css';
import {
  nameValidator,
  dateValidator,
  timeValidator,
  descriptionValidator,
} from '../../helpers/validatorOrders';

const validator = {
  [fields.name]: nameValidator,
  [fields.date]: dateValidator,
  [fields.time]: timeValidator,
  [fields.description]: descriptionValidator,
};
const initialFormData = {
  [fields.name]: '',
  [fields.date]: '',
  [fields.time]: '',
  [fields.description]: '',
};
const NewEventForm = ({ history }) => {
  const [formInfo, setFormInfo] = useState(initialFormData);

  const handleCancel = () => {
    setFormInfo({ ...initialFormData });
  };
  const handlerSubmit = formData => {
    helpers.addEvent(formData).then(() => {
      history.push('/adminpanel/events');
    });
  };
  return (
    <>
      <h1>Додати подію</h1>
      <Form
        initialFormData={formInfo}
        onSubmit={handlerSubmit}
        validator={validator}
        styles={classes}
      >
        <Input name={fields.name} labelText="Назва Події" />
        <Input name={fields.date} labelText="Дата" placeholder="дата у форматі  26.03.2020" />
        <Input name={fields.time} labelText="Час" placeholder="час у форматі  11:00-12:10" />
        <TextArea name={fields.description} labelText="Опис" />
        <AdminButtonSaveEdit editBtnTxt="Очистити" saveBtnTxt="Зберегти" reset={handleCancel} />
      </Form>
    </>
  );
};
NewEventForm.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};
export default NewEventForm;

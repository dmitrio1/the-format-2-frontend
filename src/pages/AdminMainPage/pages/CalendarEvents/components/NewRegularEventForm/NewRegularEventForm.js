import React, { useState } from 'react';
// import { withRouter } from 'react-router-dom';
import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import TextArea from 'components/Form/components/TextArea';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit';
import PropTypes from 'prop-types';
import { fields } from '../../helpers/constants';
import helpers from '../../helpers/helpers';
import classes from './newRegularEventForm.module.css';
import {
  nameValidator,
  dateValidator,
  timeValidator,
  descriptionValidator,
} from '../../helpers/validatorOrders';

const validator = {
  [fields.name]: nameValidator,
  [fields.date]: dateValidator,
  [fields.time]: timeValidator,
  [fields.description]: descriptionValidator,
};
const initialFormData = {
  [fields.name]: '',
  [fields.date]: '',
  [fields.time]: '',
  [fields.description]: '',
  [fields.isRegular]: true,
};
const NewEventForm = ({ history }) => {
  const [formInfo, setFormInfo] = useState(initialFormData);

  const handleCancel = () => {
    setFormInfo({ ...formInfo });
  };

  const handlerSubmit = formData => {
    helpers.addEvent(formData).then(() => {
      history.push('/adminpanel/events');
    });
  };
  return (
    <>
      <h1>Додати регулярну подію</h1>
      <Form
        initialFormData={formInfo}
        onSubmit={handlerSubmit}
        validator={validator}
        styles={classes}
      >
        <Input name={fields.name} labelText="Назва регулярної події" />
        <Input name={fields.date} labelText="Дата" />
        <Input name={fields.time} labelText="Час" />
        <TextArea name={fields.description} labelText="Опис події" />
        <AdminButtonSaveEdit editBtnTxt="Очистити" saveBtnTxt="Зберегти" reset={handleCancel} />
      </Form>
    </>
  );
};
NewEventForm.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};
export default NewEventForm;

/* eslint-disable react/forbid-prop-types */
import React, { useEffect, useState } from 'react';
import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import Textarea from 'components/Form/components/TextArea';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit';
import PropTypes from 'prop-types';
import classes from './editEventForm.module.css';
import { fields } from '../../helpers/constants';
import helpers from '../../helpers/helpers';
import {
  nameValidator,
  dateValidator,
  timeValidator,
  descriptionValidator,
} from '../../helpers/validatorOrders';

const validator = {
  [fields.name]: nameValidator,
  [fields.date]: dateValidator,
  [fields.time]: timeValidator,
  [fields.description]: descriptionValidator,
};

const EditEventForm = ({ match, history }) => {
  const { id } = match.params;
  const [formInfo, setFormInfo] = useState(null);

  useEffect(() => {
    const getEventData = async () => {
      const result = await helpers.getEventForEdit(id);
      setFormInfo(result);
    };

    getEventData();
  }, [id]);

  const handleCancel = () => {
    setFormInfo({ ...formInfo });
  };

  if (!formInfo) {
    return false;
  }
  const submitionHandler = formData => {
    helpers.putEvent(id, formData).then(() => {
      history.push('/adminpanel/events');
    });
  };
  return (
    <>
      <h1 className={classes.title}>Редагувати подію</h1>
      <Form
        initialFormData={formInfo}
        onSubmit={submitionHandler}
        validator={validator}
        styles={classes}
      >
        <Input name={fields.name} labelText="Назва події" />
        <Input name={fields.time} labelText="Час" placeholder="час у форматі  11:00-12:10" />
        <Input name={fields.date} labelText="Дата" placeholder="дата у форматі  26.03.2020" />
        <Textarea name={fields.description} labelText="Опис події" />
        <AdminButtonSaveEdit editBtnTxt="Очистити" saveBtnTxt="Зберегти" reset={handleCancel} />
      </Form>
    </>
  );
};
EditEventForm.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};
export default EditEventForm;

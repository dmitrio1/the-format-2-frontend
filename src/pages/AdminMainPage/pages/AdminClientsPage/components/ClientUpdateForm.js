/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import ButtonSubmit from 'components/Form/components/ButtonSubmit';

import { nameValidator, phoneValidator, emailValidator } from '../helpers/validateClientsForm';
import fetchHelper from '../../../helpers/fetchHelper';
import styles from '../AdminClients.module.scss';

const ClientUpdateForm = ({ match, history, onUpdate }) => {
  const [client, setClient] = useState(null);
  const [picture, setPicture] = useState(null);

  const { clientId } = match.params;

  useEffect(() => {
    const getClient = async () => {
      const _client = await fetchHelper.getClient(clientId);
      setClient(_client);

      const hasCourse = _client.course.length !== 0;
      if (!hasCourse) {
        return;
      }

      const isMasterAdult = _client.course[0].courseType === 'MasterAdult';
      if (!isMasterAdult) {
        return;
      }

      const { picId } = _client.course[0];
      if (!picId) {
        return;
      }

      const { courseId } = _client.course[0];
      const _picture = await fetchHelper.getImageUrl(courseId);
      setPicture(_picture);
    };

    getClient();
  }, [clientId]);

  if (!client) {
    return null;
  }

  const validator = {
    name: nameValidator,
    phone: phoneValidator,
    email: emailValidator,
  };

  function handleSubmit({ _id: id, phone, name, email }) {
    const editedClient = { id, phone, email, name };

    const updateClient = async () => {
      const status = await fetchHelper.updateClient(editedClient);

      if (status === 200) {
        onUpdate();
        history.replace('/adminpanel/clients');
      }
    };

    updateClient();
  }

  const returnInitialValues = () => {
    setClient({ ...client });
  };

  return (
    <section className={styles.formContainer}>
      <h2 className={styles.formTitle}>Редагувати клієнта</h2>
      {client && (
        <Form
          initialFormData={client}
          onSubmit={handleSubmit}
          styles={styles}
          validator={validator}
          shouldClean
        >
          <Input name="name" labelText="Прізвище та ім'я:" isRequired />
          <Input name="email" labelText="Електронна адреса:" isRequired />
          <Input name="phone" labelText="Телефон:" isRequired />

          {picture && (
            <div className={styles.imgBox}>
              <img className={styles.img} src={`https://68.183.77.250${picture}`} alt="" />
            </div>
          )}

          <div className={styles.editBtnBox}>
            <ButtonSubmit text="Зберегти" />
            <button type="button" className={styles.clearBtn} onClick={returnInitialValues}>
              Очистити
            </button>
          </div>
        </Form>
      )}
    </section>
  );
};

ClientUpdateForm.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      clientId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  onUpdate: PropTypes.func.isRequired,
};

export default ClientUpdateForm;

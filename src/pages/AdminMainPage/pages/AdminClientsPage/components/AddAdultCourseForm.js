/* eslint-disable jsx-a11y/no-onchange */
/* eslint-disable no-unused-vars */
/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import ButtonSubmit from 'components/Form/components/ButtonSubmit';
import Select from 'components/Form/components/Select';

import {
  nameValidator,
  phoneValidator,
  emailValidator,
  courseValidator,
} from '../helpers/validateClientsForm';
import fetchHelper from '../../../helpers/fetchHelper';
import styles from '../AddForm.module.scss';

const initialValues = {
  name: '',
  phone: '+380',
  email: '',
};

const AddAdultCourseForm = ({ history, onUpdate }) => {
  const [initialFormData, setInitialFormData] = useState(initialValues);
  const [courses, setCourses] = useState([]);
  const [courseId, setCourseId] = useState(null);
  const [courseError, setCourseError] = useState('');
  const [picId, setPicId] = useState(null);
  const [picError, setPicError] = useState('');

  useEffect(() => {
    const getMasterAdultCourses = async () => {
      const _courses = await fetchHelper.getMasterAdultCourses();
      setCourses(_courses);
    };
    getMasterAdultCourses();
  }, []);

  const validator = {
    name: nameValidator,
    phone: phoneValidator,
    email: emailValidator,
  };

  function handleSubmit({ name, phone, email }) {
    if (!courseId || courseId === '') {
      setCourseError('Оберіть курс');
      return;
    }

    if (!picId) {
      setPicError('Оберіть картину');
      return;
    }

    const newClient = { name, phone, email, courseId, picId, courseType: 'MasterAdult' };

    const addNewClient = async () => {
      const status = await fetchHelper.addNewClientAndCourse(newClient);

      if (status === 200) {
        onUpdate();
        history.replace('/adminpanel/clients');
      }
    };
    addNewClient();
  }

  function handleSelectChange(e) {
    setCourseError('');
    setPicError('');
    setCourseId(e.target.value);
    setPicId(null);
  }

  function handleRadioChange(e) {
    setPicError('');
    setPicId(e.target.value);
  }

  function returnInitialValues() {
    setInitialFormData({ ...initialValues });
    setCourseError('');
    setPicError('');
    setCourseId(null);
    setPicId(null);
  }

  function getPictures() {
    const currentCourse = courses.find(course => course._id === courseId);

    return currentCourse.desc.map(pictureInfo => {
      return (
        <div key={pictureInfo._id} className={styles.onePictureContainer}>
          <input type="radio" name="picId" onChange={handleRadioChange} value={pictureInfo._id} />
          <div className={styles.imgBox}>
            <img
              className={styles.img}
              src={`https://68.183.77.250/${pictureInfo.image}`}
              alt={pictureInfo.imgDesc}
            />
          </div>
        </div>
      );
    });
  }

  return (
    <section>
      <h2>Додати клієнта на майстеркласс для дорослих</h2>
      {initialFormData && (
        <Form
          initialFormData={initialFormData}
          onSubmit={handleSubmit}
          styles={styles}
          validator={validator}
          shouldClean
        >
          <Input name="name" labelText="Прізвище та ім'я:" isRequired />
          <Input name="email" labelText="Електронна адреса:" isRequired />
          <Input name="phone" labelText="Телефон:" isRequired />

          <div className={styles.inputWrapper}>
            <p className={styles.inputTitle}>
              Оберіть курс:
              <span className={styles.inputTitle_warn}> * </span>
              <span className={styles.inputTitle_warn}>{courseError}</span>
            </p>
            <select
              name="courseId"
              id="courseId"
              onChange={handleSelectChange}
              className={styles.select}
            >
              <option />
              {courses.map(course => (
                <option value={course._id} key={course._id}>
                  {course.name}
                </option>
              ))}
            </select>
          </div>

          {courseId && (
            <div className={styles.inputWrapper}>
              <p className={styles.inputTitle}>
                Оберіть картину:
                <span className={styles.inputTitle_warn}> * </span>
                <span className={styles.inputTitle_warn}>{picError}</span>
              </p>
              {getPictures()}
            </div>
          )}

          <div className={styles.editBtnBox}>
            <ButtonSubmit text="Зберегти" />
            <button type="button" className={styles.clearBtn} onClick={returnInitialValues}>
              Очистити
            </button>
          </div>
        </Form>
      )}
    </section>
  );
};

AddAdultCourseForm.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired,
  }).isRequired,
  onUpdate: PropTypes.func.isRequired,
};

export default AddAdultCourseForm;

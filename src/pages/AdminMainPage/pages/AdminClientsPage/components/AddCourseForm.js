/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import ButtonSubmit from 'components/Form/components/ButtonSubmit';
import Select from 'components/Form/components/Select';

import fetchHelper from '../../../helpers/fetchHelper';
import {
  nameValidator,
  phoneValidator,
  emailValidator,
  courseValidator,
} from '../helpers/validateClientsForm';
import styles from '../AddForm.module.scss';

const initialValues = {
  name: '',
  phone: '+380',
  email: '',
  courseId: '',
};

const AddCourseForm = ({ history, onUpdate }) => {
  const [initialFormData, setInitialFormData] = useState(initialValues);
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    const getCourses = async () => {
      const _courses = await fetchHelper.getCourses();
      setCourses(_courses);
    };
    getCourses();
  }, []);

  useEffect(() => {
    if (courses.length === 0) {
      return;
    }
    setInitialFormData({ ...initialValues });
  }, [courses]);

  const validator = {
    name: nameValidator,
    phone: phoneValidator,
    email: emailValidator,
    courseId: courseValidator,
  };

  function handleSubmit({ name, phone, email, courseId }) {
    const newClient = { name, phone, email, courseId, courseType: 'Course' };

    const addNewClient = async () => {
      const status = await fetchHelper.addNewClientAndCourse(newClient);

      if (status === 200) {
        onUpdate();
        history.replace('/adminpanel/clients');
      }
    };
    addNewClient();
  }

  function returnInitialValues() {
    setInitialFormData({ ...initialValues });
  }

  const options = courses.map(course => (
    <option value={course._id} key={course._id}>
      {course.name}
    </option>
  ));
  const optionsList = [<option key="1" />, ...options];

  return (
    <section>
      <h2>Додати клієнта на курс</h2>
      {initialFormData && (
        <Form
          initialFormData={initialFormData}
          onSubmit={handleSubmit}
          styles={styles}
          validator={validator}
          shouldClean
        >
          <Input name="name" labelText="Прізвище та ім'я:" isRequired />
          <Input name="email" labelText="Електронна адреса:" isRequired />
          <Input name="phone" labelText="Телефон:" isRequired />
          <Select
            name="courseId"
            type="select"
            isRequired
            labelText="Виберіть курс"
            optionsList={optionsList}
          />

          <div className={styles.editBtnBox}>
            <ButtonSubmit text="Зберегти" />
            <button type="button" className={styles.clearBtn} onClick={returnInitialValues}>
              Очистити
            </button>
          </div>
        </Form>
      )}
    </section>
  );
};

AddCourseForm.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired,
  }).isRequired,
  onUpdate: PropTypes.func.isRequired,
};

export default AddCourseForm;

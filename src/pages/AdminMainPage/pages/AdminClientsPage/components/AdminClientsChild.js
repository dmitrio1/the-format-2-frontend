import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import ClientsTable from '../helpers/AdminClientsTable';
import fetchHelper from '../../../helpers/fetchHelper';

const AdminClientsChild = ({ onDelete }) => {
  const [clients, setClients] = useState([]);
  // const [error, setError] = useState(null);

  useEffect(() => {
    const getClientsChild = async () => setClients(await fetchHelper.getClientsChild());
    getClientsChild();
  }, []);

  return (
    <ClientsTable
      clients={clients}
      onDelete={onDelete}
      type="Type Master Kid"
      extraTitle="(мк для дітей)"
    />
  );
};

AdminClientsChild.propTypes = {
  onDelete: PropTypes.func.isRequired,
};

export default AdminClientsChild;

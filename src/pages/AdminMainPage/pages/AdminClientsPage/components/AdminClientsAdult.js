import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import ClientsTable from '../helpers/AdminClientsTable';
import fetchHelper from '../../../helpers/fetchHelper';

const AdminClientsAdult = ({ onDelete }) => {
  const [clients, setClients] = useState([]);
  // const [error, setError] = useState(null);

  useEffect(() => {
    const getClientsAdult = async () => setClients(await fetchHelper.getClientsAdult());
    getClientsAdult();
  }, []);

  return (
    <ClientsTable
      clients={clients}
      onDelete={onDelete}
      type="Type Master Adult"
      extraTitle="(мк для дорослих)"
    />
  );
};

AdminClientsAdult.propTypes = {
  onDelete: PropTypes.func.isRequired,
};

export default AdminClientsAdult;

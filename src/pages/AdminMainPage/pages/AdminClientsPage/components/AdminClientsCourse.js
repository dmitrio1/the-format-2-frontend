import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import ClientsTable from '../helpers/AdminClientsTable';
import fetchHelper from '../../../helpers/fetchHelper';

const AdminClientsCourse = ({ onDelete }) => {
  const [clients, setClients] = useState([]);
  // const [error, setError] = useState(null);

  useEffect(() => {
    const getClientsCourse = async () => setClients(await fetchHelper.getClientsCourse());
    getClientsCourse();
  }, []);

  return (
    <ClientsTable clients={clients} onDelete={onDelete} type="Type course" extraTitle="(курси)" />
  );
};

AdminClientsCourse.propTypes = {
  onDelete: PropTypes.func.isRequired,
};

export default AdminClientsCourse;

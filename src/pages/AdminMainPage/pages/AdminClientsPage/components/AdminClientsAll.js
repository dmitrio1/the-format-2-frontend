import React from 'react';
import PropTypes from 'prop-types';

import ClientsTable from '../helpers/AdminClientsTable';

const AdminClientsAll = ({ clients, onDelete, onUnmark, clientsQuantity }) => {
  return (
    <ClientsTable
      clients={clients}
      onDelete={onDelete}
      onUnmark={onUnmark}
      clientsQuantity={clientsQuantity}
      type="Course"
      extraTitle=""
    />
  );
};

AdminClientsAll.propTypes = {
  clients: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }).isRequired,
  ).isRequired,
  clientsQuantity: PropTypes.number.isRequired,
  onDelete: PropTypes.func.isRequired,
  onUnmark: PropTypes.func.isRequired,
};

export default AdminClientsAll;

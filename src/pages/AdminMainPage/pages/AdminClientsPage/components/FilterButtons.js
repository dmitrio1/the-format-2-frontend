import React from 'react';
import { NavLink } from 'react-router-dom';

import styles from '../AdminClients.module.scss';

const FilterButtons = () => {
  return (
    <>
      <button type="button" className={styles.filterBtn}>
        <NavLink to="/adminpanel/clients" className={styles.filterBtnLink}>
          All
        </NavLink>
      </button>
      <button type="button" className={styles.filterBtn}>
        <NavLink to="/adminpanel/course-clients" className={styles.filterBtnLink}>
          Course
        </NavLink>
      </button>
      <button type="button" className={styles.filterBtn}>
        <NavLink to="/adminpanel/adult-clients" className={styles.filterBtnLink}>
          Adult
        </NavLink>
      </button>
      <button type="button" className={styles.filterBtn}>
        <NavLink to="/adminpanel/child-clients" className={styles.filterBtnLink}>
          Child
        </NavLink>
      </button>
    </>
  );
};

export default FilterButtons;

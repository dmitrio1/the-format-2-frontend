import validator from 'validator';

export const nameValidator = value => {
  if (validator.isEmpty(value)) {
    return " Введіть  ім'я";
  }
  if (!validator.isLength(value, { min: 3, max: 15 })) {
    return "ім'я від 3 до 15 символів";
  }

  return null;
};

export const phoneValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть номер';
  }

  if (!validator.isMobilePhone(value, 'uk-UA', { strictMode: true })) {
    return ' Номер телефону не коректний';
  }
  return null;
};

export const emailValidator = email => {
  if (!validator.isEmail(email)) {
    return 'Введіть коректний email';
  }

  return null;
};

export const courseValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Оберіть курс ';
  }
  return null;
};

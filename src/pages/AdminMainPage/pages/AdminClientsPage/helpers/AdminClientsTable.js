/* eslint-disable no-underscore-dangle */
/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import PropTypes from 'prop-types';
import AdminTableButtons from 'components/AdminTableButtons';

import styles from '../AdminClients.module.scss';
import FilterButtons from '../components/FilterButtons';

const ClientsTable = ({ clients, onDelete, onUnmark, type, extraTitle, clientsQuantity }) => {
  const isCourseRoute = type === 'Course';
  const hasNewClients = clientsQuantity !== 0;

  return (
    <section>
      <h2 className={styles.clientsTitle}>Список клієнтів {extraTitle}</h2>
      <div className={styles.filterBtnBox}>
        <FilterButtons />
        {isCourseRoute && hasNewClients && (
          <button type="button" className={styles.unmarkBtn} onClick={() => onUnmark()}>
            Unmark new users
          </button>
        )}
      </div>
      {clients.length !== 0 && (
        <ul className={styles.clientsList}>
          <li className={styles.clientsList__item}>
            <p>Id</p>
            <p>Name</p>
            <p>Email</p>
            <p>Phone</p>
            <p>{type}</p>
          </li>
          {clients.map((client, idx) => {
            return (
              <li key={client._id} className={styles.clientsList__item}>
                <p>{idx + 1}.</p>
                <p>
                  {client.name}
                  {!client.isCheck && <span className={styles.clientsList_new}>New</span>}
                </p>
                <p>{client.email}</p>
                <p>{client.phone}</p>
                {isCourseRoute && <p>{client.course[0] ? client.course[0].courseType : ''}</p>}
                {!isCourseRoute && <p>{client.course[0].courseName}</p>}

                <AdminTableButtons
                  path={`/adminpanel/client/${client._id}/update`}
                  id={client._id}
                  onDelete={onDelete}
                />
              </li>
            );
          })}
        </ul>
      )}
    </section>
  );
};

ClientsTable.propTypes = {
  clients: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }).isRequired,
  ).isRequired,
  clientsQuantity: PropTypes.number.isRequired,
  extraTitle: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  onUnmark: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
};

export default ClientsTable;

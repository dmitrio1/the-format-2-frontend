import validator from 'validator';

function validateClientUpdateForm(formData) {
  const { name, phone, email } = formData;

  const errors = {};

  if (!validator.isLength(name, { min: 2, max: 40 })) {
    errors.name = " Ім'я має складатися не менш ніж з 2 та не більше ніж з 40 символів";
  }

  if (validator.isEmpty(name)) {
    errors.name = " Введіть ім'я";
  }

  if (!validator.isEmail(email)) {
    errors.email = ' Будь ласка, введіть коректний email';
  }

  if (validator.isEmpty(email)) {
    errors.email = ' Будь ласка, введіть email';
  }

  if (!validator.isMobilePhone(phone, 'uk-UA', { strictMode: true })) {
    errors.phone = ' Номер телефону не корректний';
  }
  if (validator.isEmpty(phone)) {
    errors.phone = ' Введіть номер';
  }

  return errors;
}

export default validateClientUpdateForm;

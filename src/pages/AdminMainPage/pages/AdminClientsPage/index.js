import AdminClientsAll from './components/AdminClientsAll';
import AdminClientsCourse from './components/AdminClientsCourse';
import AdminClientsAdult from './components/AdminClientsAdult';
import AdminClientsChild from './components/AdminClientsChild';
import ClientUpdateForm from './components/ClientUpdateForm';
import AddCourseForm from './components/AddCourseForm';
import AddAdultCourseForm from './components/AddAdultCourseForm';
import AddKidsCourseForm from './components/AddKidsCourseForm';

export {
  AdminClientsAll,
  AdminClientsCourse,
  AdminClientsAdult,
  AdminClientsChild,
  ClientUpdateForm,
  AddCourseForm,
  AddAdultCourseForm,
  AddKidsCourseForm,
};

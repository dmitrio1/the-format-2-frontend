import React, { useEffect, useState } from 'react';
import { getClientServices, removeClientService } from '../../../helpers/fetchHelper';
import Services from './Services';

const ServicesContainer = () => {
  const [services, setServices] = useState([]);

  useEffect(() => {
    (async () => {
      setServices(await getClientServices());
    })();
  }, []);

  const handleDeleteOrder = async (id, serviceInd) => {
    const status = await removeClientService(id, serviceInd);
    if (status === 200) {
      setServices(await getClientServices());
    }
    return new Error('delete are not successful');
  };

  return <Services clientServices={services} onDelete={handleDeleteOrder} />;
};

ServicesContainer.defaultProps = {};

ServicesContainer.propTypes = {};

export default ServicesContainer;

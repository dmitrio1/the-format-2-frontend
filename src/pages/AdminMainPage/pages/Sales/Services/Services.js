/* eslint-disable no-underscore-dangle */
import React from 'react';
import { arrayOf, string, number, shape, func } from 'prop-types';
import styles from './services.module.scss';
import AdminTableButtons from '../../../../../components/AdminTableButtons/AdminTableButtons';

const Services = React.memo(({ clientServices, onDelete }) => {
  return (
    <section>
      <h2 className={styles.title}>Список замовлень послуг</h2>
      {clientServices.length !== 0 && (
        <ul className={styles.clientsList}>
          <li className={styles.clientsList__item}>
            <p>ID</p>
            <p>Назва</p>
            <p>Замовник</p>
            <p>Телефон</p>
            <p>Статус</p>
            <p>Дата</p>
          </li>

          {clientServices.map((clientService, idx) => {
            return clientService.services.map((service, ind) => {
              return (
                <li key={service._id} className={styles.clientsList__item}>
                  <p>{idx + 1}</p>
                  <p>{service.serviceName}</p>
                  <p>{clientService.name}</p>
                  <p>{clientService.phone}</p>
                  <p>{service.status}</p>
                  <p>{service.time}</p>

                  <AdminTableButtons
                    path={`/adminpanel/orderService/${clientService.clientId}/update`}
                    id={service._id}
                    onDelete={() => onDelete(clientServices[0].clientId, ind)}
                  />
                </li>
              );
            });
          })}
        </ul>
      )}
    </section>
  );
});

Services.defaultProps = {};

Services.propTypes = {
  clientServices: arrayOf(
    shape({
      _id: string,
      date: string,
      status: string,
      name: string,
      phone: string,
      email: string,
      item: string,
      price: number,
      __v: number,
    }),
  ).isRequired,
  onDelete: func.isRequired,
};

export default Services;

/* eslint-disable react/forbid-prop-types */
import React, { useState } from 'react';
import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit';
import { func, object } from 'prop-types';
import classes from './edit-ordered-service.module.scss';
import { fields } from './helpers/constants';
import {
  phoneValidator,
  nameValidator,
  emailValidator,
} from '../../../../../../../components/Form/helpers/validator';
import CustomSelect from '../../../../../../../components/Form/components/CustomSelect';
import { statuses } from '../../../Pictures/components/EditOrderedPictureForm/helpers/constants';

const validator = {
  [fields.phone]: phoneValidator,
  [fields.name]: nameValidator,
  [fields.email]: emailValidator,
};

const EditOrderedServiceForm = ({ clientService, submissionHandler }) => {
  // eslint-disable-next-line no-unused-vars
  const [service, setService] = useState(clientService);

  const initialFormData = {
    [fields.serviceName]: clientService.service[0].serviceName,
    [fields.address]: clientService.address,
    [fields.phone]: clientService.phone,
    [fields.email]: clientService.email,
    [fields.certificateSum]: '',
    [fields.status]: clientService.service[0].status,
    [fields.name]: clientService.name,
  };

  return (
    <>
      <h1 className={classes.title}>Редагувати замовлення (послуги)</h1>
      <Form
        initialFormData={initialFormData}
        onSubmit={submissionHandler}
        validator={validator}
        styles={classes}
      >
        <CustomSelect name={fields.status} options={statuses} defOptInd={0} isRequired={false} />
        <Input name={fields.serviceName} labelText="Назва" isDisabled />
        <Input
          name={fields.certificateSum}
          labelText="Сума сертифікату"
          isDisabled
          isRequired={false}
        />
        <Input name={fields.name} labelText="Ім'я клієнта" />
        <Input name={fields.email} labelText="Email" />
        <Input name={fields.phone} labelText="Phone" />
        <Input name={fields.address} labelText="Address" isRequired={false} />
        <AdminButtonSaveEdit
          editBtnTxt="Очистити"
          saveBtnTxt="Зберегти"
          reset={() => setService(initialFormData)}
        />
      </Form>
    </>
  );
};

EditOrderedServiceForm.propTypes = {
  clientService: object.isRequired,
  submissionHandler: func.isRequired,
};
export default EditOrderedServiceForm;

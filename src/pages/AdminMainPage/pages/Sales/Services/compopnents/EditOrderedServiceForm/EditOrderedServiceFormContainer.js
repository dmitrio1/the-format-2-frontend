/* eslint-disable react/forbid-prop-types */
import React, { useEffect, useState } from 'react';
import { object } from 'prop-types';
import EditOrderedServiceForm from './EditOrderedServiceForm';
import helpers, { updateClientService } from '../../../../../helpers/fetchHelper';

const EditOrderedServiceFormContainer = ({ match, history }) => {
  const id = match.params.id;
  const [clientService, setClientService] = useState(null);

  useEffect(() => {
    const getClientServiceData = async () => {
      const result = await helpers.getClient(id);
      setClientService(result);
    };

    getClientServiceData();
  }, [id]);

  const submissionHandler = formData => {
    updateClientService(id, formData).then(() => {
      history.push('/adminpanel/orders-service');
    });
  };

  return clientService ? (
    <EditOrderedServiceForm clientService={clientService} submissionHandler={submissionHandler} />
  ) : null;
};

EditOrderedServiceFormContainer.defaultProps = {};

EditOrderedServiceFormContainer.propTypes = {
  history: object.isRequired,
  match: object.isRequired,
};

export default EditOrderedServiceFormContainer;

/* eslint-disable import/prefer-default-export */
export const fields = {
  status: 'status',
  name: 'name',
  address: 'address',
  phone: 'phone',
  email: 'email',
  certificateSum: 'certificateSum',
  serviceName: 'serviceName',
};

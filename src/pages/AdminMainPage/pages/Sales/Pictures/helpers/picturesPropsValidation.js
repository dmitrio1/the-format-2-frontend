/* eslint-disable import/prefer-default-export */
import { shape, string, number } from 'prop-types';

const pictureOrder = shape({
  _id: string.isRequired,
  date: string.isRequired,
  status: string.isRequired,
  name: string.isRequired,
  phone: string.isRequired,
  email: string.isRequired,
  item: string.isRequired,
  price: number.isRequired,
  __v: number.isRequired,
});

export { pictureOrder };

import React, { useEffect, useState } from 'react';
import { getOrdersPictures, removeOrderPictures } from '../../../helpers/fetchHelper';
import Pictures from './Pictures';

const PicturesContainer = () => {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    (async () => {
      setOrders(await getOrdersPictures());
    })();
  }, []);

  const handleDeleteOrder = async id => {
    const status = await removeOrderPictures(id);
    if (status === 200) {
      setOrders(await getOrdersPictures());
    }
    return new Error('delete are not successful');
  };

  return <Pictures orders={orders} onDelete={handleDeleteOrder} />;
};

PicturesContainer.defaultProps = {};

PicturesContainer.propTypes = {};

export default PicturesContainer;

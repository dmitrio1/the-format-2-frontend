import React from 'react';
import FormTable from './FormTable/FormTable';
import { fields } from '../helpers/constants';

const CustomerSection = () => {
  const columns = ["Ім'я", 'Телефон', 'Email'];

  const fieldsData = [{ name: fields.name }, { name: fields.phone }, { name: fields.email }];

  return (
    <div className="wrapper">
      <FormTable title="Покупець" fieldsData={fieldsData} columns={columns} />
    </div>
  );
};

CustomerSection.defaultProps = {};

CustomerSection.propTypes = {};

export default CustomerSection;

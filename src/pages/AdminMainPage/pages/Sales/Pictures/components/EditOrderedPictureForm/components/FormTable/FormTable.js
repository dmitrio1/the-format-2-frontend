/* eslint-disable react/no-array-index-key */
import React from 'react';
import { arrayOf, string, shape } from 'prop-types';
import Input from 'components/Form/components/Input';
import Img from 'components/Form/components/Img';
import styles from './form-table.module.scss';

const FormTable = ({ title, columns, fieldsData }) => {
  const columnTitles = columns.map(columnTitle => {
    return (
      <div className={styles['column-title']} key={columnTitle}>
        {columnTitle}
      </div>
    );
  });

  const fields = fieldsData.map((field, ind) => {
    return (
      <div key={ind}>
        {!field.type && (
          <div className={`${styles.field} ${styles[field.className] || ''}`} key={ind}>
            <Input name={field.name} isRequired={false} isDisabled={field.disabled} />
          </div>
        )}
        {field.type === 'img' && (
          <div className={`${styles['field-image']} ${styles[field.className] || ''}`} key={ind}>
            <Img
              name={field.name}
              className={styles.image}
              alt="заказаный продукт"
              isRequired={false}
            />
          </div>
        )}
      </div>
    );
  });

  return (
    <div className={styles.wrapper}>
      <div className={styles['form-title']}>{title}</div>
      <div className={styles.form}>
        <div className={styles.header}>{columnTitles}</div>

        <div className={styles.body}>{fields}</div>
      </div>
    </div>
  );
};

FormTable.defaultProps = {};

FormTable.propTypes = {
  columns: arrayOf(string).isRequired,

  fieldsData: arrayOf(
    shape({
      name: string.isRequired,
      classNames: string,
    }),
  ).isRequired,

  title: string.isRequired,
};

export default FormTable;

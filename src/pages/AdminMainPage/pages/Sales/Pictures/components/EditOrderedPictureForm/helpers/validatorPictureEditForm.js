/* eslint-disable import/prefer-default-export */
import validator from 'validator';

export const addressValidator = value => {
  if (validator.isEmpty(value)) {
    return 'Введіть адрес';
  }

  return null;
};

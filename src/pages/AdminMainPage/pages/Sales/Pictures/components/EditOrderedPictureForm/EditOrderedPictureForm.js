/* eslint-disable react/forbid-prop-types */
import React, { useState } from 'react';
import Form from 'components/Form';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit/index';
import { func } from 'prop-types';
import styles from './edit-ordered-picture-form.module.scss';
import { pictureOrder } from '../../helpers/picturesPropsValidation';

import { fields, statuses } from './helpers/constants';
import InfoSection from './components/InfoSection';
import CustomerSection from './components/CustomerSection';
import {
  emailValidator,
  nameValidator,
  notValidate,
} from '../../../../../../../components/Form/helpers/validator';
// eslint-disable-next-line max-len
import { phoneValidator } from '../../../../../../Home/components/Orders/components/OrderForm/validateOrders';
import { addressValidator } from './helpers/validatorPictureEditForm';
import ProductSection from './components/ProductSection';

const validator = {
  [fields.date]: notValidate,
  [fields.status]: notValidate,
  [fields.name]: nameValidator,
  [fields.email]: emailValidator,
  [fields.phone]: phoneValidator,
  [fields.address]: addressValidator,
  [fields.item]: notValidate,
};

const EditOrderedPictureForm = ({ order, handleSubmit }) => {
  const { name, phone, email, item, price, pictureSrc } = order;

  const initialFormData = {
    [fields.date]: order.date,
    [fields.status]: statuses[0],
    [fields.name]: name,
    [fields.email]: email,
    [fields.phone]: phone,
    [fields.item]: item,
    [fields.price]: price,
    [fields.count]: 1,
    [fields.totalPrice]: price * 1,
    [fields.productImg]: `https://68.183.77.250/${pictureSrc}`,
    [fields.address]: '',
  };

  const [orderData, setOrderData] = useState(initialFormData);
  return (
    <Form initialFormData={orderData} styles={styles} validator={validator} onSubmit={handleSubmit}>
      <h2 className={styles.title}>Список замовлень</h2>
      <InfoSection />
      <CustomerSection />
      <ProductSection />
      <div className={styles.buttons}>
        <AdminButtonSaveEdit
          editBtnTxt="Очистити"
          saveBtnTxt="Змінити"
          reset={() => setOrderData(initialFormData)}
        />
      </div>
    </Form>
  );
};

EditOrderedPictureForm.defaultProps = {};

EditOrderedPictureForm.propTypes = {
  handleSubmit: func.isRequired,
  order: pictureOrder.isRequired,
};

export default EditOrderedPictureForm;

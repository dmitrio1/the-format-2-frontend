import React from 'react';
import TextArea from 'components/Form/components/TextArea';
import Input from 'components/Form/components/Input';
import CustomSelect from 'components/Form/components/CustomSelect';
import { fields, statuses } from '../helpers/constants';
import styles from '../edit-ordered-picture-form.module.scss';

const InfoSection = () => {
  return (
    <div className={styles.info}>
      <div className={styles['info-status']}>
        <h3 className={styles['sub-title']}>Статус</h3>

        <div className={styles['form-field']}>
          <p>Відправлено</p>
          <Input name={fields.date} disabled isRequired={false} />
        </div>

        <div className={styles['form-field']}>
          <p>Оновити статус</p>
          <CustomSelect name={fields.status} options={statuses} isRequired={false} defOptInd={0} />
        </div>
      </div>

      <div className={styles.address}>
        <h3 className={styles['sub-title']}>Адреса доставки</h3>
        <TextArea name={fields.address} placeholder="Змінити адресу" isRequired={false} />
      </div>
    </div>
  );
};

InfoSection.defaultProps = {};

InfoSection.propTypes = {};

export default InfoSection;

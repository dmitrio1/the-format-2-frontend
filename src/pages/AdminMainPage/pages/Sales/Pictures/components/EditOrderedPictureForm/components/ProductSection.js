import React from 'react';
import FormTable from './FormTable/FormTable';
import { fields } from '../helpers/constants';

const ProductSection = () => {
  const columns = ['', 'Назва', 'Ціна', 'Кількість', 'Всього'];

  const fieldsData = [
    { name: fields.productImg, type: 'img' },
    { name: fields.item, disabled: true },
    { name: fields.price, disabled: true },
    { name: fields.count, disabled: true },
    { name: fields.totalPrice, disabled: true },
  ];

  return (
    <div className="wrapper">
      <FormTable title="Продукт" fieldsData={fieldsData} columns={columns} />
    </div>
  );
};

ProductSection.defaultProps = {};

ProductSection.propTypes = {};

export default ProductSection;

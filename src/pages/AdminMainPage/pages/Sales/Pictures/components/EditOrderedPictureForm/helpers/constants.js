/* eslint-disable import/prefer-default-export */
export const fields = {
  name: 'name',
  date: 'date',
  email: 'email',
  phone: 'phone',
  status: 'status',
  address: 'address',
  item: 'item',
  price: 'price',
  totalPrice: 'totalPrice',
  count: 'count',
  productImg: 'productImg',
};

export const statuses = ['В обробці', 'Виконано'];

/* eslint-disable react/forbid-prop-types */
import React, { useEffect, useState } from 'react';
import { getOrder, getPictureToSale, updateOrder } from 'pages/AdminMainPage/helpers/fetchHelper';
import { object } from 'prop-types';
import EditOrderedPictureForm from './EditOrderedPictureForm';

const EditOrderedPictureContainer = props => {
  const [order, setOrder] = useState(null);

  // eslint-disable-next-line react/prop-types, react/destructuring-assignment
  const id = props.match.params.id;

  useEffect(() => {
    (async () => {
      const orderFromServer = await getOrder(id);
      /* ----------------- PICTURE GET SRC ---------------- OPEN */
      /* ******************************************************* */
      /* CRUTCH TO GET PICTURE BY NAME. SERVER /ORDER RESPONSE   */
      /* NOT CONTAIN PICTURE_SRC AND EVEN IT ID. **************  */
      /* UPD: PICTURE_SRC CAN BE WRONG ************************* */
      /* ******************************************************* */
      const picture = await getPictureToSale(orderFromServer.item);
      const pictureSrc = Array.isArray(picture) ? picture[0].picture : picture.picture;
      setOrder({ ...orderFromServer, pictureSrc });
      /* ----------------- PICTURE GET SRC ----------------- CLOSE */
    })();
  }, [id]);

  const handleSubmit = async values => {
    const status = await updateOrder(id, values);
    if (status === 200) {
      props.history.push('/adminpanel/orders');
    }
    return new Error('update is not successful');
  };

  return order ? (
    <EditOrderedPictureForm order={order} id={id} handleSubmit={handleSubmit} />
  ) : null;
};

EditOrderedPictureContainer.defaultProps = {};

EditOrderedPictureContainer.propTypes = {
  history: object.isRequired,
};

export default EditOrderedPictureContainer;

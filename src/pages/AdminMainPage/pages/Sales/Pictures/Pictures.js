/* eslint-disable no-underscore-dangle */
import React from 'react';
import { arrayOf, string, number, shape, func } from 'prop-types';
import styles from './picture.module.scss';
import AdminTableButtons from '../../../../../components/AdminTableButtons/AdminTableButtons';

const Pictures = React.memo(({ orders, onDelete }) => {
  return (
    <section>
      <h2 className={styles.title}>Список замовлень</h2>
      {orders.length !== 0 && (
        <ul className={styles.clientsList}>
          <li className={styles.clientsList__item}>
            <p>ID</p>
            <p>Назва</p>
            <p>Замовник</p>
            <p>Ел. почта</p>
            <p>Телефон</p>
            <p>Статус</p>
            <p>Дата</p>
          </li>

          {orders.map((order, idx) => {
            return (
              <li key={order._id} className={styles.clientsList__item}>
                <p>{idx + 1}</p>
                <p>{order.item}</p>
                <p>{order.name}</p>
                <p>{order.email}</p>
                <p>{order.phone}</p>
                <p>{order.status}</p>
                <p>{order.date}</p>
                {/* {isCourseRoute && <p>{client.course[0] ? client.course[0].courseType : ''}</p>}
                {!isCourseRoute && <p>{client.course[0].courseName}</p>} */}

                <AdminTableButtons
                  path={`/adminpanel/order/${order._id}/update`}
                  id={order._id}
                  onDelete={onDelete}
                />
              </li>
            );
          })}
        </ul>
      )}
    </section>
  );
});

Pictures.defaultProps = {};

Pictures.propTypes = {
  onDelete: func.isRequired,
  orders: arrayOf(
    shape({
      _id: string.isRequired,
      date: string.isRequired,
      status: string.isRequired,
      name: string.isRequired,
      phone: string.isRequired,
      email: string.isRequired,
      item: string.isRequired,
      price: number.isRequired,
      __v: number.isRequired,
    }),
  ).isRequired,
};

export default Pictures;

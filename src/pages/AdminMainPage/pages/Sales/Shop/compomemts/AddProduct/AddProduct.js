import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit';

// import fetchHelper from '../../../../helpers/fetchHelper';
// import UploadBox from '../uploadBox/UploadBox';
import styles from './add-product.module.scss';
import UploadBox from '../../../../AdminServices/components/uploadBox/UploadBox';
import { nameValidator } from '../../../../../../../components/Form/helpers/validator';
import { priceValidator, materialValidator, sizeValidator } from './validatorProduct';
import { addProduct } from '../../../../../helpers/fetchHelper';

const initialProductData = {
  name: '',
  size: '',
  material: '',
  price: '',
};

const AddProduct = ({ history }) => {
  const [photo, setPhoto] = useState(null);
  const [icon, setIcon] = useState(null);
  const [productData, setProductData] = useState(initialProductData);
  // eslint-disable-next-line no-unused-vars
  const [error, setError] = useState(null);

  const validator = {
    name: nameValidator,
    material: materialValidator,
    price: priceValidator,
    size: sizeValidator,
  };

  function handleAddPhoto(e) {
    setIcon(URL.createObjectURL(e.target.files[0]));
    setPhoto(e.target.files[0]);
  }

  async function handleSubmit(formValues) {
    if (!photo) {
      return;
    }

    const status = await addProduct({ ...formValues, image: photo });

    if (status === 200) {
      history.replace('/adminpanel/products');
      return;
    }

    setError('Something went wrong');
  }

  function resetFields() {
    setProductData({ ...initialProductData });
  }

  return (
    <section className={styles.wrapper}>
      <h2>Додати продукт</h2>
      <div className={styles.contentWrapper}>
        <Form
          initialFormData={productData}
          validator={validator}
          onSubmit={handleSubmit}
          styles={styles}
        >
          <Input placeholder="Введіть назву" name="name" labelText="Назва Товару" />
          <div className={styles['photo-info']}>
            <UploadBox handleAddPhoto={handleAddPhoto} icon={icon} isDynamicIcon />
            <div className={styles.inputs}>
              <Input name="size" labelText="Розмір (см.)" placeholder="20x20" />
              <Input name="material" labelText="Матеріал" placeholder="Введіть матеріал товару" />
              <Input name="price" labelText="Ціна (грн.)" placeholder="Введіть ціну товару" />
            </div>
          </div>

          <AdminButtonSaveEdit
            classes={styles}
            editBtnTxt="Очистити"
            saveBtnTxt="Зберегти"
            reset={resetFields}
          />
        </Form>
      </div>
    </section>
  );
};

AddProduct.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired,
  }).isRequired,
};

export default AddProduct;

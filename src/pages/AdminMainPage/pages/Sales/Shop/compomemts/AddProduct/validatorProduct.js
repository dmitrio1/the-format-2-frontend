import validator from 'validator';

export const priceValidator = value => {
  value += ''; // must be string
  if (!validator.isNumeric(value) || validator.isEmpty(value)) {
    return ' Будь ласка, введіть число';
  }

  if (value <= 0) {
    return ' Будь ласка, введіть додатнє число';
  }

  return null;
};

export const sizeValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Будь ласка, введіть прзмір';
  }

  const regex = /\d+x\d+/;

  if (!regex.test(value)) {
    return ' Будь ласка, введіть розмір в вірному форматі';
  }

  return null;
};

export const materialValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Будь ласка, введіть мтеріал';
  }

  return null;
};

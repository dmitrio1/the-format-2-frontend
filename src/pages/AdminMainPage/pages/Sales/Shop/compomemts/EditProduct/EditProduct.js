import React, { useState } from 'react';
import { shape, string, number, func } from 'prop-types';

import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import AdminButtonSaveEdit from 'components/AdminButtonSaveEdit';

// import fetchHelper from '../../../../helpers/fetchHelper';
// import UploadBox from '../uploadBox/UploadBox';
import Img from 'components/Form/components/Img';
import styles from './edit-product.module.scss';
import { nameValidator } from '../../../../../../../components/Form/helpers/validator';
import { priceValidator, materialValidator, sizeValidator } from '../AddProduct/validatorProduct';

const EditProduct = ({ product, handleSubmit }) => {
  // eslint-disable-next-line no-unused-vars
  const initialProductData = {
    name: product.name,
    material: product.material,
    price: product.price,
    size: product.size,
    image: product.picture,
  };
  const [productData, setProductData] = useState(initialProductData);

  const validator = {
    name: nameValidator,
    material: materialValidator,
    price: priceValidator,
    size: sizeValidator,
  };

  function resetFields() {
    setProductData({ ...initialProductData });
  }

  return (
    <section className={styles.wrapper}>
      <h2>Редагувати продукт</h2>
      <div className={styles.contentWrapper}>
        <Form
          initialFormData={productData}
          validator={validator}
          onSubmit={handleSubmit}
          styles={styles}
        >
          <Input placeholder="Введіть назву" name="name" labelText="Назва Товару" />
          <div className={styles['photo-info']}>
            <Img name="image" isRequired={false} />
            <div className={styles.inputs}>
              <Input name="size" labelText="Розмір (см.)" placeholder="20x20" />
              <Input name="material" labelText="Матеріал" placeholder="Введіть матеріал товару" />
              <Input name="price" labelText="Ціна (грн.)" placeholder="Введіть ціну товару" />
            </div>
          </div>

          <AdminButtonSaveEdit
            classes={styles}
            editBtnTxt="Очистити"
            saveBtnTxt="Зберегти"
            reset={resetFields}
          />
        </Form>
      </div>
    </section>
  );
};

EditProduct.propTypes = {
  handleSubmit: func.isRequired,

  product: shape({
    name: string.isRequired,
    material: string.isRequired,
    size: string.isRequired,
    price: number.isRequired,
  }).isRequired,
};

export default EditProduct;

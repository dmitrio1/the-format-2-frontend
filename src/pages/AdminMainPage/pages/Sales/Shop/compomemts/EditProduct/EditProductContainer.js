/* eslint-disable react/forbid-prop-types */
import React, { useEffect, useState } from 'react';
import { object } from 'prop-types';
import { getProduct, updateProduct } from '../../../../../helpers/fetchHelper';
import EditProduct from './EditProduct';

const EditProductContainer = ({ match, history }) => {
  const id = match.params.id;
  const [product, setProduct] = useState(null);

  useEffect(() => {
    (async () => {
      const res = await getProduct(id);
      setProduct({ ...res, picture: `https://68.183.77.250${res.picture}` });
    })();
  }, [id]);

  async function handleSubmit(formValues) {
    const status = await updateProduct(id, formValues);

    if (status === 200) {
      history.replace('/adminpanel/products');
    }
  }

  return product ? <EditProduct product={product} handleSubmit={handleSubmit} /> : null;
};

EditProductContainer.defaultProps = {};

EditProductContainer.propTypes = {
  history: object.isRequired,
  match: object.isRequired,
};

export default EditProductContainer;

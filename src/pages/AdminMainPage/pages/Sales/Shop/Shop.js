/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/prop-types */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import { NavLink } from 'react-router-dom';
import { func, array } from 'prop-types';
import styles from './shop.module.scss';
import AdminTableButtons from '../../../../../components/AdminTableButtons/AdminTableButtons';

const Shop = React.memo(({ pictures, onDelete }) => {
  return (
    <section>
      <h2 className={styles.title}>Каталог</h2>
      {pictures.length !== 0 && (
        <ul className={styles.clientsList}>
          <li className={styles.clientsList__item}>
            <p>ID</p>
            <p>Img</p>
            <p>Назва</p>
            <p>Ціна</p>
            <p>Розмір</p>
          </li>

          {pictures.map((picture, idx) => {
            return (
              <li key={picture._id} className={styles.clientsList__item}>
                <p>{idx + 1}</p>
                <p>
                  <img
                    src={`https://68.183.77.250${picture.picture}`}
                    className={styles.picture}
                    alt="product to sale"
                  />
                </p>
                <p>{picture.name}</p>
                <p>{picture.price}</p>
                <p>{picture.size}</p>

                <AdminTableButtons
                  path={`/adminpanel/product/${picture._id}/update`}
                  id={picture._id}
                  onDelete={onDelete}
                />
              </li>
            );
          })}
        </ul>
      )}
      <NavLink className={styles.addButton} to="/adminpanel/product/add">
        Додати Товар
      </NavLink>
    </section>
  );
});

Shop.defaultProps = {};

Shop.propTypes = {
  onDelete: func.isRequired,
  pictures: array.isRequired,
};

export default Shop;

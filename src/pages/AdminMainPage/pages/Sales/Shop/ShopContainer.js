import React, { useEffect, useState } from 'react';
import { getPictures, removePicture } from '../../../helpers/fetchHelper';
import Shop from './Shop';

const ShopContainer = () => {
  const [pictures, setPictures] = useState([]);

  useEffect(() => {
    (async () => {
      setPictures(await getPictures());
    })();
  }, []);

  const handleDeleteOrder = async id => {
    const status = await removePicture(id);
    if (status === 200) {
      setPictures(await getPictures());
    }
    return new Error('delete are not successful');
  };

  return <Shop pictures={pictures} onDelete={handleDeleteOrder} />;
};

ShopContainer.defaultProps = {};

ShopContainer.propTypes = {};

export default ShopContainer;

/* eslint-disable react/jsx-props-no-spreading */
import React, { useContext, useState } from 'react';
import Slider from 'react-slick';
import Modal from 'components/Modal/Modal';
import BuyingForm from 'pages/Shop/components/BuyingForm/BuyingForm';
import SuccessMessage from 'components/SuccessMessage/index';
import SuccessMsgContent from 'pages/Shop/components/SuccessMessageContent/SuccessMessageContent';
import DataContext from '../../../../../../Contexts';
import ShopItem from '../SliderItem/ShopItem';
import NextArrow from '../Arrows/NextArrow';
import PrevArrow from '../Arrows/PrevArrow';

const ShopSlider = () => {
  const appData = useContext(DataContext);
  const products = appData ? appData.shop : [];

  const [pictureForBuying, setPictureForBuying] = useState(null);
  const [isSuccessResponse, setIsSuccessResponse] = useState(false);

  function toggleSuccesResponse() {
    setIsSuccessResponse(prevState => {
      return !prevState;
    });
  }

  function closeModal() {
    setPictureForBuying(null);
  }

  function setPictureId(id) {
    const pictureById = products.find(picture => picture.id === id);
    setPictureForBuying(pictureById);
  }

  const shopList = products.map(product => {
    return <ShopItem picture={product} onBuy={() => setPictureId(product.id)} key={product.id} />;
  });

  const settings = {
    dots: false,
    infinite: true,
    speed: 700,
    slidesToShow: shopList.length < 3 ? shopList.length : 3,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 5000,
    cssEase: 'ease-out',
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
      {
        breakpoint: 820,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 540,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };

  return (
    <div className="shop-slider">
      <Slider {...settings}>{shopList}</Slider>
      {pictureForBuying && (
        <Modal isOpen={pictureForBuying !== null} onClose={closeModal}>
          <BuyingForm
            picture={pictureForBuying}
            onClose={closeModal}
            isSuccess={toggleSuccesResponse}
          />
        </Modal>
      )}
      {isSuccessResponse && (
        <Modal isOpen={isSuccessResponse} onClose={toggleSuccesResponse}>
          <SuccessMessage>
            <SuccessMsgContent onClose={toggleSuccesResponse} />
          </SuccessMessage>
        </Modal>
      )}
    </div>
  );
};

export default ShopSlider;

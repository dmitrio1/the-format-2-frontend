import React from 'react';
import { resourceHostUrl } from 'utils/index';
import { shape, func, string, number } from 'prop-types';
import styles from './shop-item.module.scss';

const ShopItem = ({ picture, onBuy }) => {
  return (
    <li className={styles.productList__item}>
      <p className={styles.productList__text}>{picture.name}</p>
      <div className={styles.productList__imgBox}>
        <div className={styles.wrapperDescription}>
          <div className={styles.productList__description}>
            <p className={styles.productList__descriptionText}>
              Розмір:
              <span className={styles.productList__descriptionText_span}>{picture.size}</span>
            </p>
            <p className={styles.productList__descriptionText}>
              Матеріал:
              <span className={styles.productList__descriptionText_span}>{picture.material}</span>
            </p>
            <button
              className={styles.productList__descriptionBtn}
              type="button"
              onClick={() => onBuy(picture.id)}
            >
              Придбати
            </button>
          </div>
          <img
            alt="cat"
            className={styles.productList__img}
            src={`${resourceHostUrl}${picture.image}`}
          />
        </div>
      </div>
      <p className={styles.productList__text}>
        Ціна:
        {picture.price}
        грн
      </p>
    </li>
  );
};

ShopItem.propTypes = {
  onBuy: func.isRequired,
  picture: shape({
    size: string.isRequired,
    price: number.isRequired,
    image: string.isRequired,
    material: string.isRequired,
    id: string.isRequired,
    name: string.isRequired,
  }).isRequired,
};

export default ShopItem;

import React from 'react';
import { func } from 'prop-types';
import styles from './arrows.module.scss';

const PrevArrow = props => {
  const { onClick } = props;
  return <div aria-hidden="true" className={styles.prev} onClick={onClick} />;
};

PrevArrow.defaultProps = {
  onClick: null,
};

PrevArrow.propTypes = {
  onClick: func,
};

export default PrevArrow;

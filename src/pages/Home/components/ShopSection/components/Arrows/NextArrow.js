import React from 'react';
import { func } from 'prop-types';
import styles from './arrows.module.scss';

const NextArrow = props => {
  const { onClick } = props;
  return <div aria-hidden="true" className={styles.next} onClick={onClick} />;
};

NextArrow.defaultProps = {
  onClick: null,
};

NextArrow.propTypes = {
  onClick: func,
};

export default NextArrow;

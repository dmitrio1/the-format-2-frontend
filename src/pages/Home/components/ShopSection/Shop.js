import React from 'react';
import ButtonLink from '../../../../components/ButtonLink/ButtonLink';
import ShopSlider from './components/ShopSlider/ShopSlider';
import styles from './shop.module.scss';
import './components/ShopSlider/shop-slider.scss';

const ShopSection = () => {
  return (
    <div className={styles.shop} id="shop">
      <h2 className={styles.title}>Магазин</h2>
      <ShopSlider />
      <ButtonLink url="/shop" text="Детальніше" />
    </div>
  );
};

export default ShopSection;

/* eslint-disable no-underscore-dangle */
import React, { useContext } from 'react';
import TileSection from 'components/TileSectionLayout/';
import DataContext from '../../../../Contexts';
import CoursesForAdultItem from './components/CoursesForAdultItem/CoursesForAdultItem';

const CoursesForAdult = () => {
  const appData = useContext(DataContext);
  const courses = appData ? appData.masterAdult.slice() : [];

  const coursesCards = courses
    .sort((left, right) => (left.order > right.order ? 1 : -1))
    .map(course => <CoursesForAdultItem key={course._id} course={course} />);

  return (
    <>
      <span id="CoursesForAdult" />
      <TileSection
        title="Живопис для дорослих"
        linkPath="/RegistrationPage"
        buttonText="Зареєструватись"
        buttonType="link"
      >
        {coursesCards}
      </TileSection>
    </>
  );
};

CoursesForAdult.propTypes = {};

export default CoursesForAdult;

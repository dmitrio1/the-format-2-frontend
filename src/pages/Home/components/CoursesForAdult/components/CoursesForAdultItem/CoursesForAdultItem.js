import React from 'react';
import { string, shape } from 'prop-types';
import { resourceHostUrl } from 'utils';
import styles from './courses-for-adult-item.module.scss';

const CoursesForAdultItem = ({ course }) => {
  const { name, icon } = course;

  return (
    <div className={styles.card}>
      <img className={styles.icon} src={`${resourceHostUrl}${icon}`} alt={name} />

      <div className={styles.name}>{name}</div>
    </div>
  );
};

CoursesForAdultItem.propTypes = {
  course: shape({
    icon: string.isRequired,
    name: string.isRequired,
  }).isRequired,
};

export default CoursesForAdultItem;

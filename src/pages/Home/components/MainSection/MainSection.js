import React, { useContext } from 'react';
import HeaderInner from 'components/Header/components/HeaderInner';
import Button from 'components/Button';
import DataContext from 'Contexts';
import classes from './mainSection.module.css';
import Sidebar from './components/Sidebar';
import VideoBg from './components/VideoBg/VideoBg';

const MainSection = () => {
  const appData = useContext(DataContext);
  const site = appData ? appData.site[0] : {};
  const { info1: title, info2: subtitle } = site;

  return (
    <div className={classes.mainSection}>
      <Sidebar className={classes.aside} />
      <div className={classes.content}>
        <VideoBg />
        <HeaderInner />
        <div className={classes.contentInner}>
          <div className={classes.text}>
            <h1 className={classes.title}>{title}</h1>
            <p className={classes.subtitle}>{subtitle}</p>
            <Button url="/RegistrationPage">Зареєструватись</Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainSection;

import React from 'react';
import SocialIcons from 'components/SocialIcons/SocialIcons';
import classes from './Sidebar.module.css';
import logo from './assets/header-logo.svg';

const Sidebar = () => {
  return (
    <div className={classes.headerSidebar}>
      <div className={classes.headerLogo}>
        <img src={logo} alt="logo" className={classes.logo} />
      </div>
      <div className={classes.sidebarText}>
        <hr className={classes.line} />
        <p className={classes.rotateText}>ПРИЄДНУЙСЯ ДО НАС</p>
      </div>
      <div className={classes.socialLinks}>
        <SocialIcons cls="sidebar" />
      </div>
    </div>
  );
};

export default Sidebar;

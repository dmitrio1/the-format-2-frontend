/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react';
import video from './assets/formatVideo.mp4';
import classes from './videoBg.module.scss';

const VideoBg = () => {
  return (
    <video className={classes.video} loop="loop" autoPlay="autoplay" muted="muted" preload="auto">
      <source src={video} type='video/mp4; codecs="avc1.42E030, mp4a.40.2"' />
    </video>
  );
};

export default VideoBg;

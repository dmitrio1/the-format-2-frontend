import React from 'react';
import ButtonModal from 'components/ButtonModal';
import CommentsSlider from './components/CommentsSlider/CommentsSlider';
import styles from './comments.module.scss';
import CommentsForm from './components/CommentsForm';

const Comments = () => {
  return (
    <section className={styles.comments} id="comments">
      <div className={styles.gradient} />
      <h2 className={styles.title}>Відгуки</h2>
      <CommentsSlider />
      <ButtonModal buttonText="Залишити Відгук" ModalItem={CommentsForm} />
    </section>
  );
};

export default Comments;

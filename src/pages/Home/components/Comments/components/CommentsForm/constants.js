/* eslint-disable import/prefer-default-export */

const fields = {
  name: 'name',
  email: 'email',
  comment: 'comment',
};

export { fields };

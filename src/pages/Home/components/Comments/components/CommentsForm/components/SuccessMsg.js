import React from 'react';
import { func } from 'prop-types';
import styles from './success-msg.module.scss';

const SuccessMsg = ({ onClose }) => {
  return (
    <div className={styles.textWrapper}>
      <button type="button" className={styles.closeBtn} onClick={() => onClose()}>
        <span>&#10006;</span>
      </button>
      <p className={styles.thanks}>Дякуємо за відгук!</p>
      <p className={styles.manager}>Найближчим часом ваш вiдгук зв&apos;явиться на сайтi</p>
    </div>
  );
};

SuccessMsg.propTypes = {
  onClose: func.isRequired,
};

export default SuccessMsg;

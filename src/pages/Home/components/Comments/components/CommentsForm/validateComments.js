/* eslint-disable import/prefer-default-export */
import validator from 'validator';

export const commentValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Будь ласка, введіть коментарій';
  }
  if (!validator.isLength(value, { min: 0, max: 800 })) {
    return ' не більше ніж 800 символів';
  }

  return null;
};

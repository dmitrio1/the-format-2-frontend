/* eslint-disable no-underscore-dangle */
import React, { useState } from 'react';
import axios from 'axios';
import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import ButtonSubmit from 'components/Form/components/ButtonSubmit';
import { func } from 'prop-types';
import { fields } from './constants';
import styles from './comments-form.module.scss';
import TextArea from '../../../../../../components/Form/components/TextArea';
import { nameValidator, emailValidator } from '../../../../../../components/Form/helpers/validator';
import { commentValidator } from './validateComments';
import SuccessMessage from '../../../../../../components/SuccessMessage';
import SuccessMsg from './components/SuccessMsg';

const CommentsForm = ({ closeModal }) => {
  const [isSuccess, setIsSuccess] = useState(false);
  const submissionHandler = formData => {
    const data = {
      name: formData[fields.name],
      email: formData[fields.email],
      text: formData[fields.comment],
    };

    axios.post('https://68.183.77.250/comments', data);
    setIsSuccess(true);
  };

  const initialFormData = {
    [fields.name]: '',
    [fields.email]: '',
    [fields.comment]: '',
  };

  const validator = {
    [fields.name]: nameValidator,
    [fields.email]: emailValidator,
    [fields.comment]: commentValidator,
  };

  return (
    <>
      {!isSuccess && (
        <Form
          initialFormData={initialFormData}
          onSubmit={submissionHandler}
          styles={styles}
          validator={validator}
        >
          <Input name={fields.name} labelText="Прізвище та ім'я:" />

          <Input name={fields.email} labelText="Електронна адреса:" />

          <TextArea name={fields.comment} labelText="Відгук:" />

          <ButtonSubmit text="Надіслати" />
        </Form>
      )}
      {isSuccess && (
        <SuccessMessage>
          <SuccessMsg onClose={closeModal} />
        </SuccessMessage>
      )}
    </>
  );
};

CommentsForm.propTypes = {
  closeModal: func.isRequired,
};
export default CommentsForm;

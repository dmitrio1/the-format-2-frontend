/* eslint-disable react/jsx-props-no-spreading */
import React, { useContext } from 'react';
import Slider from 'react-slick';
import DataContext from 'Contexts';
import CommentsItem from '../CommentsItem/CommentsItem';
import './comments-slider.scss';

const CommentsSlider = () => {
  const appData = useContext(DataContext);
  const comments = appData ? appData.comments : [];

  const commentsList = comments.map(comment => {
    return <CommentsItem name={comment.name} text={comment.text} key={comment.email} />;
  });

  const settings = {
    dots: true,
    infinite: true,
    arrows: false,
    speed: 700,
    slidesToShow: comments.length < 3 ? comments.length : 3,
    slidesToScroll: 3,
    autoplay: false,
    autoplaySpeed: 5000,
    cssEase: 'ease-out',
    // nextArrow: <NextArrow />,
    // prevArrow: <PrevArrow />,
    // responsive: [
    //   {
    //     breakpoint: 820,
    //     settings: {
    //       slidesToShow: 2,
    //       slidesToScroll: 1,
    //       infinite: true,
    //     },
    //   },
    // ],
  };

  return (
    <div className="comments-slider">
      <Slider {...settings}>{commentsList}</Slider>
    </div>
  );
};

export default CommentsSlider;

import React from 'react';
import { string } from 'prop-types';
import styles from './comments-item.module.scss';

const CommentsItem = ({ name, text }) => {
  return (
    <div className={styles['comment-wrapper']}>
      <div className={styles.comment}>
        <div className={styles.text}>{text}</div>
        <div className={styles.name}>{name}</div>
      </div>
    </div>
  );
};

CommentsItem.propTypes = {
  name: string.isRequired,
  text: string.isRequired,
};
export default CommentsItem;

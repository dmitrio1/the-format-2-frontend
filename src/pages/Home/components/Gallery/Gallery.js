/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-underscore-dangle */
import React, { useContext } from 'react';
import Slider from 'react-slick';
import './gallery.scss';
import { resourceHostUrl } from 'utils/index';
import DataContext from '../../../../Contexts';
import NextArrow from '../ShopSection/components/Arrows/NextArrow';
import PrevArrow from '../ShopSection/components/Arrows/PrevArrow';

const Gallery = () => {
  const appData = useContext(DataContext);
  const gallery = appData ? appData.gallery : [];

  const galleryItems = gallery.map(item => {
    return (
      <div className="slide" key={item._id}>
        <img className="slide-image" src={`${resourceHostUrl}${item.images}`} alt="our works" />
      </div>
    );
  });

  const settings = {
    dots: false,
    infinite: true,
    speed: 1000,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    cssEase: 'ease-out',
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
      {
        breakpoint: 660,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };

  return (
    <div className="home__gallery-section__root" id="galery">
      <h2 className="sliderName">Галерея</h2>
      <Slider {...settings} className="slider">
        {galleryItems}
      </Slider>
    </div>
  );
};

export default Gallery;

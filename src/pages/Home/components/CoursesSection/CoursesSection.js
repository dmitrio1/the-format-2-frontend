/* eslint-disable no-underscore-dangle */
import React, { useContext } from 'react';
// import PropTypes, { number, string } from 'prop-types';
import TileSection from 'components/TileSectionLayout/';
import DataContext from '../../../../Contexts';
import CourseSectionItem from './components/CourseSectionItem/CourseSectionItem';

const CoursesSection = () => {
  const appData = useContext(DataContext);
  const courses = appData ? appData.courseMain : [];

  const coursesCards = courses
    .sort((left, right) => (left.order > right.order ? 1 : -1))
    .map(course => <CourseSectionItem key={course._id} course={course} />);

  return (
    <>
      <span id="courses" />
      <TileSection
        title="Курси з живопису"
        linkPath="/CourseRegistrationPage"
        buttonText="Зареєструватись"
        buttonType="link"
      >
        {coursesCards}
      </TileSection>
    </>
  );
};

CoursesSection.propTypes = {};

export default CoursesSection;

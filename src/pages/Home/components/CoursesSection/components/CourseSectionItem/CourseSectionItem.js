import React, { useState } from 'react';
import { string, shape } from 'prop-types';

import { resourceHostUrl } from 'utils';
import Modal from 'components/Modal';

import styles from './course-section-item.module.scss';

const CourseSectionItem = ({ course }) => {
  const { description, name, icon } = course;
  const [isModalDisplayed, setIsModalDisplayed] = useState(false);

  return (
    <div className={styles.card}>
      <img className={styles.icon} src={`${resourceHostUrl}${icon}`} alt={name} />

      <div className={styles.name}>{name}</div>

      <button
        className={styles.modalButton}
        type="button"
        onClick={() => {
          setIsModalDisplayed(true);
        }}
      >
        Детальніше
      </button>

      <Modal
        isOpen={isModalDisplayed}
        onClose={() => {
          setIsModalDisplayed(false);
        }}
      >
        <div className="description">{description}</div>
      </Modal>
    </div>
  );
};

CourseSectionItem.propTypes = {
  course: shape({
    description: string.isRequired,
    icon: string.isRequired,
    name: string.isRequired,
  }).isRequired,
};

export default CourseSectionItem;

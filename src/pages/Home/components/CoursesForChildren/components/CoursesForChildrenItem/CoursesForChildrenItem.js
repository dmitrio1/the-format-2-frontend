import React from 'react';
import { string, shape } from 'prop-types';
import { resourceHostUrl } from 'utils';
import styles from './courses-for-children-item.module.scss';

const CoursesForChildrenItem = ({ course }) => {
  const { name, icon, duration, price } = course;

  return (
    <div className={styles.card}>
      <img className={styles.icon} src={`${resourceHostUrl}${icon}`} alt={name} />

      <div className={styles.name}>{name}</div>

      <div className={styles.description}>
        <div className="duration">
          Тривалість:
          {duration}
        </div>
        <div className="price">
          Вартість:
          {price}
        </div>
      </div>
    </div>
  );
};

CoursesForChildrenItem.propTypes = {
  course: shape({
    description: string.isRequired,
    icon: string.isRequired,
    name: string.isRequired,
    price: string.isRequired,
  }).isRequired,
};

export default CoursesForChildrenItem;

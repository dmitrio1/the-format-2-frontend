/* eslint-disable no-underscore-dangle */
import React, { useContext } from 'react';
import TileSection from 'components/TileSectionLayout/';
import DataContext from '../../../../Contexts';
import CoursesForChildrenItem from './components/CoursesForChildrenItem/CoursesForChildrenItem';
import styles from './courses-for-children.module.scss';
import clip from './assets/clip.svg';

const CoursesForChildren = () => {
  const appData = useContext(DataContext);
  const courses = appData ? appData.masterKid : [];

  const coursesCards = courses
    .sort((left, right) => (left.order > right.order ? 1 : -1))
    .map(course => <CoursesForChildrenItem key={course._id} course={course} />);

  return (
    <div className={styles.wrapper} id="coursesForChildren">
      <img className={styles.clip} src={clip} alt="clip" />

      <TileSection
        title="Живопис для дітей та підлітків"
        linkPath="/RegistrationPage"
        buttonText="Зареєструватись"
        buttonType="link"
      >
        {coursesCards}
      </TileSection>
    </div>
  );
};

CoursesForChildren.propTypes = {};

export default CoursesForChildren;

/* eslint-disable import/prefer-default-export */

const fields = {
  name: 'name',
  email: 'email',
  phone: 'phone',
  product: 'product',
  comment: 'comment',
  serviceName: 'serviceName',
};

export { fields };

import validator from 'validator';

export const productValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Оберіть товар ';
  }
  return null;
};

export const phoneValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть номер';
  }

  if (!validator.isMobilePhone(value, 'uk-UA', { strictMode: true })) {
    return ' Номер телефону не коректний';
  }
  return null;
};

export const commentValidator = value => {
  if (!validator.isLength(value, { min: 0, max: 800 })) {
    return ' не більше ніж 800 символів';
  }
  return null;
};

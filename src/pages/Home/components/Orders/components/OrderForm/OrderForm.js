/* eslint-disable no-underscore-dangle */
import React, { useContext, useState } from 'react';
import axios from 'axios';
import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import Select from 'components/Form/components/Select';
import ButtonSubmit from 'components/Form/components/ButtonSubmit';
// eslint-disable-next-line max-len
import SuccessMessageContent from 'pages/Shop/components/SuccessMessageContent/SuccessMessageContent';
import SuccessMessage from 'components/SuccessMessage/index';
import { func } from 'prop-types';
import DataContext from '../../../../../../Contexts';
import { fields } from '../../constants';
import { phoneValidator, productValidator, commentValidator } from './validateOrders';
import { nameValidator, emailValidator } from '../../../../../../components/Form/helpers/validator';

const OrderForm = ({ closeModal }) => {
  const appData = useContext(DataContext);
  const services = appData ? appData.service : [];

  const [isSuccess, setIsSuccess] = useState(false);

  const submitionHandler = formData => {
    const data = {
      name: formData.name,
      email: formData.email,
      phone: formData.phone,
      serviceName: formData.serviceName,
      serviceId: formData.product,
    };
    axios.post('https://68.183.77.250/admin/client', data);
    setIsSuccess(true);
  };

  const optionsList = services.map(service => {
    return <option key={service._id}>{service.name}</option>;
  });

  const initialFormData = {
    [fields.name]: '',
    [fields.email]: '',
    [fields.phone]: '+380',
    [fields.product]: services[0]._id,
    [fields.comment]: '',
    [fields.serviceName]: 'послуга (hardCoded)',
  };

  const validator = {
    [fields.name]: nameValidator,
    [fields.email]: emailValidator,
    [fields.phone]: phoneValidator,
    [fields.product]: productValidator,
    [fields.comment]: commentValidator,
  };

  return !isSuccess ? (
    <Form initialFormData={initialFormData} onSubmit={submitionHandler} validator={validator}>
      <Input name={fields.name} labelText="Прізвище та ім'я:" />
      <Input name={fields.email} labelText="Електронна адреса:" />
      <Input name={fields.phone} labelText="Телефон:" />

      <Select
        name={fields.product}
        type="select"
        labelText="Оберіть товар:"
        optionsList={optionsList}
      />

      <Input name={fields.comment} labelText="Додайте коментар:" isRequired={false} />

      <ButtonSubmit text="Надіслати" />
    </Form>
  ) : (
    <SuccessMessage>
      <SuccessMessageContent onClose={closeModal} />
    </SuccessMessage>
  );
};

OrderForm.propTypes = {
  closeModal: func.isRequired,
};

export default OrderForm;

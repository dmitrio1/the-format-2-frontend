/* eslint-disable no-underscore-dangle */
import React, { useContext } from 'react';
// import PropTypes, { number, string } from 'prop-types';
import TileSection from 'components/TileSectionLayout/';
import DataContext from '../../../../Contexts';
import CourseSectionItem from '../CoursesSection/components/CourseSectionItem/CourseSectionItem';
import styles from './orders.module.scss';
import OrderForm from './components/OrderForm/OrderForm';

const Orders = () => {
  const appData = useContext(DataContext);
  const courses = appData ? appData.service : [];
  const coursesCards = courses
    .sort((left, right) => (left.order > right.order ? 1 : -1))
    .map(course => <CourseSectionItem key={course._id} course={course} />);

  return (
    <div className={styles.wrapper} id="orders">
      <div className={styles.gradient} />
      <TileSection
        title="В нас можна замовити"
        linkPath="/CourseRegistrationPage"
        buttonText="Замовити"
        buttonType="modal"
        modalItem={OrderForm}
      >
        {coursesCards}
      </TileSection>
    </div>
  );
};

Orders.propTypes = {};

export default Orders;

import React, { useContext } from 'react';
import DataContext from 'Contexts';
import classes from './contactPlate.module.css';

const ContactPlate = () => {
  const appData = useContext(DataContext);
  const site = appData ? appData.site[0] : {};

  const { phone, email, address } = site;
  return (
    <div className={classes.contacts}>
      <p className={classes.adress}>{address}</p>
      <a className={classes.phone} href={`tel:${phone}`}>
        {phone}
      </a>
      <a className={classes.email} href={`mailto:${email}`}>
        {email}
      </a>
    </div>
  );
};

export default ContactPlate;

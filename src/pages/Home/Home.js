import React from 'react';
import MainSection from './components/MainSection';
import Footer from '../../components/Footer';
import Map from './components/MapSection/Map';
import CoursesSection from './components/CoursesSection';
import CoursesForChildren from './components/CoursesForChildren';
import CoursesForAdult from './components/CoursesForAdult';
import Orders from './components/Orders';
import Gallery from './components/Gallery';
import ShopSection from './components/ShopSection';
import Comments from './components/Comments/index';

const Home = () => {
  return (
    <div>
      <MainSection />
      <CoursesSection />
      <CoursesForChildren />
      <CoursesForAdult />
      <Orders />
      <Gallery />
      <ShopSection />
      <Comments />
      <Map />
      <Footer />
    </div>
  );
};

export default Home;

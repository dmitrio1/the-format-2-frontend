import validator from 'validator';

export const loginValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть  логін';
  }
  return null;
};
export const passwordValidator = value => {
  if (validator.isEmpty(value)) {
    return ' Введіть  пароль';
  }
  return null;
};

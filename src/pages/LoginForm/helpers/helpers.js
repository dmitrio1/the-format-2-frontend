import axios from 'axios';

async function login(data) {
  const response = await axios.put('https://68.183.77.250/logreg/login', data);
  return response;
}
export default login;

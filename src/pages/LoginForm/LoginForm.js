/* eslint-disable */
import React from 'react';
import Form from 'components/Form';
import Input from 'components/Form/components/Input';
import AdminPanelHeader from 'components/AdminPanelHeader';
import logo from './assets/header-logo.svg';
import classes from './loginForm.module.scss';
import login from './helpers/helpers';
import { fields } from './helpers/constants';
import { loginValidator, passwordValidator } from './helpers/formValidator';

const initFormData = {
  [fields.login]: '',
  [fields.password]: '',
};
const validator = {
  [fields.login]: loginValidator,
  [fields.password]: passwordValidator,
};
const LoginForm = ({ history, setAuth }) => {
  const handlerLogin = async data => {
    try {
      const response = await login(data);
      setAuth(true);
      history.push('/adminpanel/page/mainpage');

      console.log(response.data);
    } catch (e) {
      console.log(e.response);
    }
  };

  return (
    <>
      <AdminPanelHeader />
      <div className={classes.loginForm}>
        <img className={classes.logoIcon} src={logo} alt="логотип" />
        <Form
          initialFormData={initFormData}
          onSubmit={handlerLogin}
          styles={classes}
          validator={validator}
        >
          <Input name={fields.login} labelText="Ім'я" isRequired={false} />
          <Input name={fields.password} labelText="Пароль" isRequired={false} type="password" />
          <button className={classes.buttonSave} type="submit">
            Зайти
          </button>
        </Form>
      </div>
    </>
  );
};

export default LoginForm;

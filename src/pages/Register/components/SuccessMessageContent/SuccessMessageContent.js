import React from 'react';
import { Link } from 'react-router-dom';

import styles from './SuccessMessageContent.module.scss';

const SuccessMessageContent = () => {
  return (
    <div className={styles.textWrapper}>
      <p className={styles.thanks}>Дякуємо за реєстрацію!</p>
      <p className={styles.manager}>Найближчим часом наш менеджер зв&apos;яжеться з Вами</p>
      <Link to="/" className={styles.link}>
        на головну
      </Link>
    </div>
  );
};

export default SuccessMessageContent;

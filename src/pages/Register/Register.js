/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/no-onchange */
import React, { useState } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

import Header from 'components/Header';
import BreadCrumbs from 'components/BreadCrumbs';
import Footer from 'components/Footer';
import SuccessMessage from 'components/SuccessMessage';
import styles from './register.module.scss';

import validateBaseForm from './helpers/validateBaseForm';
import validateMasterClassForm from './helpers/validateMasterClassForm';
import validateCourseForm from './helpers/validateCourseForm';

import BaseForm from './formParts/BaseForm';
import MasterClassesForm from './formParts/MasterClassesForm';
import CoursesForm from './formParts/CoursesForm';
import SuccessMessageContent from './components/SuccessMessageContent/SuccessMessageContent';

import { isEmpty } from './helpers';

const breadCrumbsItems = [
  { to: '/', pageName: 'Головна' },
  { to: '/RegistrationPage', pageName: 'Реєстраційна форма' },
];

const initialState = {
  name: '',
  email: '',
  phone: '',
  courseId: '',
  picId: '',
};

const Register = ({ match }) => {
  /**
   *  @TODO  now component is purely scalable, need one more refactor
   */

  /**
   * can be organized like base + optional fields, base + optional validation
   */

  /**
   * @TODO think about it in future. . .
   */
  const isCoursesRoute = match.url === '/CourseRegistrationPage';

  const initialCourseType = isCoursesRoute ? 'Course' : 'MasterAdult';

  const [state, setState] = useState({ ...initialState, courseType: initialCourseType });
  const [errors, setErrors] = useState({});
  const [isSuccess, setIsSuccess] = useState(false);

  function toggleIsSuccess() {
    /* eslint-disable-next-line */
    setIsSuccess(prevState => !isSuccess);
  }

  function handleSubmit(e) {
    e.preventDefault();
    const _state = { ...state };

    const baseValidationErrors = validateBaseForm(_state);
    const optionalValidator = isCoursesRoute ? validateCourseForm : validateMasterClassForm;
    const optionalValidationErrors = optionalValidator(_state);

    const formErrors = Object.assign(baseValidationErrors, optionalValidationErrors);

    if (!isEmpty(formErrors)) {
      setErrors(formErrors);
      return;
    }

    axios
      .post('https://68.183.77.250/client', state)
      .then(() => toggleIsSuccess())
      .catch(() => {});
  }

  function handleChange(e) {
    const { name, value } = e.target;

    setErrors(prevErrors => {
      return {
        ...prevErrors,
        [name]: null,
      };
    });

    if (name === 'courseType') {
      setState(prevState => {
        return {
          ...prevState,
          [name]: value,
          courseId: '',
        };
      });
    }

    setState(prevState => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  }

  return (
    <>
      <Header />
      <section className={styles.registerFormWrapper}>
        <div className={styles.breadCrumbsWrapper}>
          <BreadCrumbs breadCrumbsItems={breadCrumbsItems} />
        </div>
        {!isSuccess && (
          <form className={styles.registerForm} onSubmit={handleSubmit}>
            <BaseForm state={state} errors={errors} onChange={handleChange} />
            {isCoursesRoute ? (
              <CoursesForm state={state} errors={errors} onChange={handleChange} />
            ) : (
              <MasterClassesForm state={state} errors={errors} onChange={handleChange} />
            )}
            <button type="submit" className={styles.registerForm__btn}>
              Надіслати
            </button>
          </form>
        )}
        {isSuccess && (
          <SuccessMessage>
            <SuccessMessageContent />
          </SuccessMessage>
        )}
      </section>
      <Footer />
    </>
  );
};

Register.propTypes = {
  match: PropTypes.shape({
    url: PropTypes.string.isRequired,
  }).isRequired,
};

export default Register;

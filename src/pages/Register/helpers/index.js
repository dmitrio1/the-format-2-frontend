/* eslint-disable */
export function isEmpty(val) {
  if (val === undefined || val === null) {
    return true;
  }

  if (val === '') {
    return true;
  }

  if (typeof val === 'object') {
    let r = true;

    for (const f in val) {
      r = false;
    }

    return r;
  }

  return false;
}

/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
/* eslint-disable no-unused-vars */

import validator from 'validator';
import { isEmpty } from '.';

function validateCourseForm(data) {
  const errors = {};

  data.courseId = !isEmpty(data.courseId) ? data.courseId : '';

  if (validator.isEmpty(data.courseId)) {
    errors.courseId = ' Оберіть курс';
  }

  return errors;
}

export default validateCourseForm;

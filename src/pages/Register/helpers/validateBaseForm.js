/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
/* eslint-disable no-unused-vars */

import validator from 'validator';
import { isEmpty } from '.';

function validateBaseForm(data) {
  const errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.phone = !isEmpty(data.phone) ? data.phone : '';

  if (!validator.isLength(data.name, { min: 2, max: 40 })) {
    errors.name = " Ім'я має складатися не менш ніж з 2 та не більше ніж з 40 символів";
  }

  if (validator.isEmpty(data.name)) {
    errors.name = " Введіть ім'я";
  }

  if (!validator.isEmail(data.email)) {
    errors.email = ' Будь ласка, введіть коректний email';
  }

  if (validator.isEmpty(data.email)) {
    errors.email = ' Будь ласка, введіть email';
  }

  if (!validator.isMobilePhone(data.phone, 'uk-UA', { strictMode: true })) {
    errors.phone = ' Номер телефону не коректний';
  }

  if (validator.isEmpty(data.phone)) {
    errors.phone = ' Введіть номер';
  }

  return errors;
}

export default validateBaseForm;

/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
/* eslint-disable no-unused-vars */

import validator from 'validator';
import { isEmpty } from '.';

function validateProfileUpdate(data) {
  const errors = {};

  data.courseType = !isEmpty(data.courseType) ? data.courseType : '';
  data.courseId = !isEmpty(data.courseId) ? data.courseId : '';

  if (validator.isEmpty(data.courseType)) {
    errors.courseType = ' Оберіть категорію';
  }

  if (validator.isEmpty(data.courseId)) {
    errors.courseId = ' Оберіть категорію живопису';
  }

  return errors;
}

export default validateProfileUpdate;

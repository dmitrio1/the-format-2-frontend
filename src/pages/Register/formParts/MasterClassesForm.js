/*eslint-disable*/
import React, { useContext, useEffect } from 'react';
// import PropTypes from 'prop-types';
import DataContext from 'Contexts';
import CourseSectionItem from 'pages/Home/components/CoursesSection/components/CourseSectionItem/CourseSectionItem';

import styles from '../register.module.scss';

const MasterClassesForm = props => {
  const { state, errors, onChange } = props;

  const coursesData = useContext(DataContext);

  const isAdultMasterClasses = state.courseType === 'MasterAdult';

  if (!coursesData) {
    return null;
  }

  const {
    courses: masterClasses,
    masterAdult: adultMasterClasses,
    masterKid: kidsMasterClasses,
  } = coursesData;

  function getCourseName(name) {
    switch (name) {
      case 'Adult':
        return 'MasterAdult';
      case 'Child':
        return 'MasterKid';
      default:
        return name;
    }
  }

  function getCoursesByQuery(category) {
    switch (category) {
      case 'MasterKid':
        return kidsMasterClasses;
      case 'MasterAdult':
        return adultMasterClasses;
      default:
        return [];
    }
  }

  const getMasterClassesOptions = () => {
    return masterClasses.map(masterClass => {
      return (
        <option key={masterClass._id} value={getCourseName(masterClass.name)}>
          {masterClass.name}
        </option>
      );
    });
  };

  const getPaintingOptions = () => {
    return getCoursesByQuery(state.courseType).map(category => {
      return (
        <option key={category._id} value={category._id}>
          {category.name}
        </option>
      );
    });
  };

  const getPictures = () => {
    const courseByQuery = getCoursesByQuery(state.courseType).find(
      course => course._id === state.courseId,
    );

    return courseByQuery.desc.map(pictureInfo => {
      return (
        <div key={pictureInfo._id} className={styles.onePictureContainer}>
          <input type="radio" name="picId" onChange={onChange} value={pictureInfo._id} />
          <div className={styles.imgBox}>
            <img
              className={styles.img}
              src={`https://68.183.77.250/${pictureInfo.image}`}
              alt={pictureInfo.imgDesc}
            />
            <p>{pictureInfo.imgDesc}</p>
          </div>
        </div>
      );
    });
  };

  return (
    <>
      <label className={styles.registerForm__label} htmlFor="masterClasses">
        <p className={styles.registerForm__text}>
          Оберіть категорію:
          <span className={styles.registerForm__text_warn}>*</span>
          <span className={styles.registerForm__text_warn}>{errors.courseType}</span>
        </p>
        <select
          id="masterClasses"
          className={styles.registerForm__input}
          onChange={onChange}
          value={state.courseType}
          name="courseType"
        >
          {getMasterClassesOptions()}
        </select>
      </label>
      <label className={styles.registerForm__label} htmlFor="painting-category">
        <p className={styles.registerForm__text}>
          Оберіть категорію живопису:
          <span className={styles.registerForm__text_warn}>*</span>
          <span className={styles.registerForm__text_warn}>{errors.courseId}</span>
        </p>
        <select
          id="painting-category"
          className={styles.registerForm__input}
          value={state.courseId}
          onChange={onChange}
          name="courseId"
        >
          <option />
          {getPaintingOptions()}
        </select>
      </label>
      {state.courseId && isAdultMasterClasses && (
        <label className={styles.radioContainer}>{getPictures()}</label>
      )}
    </>
  );
};

MasterClassesForm.propTypes = {};

export default MasterClassesForm;

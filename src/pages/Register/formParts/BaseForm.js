/*eslint-disable*/
import React from 'react';
import PropTypes from 'prop-types';
import styles from '../register.module.scss';

const BaseForm = props => {
  const { state, errors, onChange } = props;
  return (
    <>
      <label className={styles.registerForm__label} htmlFor="name">
        <p className={styles.registerForm__text}>
          Прізвище та ім&apos;я:
          <span className={styles.registerForm__text_warn}>*</span>
          <span className={styles.registerForm__text_warn}>{errors.name}</span>
        </p>
        <input
          id="name"
          className={styles.registerForm__input}
          name="name"
          value={state.name}
          onChange={onChange}
        />
      </label>
      <label className={styles.registerForm__label} htmlFor="email">
        <p className={styles.registerForm__text}>
          Електронна адреса:
          <span className={styles.registerForm__text_warn}>*</span>
          <span className={styles.registerForm__text_warn}>{errors.email}</span>
        </p>
        <input
          id="email"
          type="email"
          className={styles.registerForm__input}
          name="email"
          value={state.email}
          onChange={onChange}
        />
      </label>
      <label className={styles.registerForm__label} htmlFor="phone">
        <p className={styles.registerForm__text}>
          Телефон:
          <span className={styles.registerForm__text_warn}>*</span>
          <span className={styles.registerForm__text_warn}>{errors.phone}</span>
        </p>
        <input
          id="phone"
          type="tel"
          mask="+38(011)111-11-11"
          placeholder="Введіть Телефон у форматі +380___"
          className={styles.registerForm__input}
          name="phone"
          value={state.phone}
          onChange={onChange}
        />
      </label>
    </>
  );
};

BaseForm.propTypes = {};

export default BaseForm;

/*eslint-disable*/
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import styles from '../register.module.scss';
import DataContext from 'Contexts';

const CoursesForm = props => {
  const { state, errors, onChange } = props;
  const coursesData = useContext(DataContext);

  if (!coursesData) {
    return null;
  }

  const { courseMain: courses } = coursesData;

  const getCoursesOptions = () => {
    return courses.map(course => {
      return (
        <option key={course._id} value={course._id}>
          {course.name}
        </option>
      );
    });
  };

  return (
    <label className={styles.registerForm__label} htmlFor="courses">
      <p className={styles.registerForm__text}>
        Оберіть курс:
        <span className={styles.registerForm__text_warn}>*</span>
        <span className={styles.registerForm__text_warn}>{errors.course}</span>
      </p>
      <select
        id="courses"
        className={styles.registerForm__input}
        onChange={onChange}
        value={state.courseId}
        name="courseId"
      >
        <option />
        {getCoursesOptions()}
      </select>
    </label>
  );
};

CoursesForm.propTypes = {};

export default CoursesForm;

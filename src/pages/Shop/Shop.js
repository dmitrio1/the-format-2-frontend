import React, { useState, useEffect } from 'react';
import axios from 'axios';

import SuccessMessage from 'components/SuccessMessage';
import BreadCrumbs from 'components/BreadCrumbs';
import Footer from 'components/Footer';
import SuccessMessageContent from './components/SuccessMessageContent/SuccessMessageContent';
import Header from '../../components/Header';
import Modal from '../../components/Modal';
import Squares from './components/Squares/Squares';
import ProductList from './components/ProductList/ProductList';
import BuyingForm from './components/BuyingForm/BuyingForm';
import styles from './Shop.module.scss';

const breadCrumbsItems = [
  { to: '/', pageName: 'Головна' },
  { to: '/shop', pageName: 'Магазин' },
];

const Shop = () => {
  const [pictures, setPictures] = useState([]);
  const [pictureForBuying, setPictureForBuying] = useState(null);
  const [isSuccessResponse, setIsSuccessResponse] = useState(false);

  useEffect(() => {
    axios.get('https://68.183.77.250/shopData').then(({ data }) => {
      setPictures(data.shop);
    });
  }, []);

  function toggleSuccesResponse() {
    setIsSuccessResponse(prevState => {
      return !prevState;
    });
  }

  function closeModal() {
    setPictureForBuying(null);
  }

  function setPictureId(id) {
    const pictureById = pictures.find(picture => picture.id === id);

    setPictureForBuying(pictureById);
  }

  return (
    <>
      <Header />
      <div className={styles.shopWrapper}>
        <div className={styles.breadCrumbsWrapper}>
          <BreadCrumbs breadCrumbsItems={breadCrumbsItems} />
        </div>
        <Squares />
        {pictures.length !== 0 && <ProductList pictures={pictures} onBuy={setPictureId} />}
        {pictureForBuying && (
          <Modal isOpen={pictureForBuying !== null} onClose={closeModal}>
            <BuyingForm
              picture={pictureForBuying}
              onClose={closeModal}
              isSuccess={toggleSuccesResponse}
            />
          </Modal>
        )}
        {isSuccessResponse && (
          <Modal isOpen={isSuccessResponse} onClose={toggleSuccesResponse}>
            <SuccessMessage>
              <SuccessMessageContent onClose={toggleSuccesResponse} />
            </SuccessMessage>
          </Modal>
        )}
      </div>
      <Footer />
    </>
  );
};

export default Shop;

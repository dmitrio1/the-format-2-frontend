import React from 'react';
import PropTypes from 'prop-types';

import styles from './SuccessMessageContent.module.scss';

const SuccessMessageContent = ({ onClose }) => {
  return (
    <div className={styles.textWrapper}>
      <button type="button" className={styles.closeBtn} onClick={() => onClose()}>
        <span>&#10006;</span>
      </button>
      <p className={styles.thanks}>Дякуємо за замовлення!</p>
      <p className={styles.manager}>Найближчим часом наш менеджер зв&apos;яжеться з Вами</p>
    </div>
  );
};

SuccessMessageContent.propTypes = {
  onClose: PropTypes.func.isRequired,
};

export default SuccessMessageContent;

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import validateBaseForm from 'pages/Register/helpers/validateBaseForm';
import { isEmpty } from 'pages/Register/helpers';
import styles from './BuyingForm.module.scss';

const initialState = {
  name: '',
  email: '',
  phone: '',
};

const initialErrorsState = {
  name: null,
  email: null,
  phone: null,
};

const BuyingForm = ({ picture, onClose, isSuccess }) => {
  const [state, setState] = useState(initialState);
  const [errors, setErrors] = useState(initialErrorsState);

  function handleSubmit(e) {
    e.preventDefault();

    const formErrors = validateBaseForm(state);

    if (!isEmpty(formErrors)) {
      setErrors(formErrors);
      return;
    }

    const info = { ...state, item: picture.name, price: picture.price };

    axios.post('https://68.183.77.250/shop', info).then(() => {
      onClose();
      isSuccess();
    });
  }

  function handleChange(e) {
    const { name, value } = e.target;
    setErrors(prevErrors => {
      return {
        ...prevErrors,
        [name]: null,
      };
    });
    setState(prevState => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  }

  return (
    <div className={styles.container}>
      <button type="button" className={styles.closeBtn} onClick={() => onClose()}>
        <span>&#10006;</span>
      </button>

      <div className={styles.details}>
        <p className={styles.details__info}>
          Картина:
          {picture.name}
        </p>
        <p className={styles.details__info}>
          Ціна:
          {picture.price}
        </p>
      </div>
      <form className={styles.form} onSubmit={handleSubmit}>
        <label htmlFor="name" className={styles.form__label}>
          <p className={styles.form__text}>
            Прізвище та ім&lsquo;я:
            <span className={styles.form__text_warn}>*</span>
            <span className={styles.form__text_warn}>{errors.name}</span>
          </p>
          <input name="name" type="text" className={styles.form__input} onChange={handleChange} />
        </label>
        <label htmlFor="email" className={styles.form__label}>
          <p className={styles.form__text}>
            Електронна адреса:
            <span className={styles.form__text_warn}>*</span>
            <span className={styles.form__text_warn}>{errors.email}</span>
          </p>
          <input name="email" type="email" className={styles.form__input} onChange={handleChange} />
        </label>
        <label htmlFor="phone" className={styles.form__label}>
          <p className={styles.form__text}>
            Телефон:
            <span className={styles.form__text_warn}>*</span>
            <span className={styles.form__text_warn}>{errors.phone}</span>
          </p>
          <input
            name="phone"
            type="tel"
            className={styles.form__input}
            onChange={handleChange}
            mask="+38(011)111-11-11"
            placeholder="Введіть Телефон у форматі +380___"
          />
        </label>
        <button className={styles.form__btn} type="submit">
          Надіслати
        </button>
      </form>
    </div>
  );
};

BuyingForm.propTypes = {
  isSuccess: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  picture: PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }).isRequired,
};

export default BuyingForm;

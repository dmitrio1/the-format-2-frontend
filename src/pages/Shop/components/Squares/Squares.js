import React from 'react';

import styles from './Squares.module.scss';

const Squares = () => {
  return (
    <ul className={styles.squares}>
      <li className={styles.squares__item_1} />
      <li className={styles.squares__item_2} />
      <li className={styles.squares__item_3} />
      <li className={styles.squares__item_4} />
      <li className={styles.squares__item_5} />
    </ul>
  );
};

export default Squares;

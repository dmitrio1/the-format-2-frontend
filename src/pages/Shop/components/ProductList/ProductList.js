import React from 'react';
import propTypes from 'prop-types';

import styles from './ProductList.module.scss';

const ProductList = ({ pictures, onBuy }) => {
  return (
    <ul className={styles.productList}>
      {pictures.map(picture => {
        return (
          <li className={styles.productList__item} key={picture.id}>
            <p className={styles.productList__text}>{picture.name}</p>
            <div className={styles.productList__imgBox}>
              <div className={styles.productList__description}>
                <p className={styles.productList__descriptionText}>
                  Розмір:
                  <span className={styles.productList__descriptionText_span}>{picture.size}</span>
                </p>
                <p className={styles.productList__descriptionText}>
                  Матеріал:
                  <span className={styles.productList__descriptionText_span}>
                    {picture.material}
                  </span>
                </p>
                <button
                  className={styles.productList__descriptionBtn}
                  type="button"
                  onClick={() => onBuy(picture.id)}
                >
                  Придбати
                </button>
              </div>
              <img
                alt="cat"
                className={styles.productList__img}
                src={`https://68.183.77.250${picture.image}`}
              />
            </div>
            <p className={styles.productList__text}>
              Ціна:
              {picture.price}
              грн
            </p>
          </li>
        );
      })}
    </ul>
  );
};

ProductList.propTypes = {
  onBuy: propTypes.func.isRequired,
  pictures: propTypes.arrayOf(propTypes.object.isRequired).isRequired,
};

export default ProductList;

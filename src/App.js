import React, { useEffect, useState } from 'react';
import './App.css';
import Axios from 'axios';
import Router from './router';
import DataContext from './Contexts';

const App = () => {
  const [appData, setAppData] = useState(null); // variable to store data for dataContext

  useEffect(() => {
    const loadData = async () => {
      const res = await Axios.get('https://68.183.77.250/index'); // response is an object
      setAppData(res.data);
    };

    loadData();
  }, []);

  return (
    <DataContext.Provider value={appData}>
      <div className="App">
        <Router />
      </div>
    </DataContext.Provider>
  );
};

export default App;

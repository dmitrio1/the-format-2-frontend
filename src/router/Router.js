import React from 'react';
// import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
// /* PLOP_INJECT_COMPONENT_IMPORT */

import Register from 'pages/Register';
import Shop from 'pages/Shop';
import AdminMainPage from 'pages/AdminMainPage';
// import AdminClients from 'components/AdminClients';
import Home from 'pages/Home';

/* PLOP_INJECT_COMPONENT_IMPORT */
import FormExample from './FormExample';

const Router = props => {
  return (
    <>
      <Switch>
        <Route path="/RegistrationPage" component={Register} />
        <Route path="/CourseRegistrationPage" component={Register} />
        <Route path="/test" component={FormExample} />
        <Route path="/shop" component={Shop} />
        {/* <CoursesForAdult /> */}
        <Route exact path="/" component={Home} />
        <Route path="/adminpanel" component={AdminMainPage} />
        {/* <Route path="/adminpanel/clients" component={AdminClients} /> */}
        /* PLOP_INJECT_COMPONENT_ROUTE */
      </Switch>
    </>
  );
};

// Router.propTypes = {};

export default Router;

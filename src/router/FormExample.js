import React from 'react';
import PropTypes from 'prop-types';
import Form from 'components/Form';
import Input from 'components/Form/components/Input';

const FormExample = props => {
  return (
    <div>
      <Form initialFormData={{ name: 'her', surname: 's gori' }}>
        <Input name="name" />
        <Input name="surname" />
      </Form>
    </div>
  );
};

FormExample.propTypes = {};

export default FormExample;

import { useLocation } from 'react-router-dom';
import { useEffect } from 'react';
// eslint-disable-next-line import/prefer-default-export
export const resourceHostUrl = 'http://68.183.77.250';

// *********************************************************************
// To handle native navigation behavior of react-router/react-router-dom
// that after navigation page saves it's scroll position
// *********************************************************************
export const ScrollToTop = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
};
